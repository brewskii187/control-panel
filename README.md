Source-Linux is a way for server operators to control their game servers from a simple to use web panel. It currently supports all games that can be downloaded though SteamCmd and is mobile friendly.

Do not download in browser, there are git submodules you will need to use git clone;


###Installation###
```
#!git
git clone https://brewskii187@bitbucket.org/brewskii187/control-panel.git; cd control-panel; git submodule init; git submodule update
```
Once you install navigate to http://yourdomain.com/control-panel/install and go through the installer.

Install video here http://youtu.be/n9KNXpA_Cks

###Donations###
If you like my work please feel free to send a donation. I have spent many many hours developing and bug testing.
Every donation made will help improve and enhance and build upon the control panel.
Please see http://1911.expert for the donations link.

###Some features include: ###
* Start/Stop/Update/Restart/Backup/Edit any server.
* View list of players in game server.
* Auto-update with email notification
* Full mobile theme and custom theme support
* Crash detection and restarter with email notification.
* Multi-user with fine grained permissions.
* multi-language support.
* Server stats uptime/cpu usage/ram/system load
* Ram usage of each individual game server
* Built in rcon console.
* Full file browser that can upload/download/move/rename/delete/create/unzip files.
* File editor with syntax highlighting
* Game Server lockdown, so the server cant be accidentally started when its down for maintenance/updates ect.

###Screenshots###

http://1911.expert/screenshots/source-linux/

### Here is a list of things you will need! ###
1. A linux server with root access.
2. Apache, MySQL, PHP
3. lib32gcc1 this is so steamcmd can run on 64 bit systems (apt-get install lib32gcc1)
4. zip/unzip (apt-get install zip && apt-get install unzip)


### Here are some optional things ###
* password less sudo for your apache user with access to kill and netstat


### What not to do ###
Do **not** install this on a shared hosting webserver. While it will probably to some extent work you hosting provider will probably get really mad at you, and they probably kill any process that runs longer than 30 seconds.


### Recommended permissions. ###
1. Create a new linux user (adduser steam)
2. Edit your apache config to run apache as new linux user. restart apache (service apache2 restart)
3. chown the control-panel folder the your apache user.
4. Give your new linux user passwordless sudo for the kill and netstat commands (optional)


Crontabs that need to be run as your apache user

```
#!cron

*/5 * * * * /usr/bin/php /var/www/cron/crash-check.php
0 5 * * * /usr/bin/php /var/www/cron/restarter.php
@hourly /usr/bin/php /var/www/cron/update-check.php
```

 There are other ways to handle the apache user and permissions this is the one I use.
