<?php
define('NineteenEleven', true);
require_once '../includes/MiscClass.php';
//http://jquery.malsup.com/form/
?>
<html>
    <head>
        <title>NineteenElevens Control Panel</title>
        <meta charset="UTF-8">
        <meta required name="viewport" content="width=device-width, initial-scale=1.0">

        <!--Add some jquery -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

        <!--Jquery UI -->
        <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="../themes/default/style.css" />
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
        <script src="http://malsup.github.com/jquery.form.js"></script>
        <script>
            var options = {
                target: '#result', // target element(s) to be updated with server response
                success: showResponse
            };
            function showResponse(responseText) {

                //$('#mysql').hide();
            }
            $(document).ready(function() {
                $('#mysql').ajaxForm(options);
            });
        </script>

        <style type="text/css">
            .form val{
                display: block;
            }
            #dir{
                width:200px;
            }
            .border{
                border:1px solid #333;
                margin:8px 0;
                padding: 10px 10px;
            }
        </style>
    </head>
    <body>
        <div class='wrapper'>
            <div class='contentArea'>


                <div id='content'>
                    <form id="mysql" action="ajax/mysql.php" method="post" class="form">
                        <div class='border'>
                            <h3>Tell me about your server</h3>
                            <div class='val'>MySql Host: <input type="text" required name="host" value='localhost' /></div>
                            <div class='val'>MySql User: <input type="text" required name="user" /></div>
                            <div class='val'>MySql Password: <input type="password" required name='pass' /></div>
                            <div class='val'>MySql Database Name:<input type="text" required name='dbname' /></div>
                            <div class='val'>Table Prefix:<input type="text" required name='prefix' value="<?php echo Misc::random(4); ?>" />- these 4 characters were choosen at random, you may change them.</div></div>
                        <div class='border'>
                            <h3>What directories are we using?</h3>
                            <div class='val'>Home Directory: <input type="text" required name="homeDir" id='dir' value='/home/steam/gameservers/'/> -Where you want the game server to install to. DO NOT MAKE THIS VISIBLE FROM THE WEB!!! i.e do not install under /var/www/</div>
                            <div class='val'>Backups Directory: <input type="text" required name="backupsDir" id='dir' value="/home/steam/backups/"/> -Where you want the backups to go. </div>
                        </div>
                        <div class='border'>
                            <h3>Tell me about yourself</h3>
                            <div class='val'>CP Username: <input type="text" required name="cpuser" /> - Username you will use to log into the control panel</div>
                            <div class='val'>CP Pass: <input type="password" required name="cppass" /></div>
                            <div class='val'>Email Address: <input type="email" required name="cpemail" /></div>
                        </div>
                        <div class='border'>
                            <h3>Email settings</h3>
                            <div class='val'>Senders Name: <input type="text" required name="senderName" /></div>
                            <div class='val'>Senders Email: <input type="email" required name="senderEmail" /></div>
                            <div class='val'>Send copy of all emails to address? <input type='radio' name='dump' required value='true'>yes<input type='radio' name='dump' required value='false'>no</div>
                            <div class='val'>Dump Address: <input type="email" name="senderDumpAddy" /></div>
                        </div>
                        <div class='val'><input type="submit" id='subForm' value="Submit" class='actionBtn' /></div>
                    </form>
                    <div id='result'></div>
                </div>
            </div>
</html>
