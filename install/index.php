<html>
    <head>
        <title>NineteenElevens Control Panel</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!--Add some jquery -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

        <!--Jquery UI -->
        <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css" />
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
        <link type="text/css" rel="stylesheet" href="../themes/default/style.css" />
    </head>
    <body>
        <div class='wrapper'>
            <div class='contentArea'>
                O hai! want to install? <br />

                <h3>Here is a list of things you will need!</h3>
                <ul><li>A linux server with root access (Only tested on debian/ubuntu)</li>
                    <li>Apache, MySQL, PHP ( your probably already past this point)</li>
                    <li>lib32gcc1 this is so steamcmd can run on 64 bit systems (apt-get install lib32gcc1) </li>
                    <li>zip/unzip (apt-get install zip && apt-get install unzip)</li>
                </ul>

                <h3>Here are some optional things</h3>
                <ul><li>password less sudo for your apache user with access to kill and netstat</li>
                </ul>

                <h3>What not to do</h3>
                Do <u>not</u> install this on a shared hosting webserver. While it will probably to some extent work you hosting provider will probably get really mad at you, and they probably kill any process that runs longer than 30 seconds.
                <h3>Permissions</h3>
                <ol>
                    <li>Recommended setup, Create a new linux user (adduser steam) </li>
                    <li>Edit your apache config to run apache as new linux user. restart apache (service apache2 restart)</li>
                    <li>Give your new linux user passwordless sudo for tthe kill and netstat commands </li>
                    <li>Reload this webpage</li>
                    <li>Proceed with install</li>
                </ol>


                There are otherways to handle the apache user and permissions for this system but the above one is the simpleist
                <br />
                <a href='page1.php' class='actionBtn'>Click here to continue</a>
            </div>
        </div>
</html>
