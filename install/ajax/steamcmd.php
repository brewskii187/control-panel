<?php

/**
 * Downloads/installs/updates steamcmd
 * This will be used in the setup file for the initial install
 * $installDir will be set by an html form. not a hard coded variable.
 *
 * apt-get install lib32gcc1
 * dependency needed for steamcmd ubuntu
 */
if (!isset($_POST['installCmd'])) {
    die();
}
if (!defined('NineteeEleven')) {
    define('NineteenEleven', true);
}
require_once '../../configs/config.php';
$installDir = homeDir . "steamcmd/";


if (is_dir($installDir)) {
    die('Install directory already exists!');
}

$steamCmd = "steamcmd_linux.tar.gz";
if (substr($installDir, -1) != '/') {
    $installDir = $installDir . '/';
}

if (!defined('ABSDIR')) {

    $folderName = '/control-panel/';
    $absDir = substr(__DIR__, 0, stripos(__DIR__, $folderName)) . $folderName;
    if ($absDir == $folderName) {
        define('ABSDIR', __DIR__ . '/');
    } else {

        define('ABSDIR', $absDir);
    }
}
require_once ABSDIR . 'configs/config.php';
require_once ABSDIR . 'includes/ClassLoader.php';


$shell = new Shell;

#create the install directory
if (!is_dir($installDir)) {
    mkdir($installDir, 0770);
}
#grab the steamcmd tar ball
$shell->prepareSh("wget -P $installDir http://media.steampowered.com/installer/$steamCmd")->setExitStatus()->execute()->checkExitStatus();

#if we got it, extract it.
if (is_file($installDir . $steamCmd) && $shell->es) {
    $shell->prepareSh("tar -xvzf " . $installDir . $steamCmd . " -C $installDir")->setExitStatus()->execute()->checkExitStatus();
} else {
    echo "Unable to download steamcmd. Please install manually see the <a href='https://developer.valvesoftware.com/wiki/SteamCMD'>install instructions.</a>";
    die();
}

#k extracted?
#run steamcmd.sh to install steamcmd and update it.
if ($shell->es) {
    $shell->prepareSh($installDir . "steamcmd.sh +login anonymous +quit")->setExitStatus()->execute()->checkExitStatus();
} else {
    echo "Trouble extracting steamcmd. Please proceed manually.";
}

#alright steamcmd should be updated now.
#write a log file contining the steamcmd output and search for the login string
if ($shell->es) {
    $logFile = fopen($installDir . "install.log", "w");
    fwrite($logFile, "Steam CMD installed on " . date(DATE_RFC2822) . ", by NineteenEleven\'s Control-Panel. \r\n");
    foreach ($shell->response as $text) {
        //echo "$text <br />";
        fwrite($logFile, $text . "\r\n");
        if (stristr($text, 'Steam Console Client') !== false) {
            $updated = true;
        }
    }
}

#delete the tar ball
unlink($installDir . $steamCmd);

#let the user know what happened here.
if (isset($updated)) {
    echo "SteamCMD has been installed to $installDir and updated.";
} else {
    echo "Downloaded steamcmd but there was a problem installing it. Please proceed manually.";
}
