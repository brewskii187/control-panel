<?php

if (!isset($_POST['cannabilize'])) {
    die();
}

if (!defined('ABSDIR')) {

    $folderName = '/control-panel/';
    $absDir = substr(__DIR__, 0, stripos(__DIR__, $folderName)) . $folderName;
    if ($absDir == $folderName) {
        define('ABSDIR', __DIR__ . '/');
    } else {

        define('ABSDIR', $absDir);
    }
}
$installDir = ABSDIR . 'install/';

exec("rm -r $installDir");

if (!is_dir($installDir)) {
    echo "<h3>Install Directory removed</h3>
        <p>You will need to add a few items to your crontab (crontab -e), make sure you add them as your apache user, or some problems may arise.</p>
        <ul>
        <li>*/5 * * * * /usr/bin/php " . ABSDIR . "cron/crash-check.php</li>
        <li>0 5 * * * /usr/bin/php " . ABSDIR . "/cron/restarter.php</li>
        <li>@hourly /usr/bin/php " . ABSDIR . "/cron/update-check.php</li>
        </ul>";
} else {
    echo "<h3>Failed to remove install directory</h3>";
    echo "<p>Please do so manually (rm -r $installDir)</p>";
}
