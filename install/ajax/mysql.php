<?php

define('NineteenEleven', true);
require_once '../../includes/MiscClass.php';
require_once '../../includes/FileHandlerClass.php';
$pathing = new Pathing;
if (isset($_POST)) {
    $host = $_POST['host'];
    $user = $_POST['user'];
    $pass = $_POST['pass'];
    $dbname = $_POST['dbname'];
    $dbprefix = $_POST['prefix'];
    $homeDir = $_POST['homeDir'];
    $homedir = $pathing->aTSlash($homeDir);
    $backupsDir = $_POST['backupsDir'];
    $backupsDir = $pathing->aTSlash($backupsDir);
    $salt = Misc::random(8);
    $cpuser = $_POST['cpuser'];
    //$pass = $_POST['cppass'];
    $cppass = sha1(sha1($salt . $_POST['cppass']));
    $cpemail = $_POST['cpemail'];
    $senderName = $_POST['senderName'];
    $senderEmail = $_POST['senderEmail'];
    $dump = $_POST['dump'];

    if (isset($_POST['senderDumpAddy']) && !empty($_POST['senderDumpAddy'])) {
        $dumpAddy = $_POST['senderDumpAddy'];
    } else {
        $dumpAddy = '';
    }
    @$mysqli = new mysqli($host, $user, $pass, $dbname);


    if (empty($mysqli->connect_error)) {

        $tables = array("
CREATE TABLE IF NOT EXISTS `" . $dbprefix . "_backups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `server_name` varchar(64) NOT NULL,
  `srv_path` varchar(128) NOT NULL,
  `backup_path` varchar(128) NOT NULL,
  `compression` varchar(4) NOT NULL,
  `has_pw` int(1) NOT NULL,
  `timestamp` int(32) NOT NULL,
  `username` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;",
            "
CREATE TABLE IF NOT EXISTS `" . $dbprefix . "_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(32) NOT NULL,
  `user` varchar(64) NOT NULL,
  `log` varchar(1000) NOT NULL,
  `timestamp` int(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;",
            "CREATE TABLE IF NOT EXISTS `" . $dbprefix . "_new_servers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appid` int(16) NOT NULL,
  `path` varchar(512) NOT NULL,
  `steamuser` varchar(64) NOT NULL,
  `steampass` varchar(64) NOT NULL,
  `user_name` varchar(32) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created` int(11) NOT NULL DEFAULT '0',
  `email_confirm` varchar(128) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;",
            "CREATE TABLE IF NOT EXISTS `" . $dbprefix . "_servers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appid` varchar(64) NOT NULL,
  `name_friendly` varchar(64) NOT NULL,
  `path` varchar(256) NOT NULL,
  `exec_path` varchar(256) NOT NULL,
  `cmd_line` varchar(256) NOT NULL,
  `cmd_line_hidden` varchar(256) DEFAULT NULL,
  `pid_file` varchar(256) DEFAULT NULL,
  `ip` varchar(16) NOT NULL,
  `port` int(8) NOT NULL,
  `rcon_pass` varchar(64) DEFAULT NULL,
  `steam_inf_path` varchar(128) DEFAULT NULL,
  `steamuser` varchar(64) DEFAULT NULL,
  `steampass` varchar(64) DEFAULT NULL,
  `notes` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `auto_update` int(1) NOT NULL DEFAULT '0',
  `enabled` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  `auto_restart` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;",
            "CREATE TABLE IF NOT EXISTS `" . $dbprefix . "_server_order` (
  `id` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  KEY `id` (`id`),
  CONSTRAINT `id` FOREIGN KEY (`id`) REFERENCES `" . $dbprefix . "_servers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;",
            "CREATE TABLE IF NOT EXISTS `" . $dbprefix . "_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL,
  `password` varchar(256) NOT NULL,
  `permissions` int(32) NOT NULL,
  `enabled` int(1) NOT NULL DEFAULT '0',
  `email` varchar(128) NOT NULL,
  `rec_email` int(1) NOT NULL DEFAULT '1',
  `srvStats` int(1) NOT NULL DEFAULT '1',
  `rconPopOut` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
",
            "INSERT INTO " . $dbprefix . "_users (`username`, `email`, `password`, `permissions`, `rec_email`, `enabled`) VALUES ('$cpuser', '$cpemail', '$cppass', '511', '1', '1')

");

        foreach ($tables as $sql) {
            $mysqli->query($sql)or die($mysqli->error);
            if (empty($mysqli->error)) {

                continue;
            } else {
                die('Error creating tables ' . $mysqli->error);
            }
        }

        echo "Connected to MySQL server and installed tables.<br />";


        $file = "
<?php
if (!defined('NineteenEleven')) {
    die('Direct Access Not Permitted');
}

define('VERSION', '1.0.0');
define('DB_HOST', '" . $host . "');
define('DB_USER', '" . $user . "');
define('DB_PASS', '" . $pass . "');
define('DB_NAME', '" . $dbname . "');

define('DB_PREFIX', '" . $dbprefix . "_');
define('SALT', '" . $salt . "');

define('homeDir', '" . $homeDir . "');

define('DEFAULT_LANGUAGE', 'en-us');

define('BACKUPS_DIR', '" . $backupsDir . "');

\$settings['ajax']['ServerRefreshRate'] = '10000'; //10 seconds
\$settings['users']['MinPassLength'] = 8;
\$settings['prefs']['date_format'] = 'n/j/y H:i:s'; //http://www.php.net/manual/en/function.date.php
\$settings['prefs']['log_page'] = 30; //number of log entries per page
//show Server stats module
\$settings['display']['uptime'] = true;
\$settings['display']['ram'] = true;
\$settings['display']['cpu'] = true;
\$settings['display']['load'] = true;
\$settings['file_manager']['extensions']['open'] = array('.txt', '.cfg', '.log', '.sp');
\$settings['file_manager']['maxSize'] = 1048576 * 2; //max file size in bytes to allow viewing. 1048576=1mb
\$settings['file_manager']['allowhidden'] = true; //allow hidden files to be shown in file manager
//dev stuff below
define('DEBUG', false);
define('ALLOW_DL', true); //if false wont actually download from steamcmd just logs in and quits.
if (DEBUG) {
    error_reporting(E_ALL);
    ini_set('display_errors', '1');
}";

        $emailFile = "
        <?php

        if (!defined('NineteenEleven')) {
            die('Direct Access Not Permitted');
        }
        \$mail['name'] = '$senderName'; //senders name
        \$mail['email'] = '$senderEmail'; //senders e-mail adress
        \$mail['adminDump'] = $dump; //send copys of all emails to admin address?
        \$mail['BCC'] = '$dumpAddy'; //admin address
        ";

        $configDir = '../../configs/';

        if (!is_dir($configDir)) {
            if (!mkdir($configDir)) {
                rollback(1);
                die('Error creating home directory');
            }
        }

        if (!is_dir($homeDir)) {
            if (!mkdir($homeDir)) {
                rollback(2);
                die('Error creating home directory');
            }
        }

        if (!is_dir($backupsDir)) {
            if (!mkdir($backupsDir)) {
                rollback(3);
                die('Error creating backups directory');
            }
        }


        if (is_dir($configDir) && is_writable($configDir)) {
            $fp = fopen($configDir . 'config.php', 'w');
            if ($fp) {
                if (fwrite($fp, $file) === false) {
                    rollback(4);
                    die('Error writing config file');
                }
            } else {
                rollback(4);
                die('Error writing config file');
            }
            fclose($fp);
            unset($fp);
            $fp = fopen($configDir . 'email.php', 'w');
            if ($fp) {
                if (fwrite($fp, $emailFile) === false) {
                    rollback(4);
                    die('Error wriing config file');
                }
            } else {
                rollback(4);
                die('Error wriing config file');
            }
        } else {
            rollback(4);
            die("unable to locate or write to $configDir");
        }


        echo "Config files written. There are some other more or less standard options set in the config that you may want to change. See configs/config.php<br />";
        echo "<a href='page2.php'>Proceed to page 2</a>";
        echo "<script>$(document).ready(function(){
                $('#subForm').hide();
              }); </script>";
    } else {
        die("Un-able to connect to MySQL server. " . $mysqli->connect_error);
    }
}

function rollback($num) {
    echo 'rolling back<br />';
    global $mysqli, $dbname, $homeDir, $configDir, $backupsDir;
    if ($num >= 1) {
        $mysqli->query("DROP DATABASE `$dbname`;");
        $mysqli->query("CREATE DATABASE `$dbname`;");
    }
    if ($num >= 2) {

        rmdir($configDir);
    }
    if ($num >= 4) {

        rmdir($homeDir);
    }
    if ($num >= 5) {
        rmdir($backupsDir);
        @unlink($configDir . 'config.php');
        @unlink($configDir . 'email.php');
    }
}
