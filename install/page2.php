<?php
define('NineteenEleven', true);
require_once '../configs/config.php';
require_once '../includes/MiscClass.php';
?>
<html>
    <head>
        <title>NineteenElevens Control Panel</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!--Add some jquery -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

        <!--Jquery UI -->
        <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css" />
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
        <link type="text/css" rel="stylesheet" href="../themes/default/style.css" />
        <style type="text/css">
            .border{
                border:1px solid #333;
                margin:8px 0;
                padding: 10px 10px;
            }
        </style>
    </head>
    <body>
        <div class='wrapper'>
            <div class='contentArea'>

                <?php
                if (!is_dir(homeDir . "steamcmd")) {
                    echo "
                    <div class='border'>
                    <p>Want me to install steam cmd for you?</p>
                    <span onclick='install()' class='actionBtn' id='steaminst'>Click here</span>
                    <p>If not steamcmd must reside in " . homeDir . "steamcmd, you can symlink it.</p>
                    <div id='installResponse'></div>
                </div>
                ";
                }
                ?>
                <div class='border'>
                    <p>lastly you MUST delete this installation folder before using the control panel.</p>
                    <span onclick='del()' id='delBtn' class='actionBtn'>I can also do that for you</span>
                    <div id='deleteResponse'></div>
                </div>
            </div>
        </div>
        <script>

            function install() {
                $(document).ready(function() {
		    $('#steaminst').hide();
                    $('#installResponse').html('Installing It can take a few min. <img src="../images/ajax-loader.gif" class="ajaxLoader">');
                    $.ajax({
                        type: 'POST',
                        url: 'ajax/steamcmd.php',
                        data: {installCmd: 1},
                        success: function(result) {
                            $('#installResponse').html(result);
                        }});
                });
            }
            ;

            function del() {
                $(document).ready(function() {
                    $('#delBtn').hide();
                    $('#deleteResponse').html('<img src="../images/ajax-loader.gif" class="ajaxLoader">');
                    $.ajax({
                        type: 'POST',
                        url: 'ajax/cannabilize.php',
                        data: {cannabilize: 1},
                        success: function(result) {
                            $('#deleteResponse').html(result);
                        }});
                });
            }
            ;
        </script>
    </body>
</html>
