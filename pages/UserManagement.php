<?php

/*
 * the code in this file works, but can be optomized, its not exactly DRY.
 */

if (!defined('HOMEPAGE')) {
    die('No direct access allowed.');
}
if (!defined('NineteenEleven')) {
    define('NineteenEleven', true);
}

$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

if (isset($_POST['delUser'])) {

    $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);

    $sql = "DELETE FROM " . DB_PREFIX . "users WHERE `id`='$id'";

    $mysqli->query($sql);
    if ($mysqli->affected_rows == 1) {
        Misc::uiMsg($lang->userManagement->msg6, 'success');
    } elseif ($mysqli->errno == 0) {
        Misc::uiMsg($lang->userManagement->msg6, 'success');
    } else {
        die($mysqli->error . " " . $mysqli->errno);
    }
}

$filtersRoot = array(
    'id' => FILTER_SANITIZE_NUMBER_INT,
    'username' => FILTER_SANITIZE_STRING,
    'email' => FILTER_SANITIZE_EMAIL,
    'oldPassword' => FILTER_UNSAFE_RAW,
    'password' => FILTER_UNSAFE_RAW,
    'password2' => FILTER_UNSAFE_RAW,
    'permissions' => FILTER_SANITIZE_NUMBER_INT,
    'rec_email' => FILTER_SANITIZE_NUMBER_INT,
    'enabled' => FILTER_SANITIZE_NUMBER_INT
);
if (isset($_POST['NewUserForm']) && $_GET['loc'] == 'UserManagement' && $_SESSION['permissions'] & perms::root) {

    if (!isset($_POST['username']) || !isset($_POST['email']) || !isset($_POST['password']) || !isset($_POST['password2']) || !isset($_POST['permissions'])) {
        die('Please fill out all form fields.'); //die is bad, do something else,
    }
    $form = filter_input_array(INPUT_POST, $filtersRoot);

    $whiteList = array('username', 'email', 'password', 'password2', 'permissions', 'enabled', 'rec_email');

    $form['rec_email'] = Misc::checkboxCheck($form['rec_email']);
    $form['enabled'] = Misc::checkboxCheck($form['enabled']);


    #check the password
    if ($form['password'] !== $form['password2']) {
        Misc::uiMsg($lang->userManagement->err2, 'error');
    } else {
        unset($form['password2']);
    }

    if (strlen($form['password']) < $settings['users']['MinPassLength']) {
        die(Misc::uiMsg(sprintf($lang->userManagement->err1, $settings['users']['MinPassLength']), 'error'));
    }

    #hash the password.
    $form['password'] = sha1(sha1(SALT . $form['password']));

    foreach ($form as $key => $value) {
        if (!in_array($key, $whiteList)) {
            continue;
        }
        $userArray[$key] = $mysqli->escape_string($value);
    }
    unset($userArray['password2']);
    $sql = Misc::insertFromArray($userArray, "users");
    $mysqli->query($sql);

    if ($mysqli->affected_rows == 1) {
        Misc::uiMsg(sprintf($lang->userManagement->msg2, $form['username']), 'success');
    } elseif (stristr($mysqli->error, 'Duplicate entry')) {
        Misc::uiMsg(sprintf($lang->userManagement->err4, $form['email']), 'error');
    } elseif ($mysqli->errno == 0) {
        Misc::uiMsg($lang->userManagement->msg5, 'success');
    } else {
        die($mysqli->error . " " . $mysqli->errno);
    }
}

if (isset($_POST['ChangePEForum']) && $_GET['loc'] == 'UserManagement') {

    $form = filter_input_array(INPUT_POST, $filtersRoot);

    $id = $mysqli->escape_string($_SESSION['id']);
    if (!empty($form['oldPassword'])) {
        if ($form['password'] !== $form['password2']) {
            die(Misc::uiMsg($lang->userManagement->err2, "error"));
        } else {
            unset($form['password2']);
        }
        if (empty($form['rec_email']) || $form['rec_email'] > 1 || $form['rec_email'] < 0) {
            $form['rec_email'] = 0;
        }
        if (strlen($form['password']) < $settings['users']['MinPassLength']) {
            die(Misc::uiMsg(sprintf($lang->userManagement->err1, $settings['users']['MinPassLength']), 'error'));
        }
        $form['password'] = sha1(sha1(SALT . $form['password']));
        $form['oldPassword'] = sha1(sha1(SALT . $form['oldPassword']));

        $sql = "SELECT `password` FROM " . DB_PREFIX . "users WHERE `id` = '$id';";
        $result = $mysqli->query($sql)or die($mysqli->error . " " . $mysqli->errno);
        if ($result->num_rows == 1) {
            $dbPass = $result->fetch_array(MYSQLI_ASSOC);
        } else {
            die('em somehow your user id changed in the session...h4x?');
        }

        if ($form['oldPassword'] !== $dbPass['password']) {
            die($lang->userManagement->err3);
        }
        $sql = sprintf("UPDATE " . DB_PREFIX . "users SET `password`='%s', `email`='%s', `rec_email` ='%s' WHERE `id` = '$id';", $mysqli->escape_string($form['password']), $mysqli->escape_string($form['email']), $mysqli->escape_string($form['rec_email']));
    } else {
        $sql = sprintf("UPDATE " . DB_PREFIX . "users SET `email`='%s', `rec_email` ='%s' WHERE `id` = '$id';", $mysqli->escape_string($form['email']), $mysqli->escape_string($form['rec_email']));
    }
    $mysqli->query($sql);

    if ($mysqli->affected_rows == 1) {
        Misc::uiMsg(sprintf($lang->userManagement->msg3, $_SESSION['username']), 'success');
    } elseif (stristr($mysqli->error, 'Duplicate entry')) {
        Misc::uiMsg(sprintf($lang->userManagement->err4, $form['email']), 'error');
    } elseif ($mysqli->errno == 0) {
        Misc::uiMsg($lang->userManagement->msg5, 'success');
    } else {
        die($mysqli->error . " " . $mysqli->errno);
    }
}

if (isset($_POST['EditUserForm']) && $_GET['loc'] == 'UserManagement' && ($_SESSION['permissions'] & perms::root)) {
    $form = filter_input_array(INPUT_POST, $filtersRoot);
    $id = $form['id'];
    //var_dump($form);
    if (empty($form['enabled']) || is_null($form['enabled'])) {
        $form['enabled'] = "0";
    }
    if ($form['enabled'] < '0' || $form['enabled'] > '1') {
        die('um.. y u mess with da inputs bro?');
    }
    #check if the password is set
    if (!empty($form['password'])) {


        if ($form['password'] !== $form['password2']) {
            Misc::uiMsg($lang->userManagement->err2, 'error');
        } else {
            unset($form['password2']);
        }

        if (strlen($form['password']) < $settings['users']['MinPassLength']) {
            die(Misc::uiMsg(printf($lang->userManagement->err1, $settings['users']['MinPassLength']), 'error'));
        }

        #hash the password.
        $form['password'] = sha1(sha1(SALT . $form['password']));

        $sql = sprintf("UPDATE " . DB_PREFIX . "users SET `password`='%s', `email`='%s',`permissions`='%s',`rec_email`='%s',`enabled`='%s' WHERE `id` = '$id';", $mysqli->escape_string($form['password']), $mysqli->escape_string($form['email']), $mysqli->escape_string($form['permissions']), $mysqli->escape_string($form['rec_email']), $mysqli->escape_string($form['enabled']));
    } else {
        $sql = sprintf("UPDATE " . DB_PREFIX . "users SET `email`='%s',`permissions`='%s',`rec_email`='%s',`enabled`='%s' WHERE `id` = '$id';", $mysqli->escape_string($form['email']), $mysqli->escape_string($form['permissions']), $mysqli->escape_string($form['rec_email']), $mysqli->escape_string($form['enabled']));
    }
    $mysqli->query($sql);
    if ($mysqli->affected_rows == 1) {
        $log->setType('um')->setLog("Edited user ID: $id")->makeLog();
        Misc::uiMsg($lang->userManagement->msg4, 'success');
    } elseif (stristr($mysqli->error, 'Duplicate entry')) {
        Misc::uiMsg(sprintf($lang->userManagement->err4, $form['email']), 'error');
    } elseif ($mysqli->errno == 0) {
        Misc::uiMsg($lang->userManagement->msg5, 'success');
    } else {
        die($mysqli->error . " " . $mysqli->errno);
    }
    header("Location: home.php?loc=UserManagement&userSelect&id=$id");
}

if ($_SESSION['permissions'] & perms::root) {

    $sql = "SELECT `id`,`username` FROM " . DB_PREFIX . "users WHERE 1;";
    $result = $mysqli->query($sql) or die($mysqli->error . " " . $mysqli->errno);

    if ($result->num_rows >= 1) {

        #make combo box
        echo"<select name='appid2' class='comboBox' id='selectUserBox' onchange='selectUser()'><option value='0'>New User</option>";

        while ($user = $result->fetch_array(MYSQLI_ASSOC)) {
            if ($user['id'] == $_GET['id']) {
                printf("<option selected value='%s'>%s</option>", $user['id'], $user['username']);
            } else {
                printf("<option value='%s'>%s</option>", $user['id'], $user['username']);
            }
        }
        echo"</select>";
    } else {
        die("</select>There are no users in the database, how did you get here?");
    }
} else {
    //display change password/email form for $_SESSION['id'];
    $sql = "SELECT `email`,`rec_email` FROM " . DB_PREFIX . "users WHERE `id`='" . $_SESSION['id'] . "';";
    $result = $mysqli->query($sql) or die($mysqli->error . " " . $mysqli->errno);
    if ($result->num_rows >= 1) {
        $user = $result->fetch_array(MYSQLI_ASSOC);
        echo "<form action='" . $_SERVER['PHP_SELF'] . "?loc=UserManagement' method='POST' name='ChangePEForum' class='form'>";

        #change all changeable values of a user.
        printf("<div class='userForm'>%s<input type='text' name='email' value='%s' required></div>", $lang->userManagement->email, $user['email']);
        printf("<div class='userForm'>%s<input type='password' name='oldPassword'></div>", $lang->userManagement->oldP);
        printf("<div class='userForm'>%s<input type='password' name='password'></div>", $lang->userManagement->changeP);
        printf("<div class='userForm'>%s<input type='password' name='password2'></div>", $lang->userManagement->confirmP);
        if ($user['rec_email'] == '1') {
            printf("<div class='userForm'>%s<input type='checkbox' value='1' checked name='rec_email'></div>", $lang->userManagement->recEmail);
        } else {
            printf("<div class='userForm'>%s<input type='checkbox' value='1' name='rec_email'></div>", $lang->userManagement->recEmail);
        }
        echo"<input type='hidden' name='ChangePEForum'><input type='submit' class='actionBtn' value='Edit User'></form>";
    } else {
        die($lang->userManagement->err5);
    }
}



#make new user forum display here, if user is selected from combo box, make form for that user instead.
if ($_GET['loc'] == 'UserManagement' && isset($_GET['userSelect']) && $_SESSION['permissions'] & perms::root) {
    #display edit user form
    $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
    $sql = "SELECT * FROM " . DB_PREFIX . "users WHERE `id`='$id';";
    $result = $mysqli->query($sql) or die($mysqli->error . " " . $mysqli->errno);
    if ($result->num_rows >= 1) {
        $user = $result->fetch_array(MYSQLI_ASSOC);

        printf("<div class='userFormInfo'>" . $lang->userManagement->msg1 . "</div>", $user['username']);

        echo "<form action='" . $_SERVER['PHP_SELF'] . "?loc=UserManagement' method='POST' name='EditUserForm' class='form'>";

        #change all changeable values of a user.
        printf("<div class='userForm'>%s<input type='text' name='email' value='%s' id='userEmail' required></div>", $lang->userManagement->email, $user['email']);

        printf("<div class='userForm'>%s<input type='password' name='password' ></div>", $lang->userManagement->changeP);
        printf("<div class='userForm'>%s<input type='password' name='password2' ></div>", $lang->userManagement->confirmP);

//        printf("<div class='userForm'>%s<input type='text' name='permissions' value='%s' required></div>", $lang->userManagement->perms, $user['permissions']);

printPermissions($user['permissions']);
        if ($user['rec_email'] == '1') {
            printf("<div class='userForm'>%s<input type='checkbox' value='1' checked name='rec_email'></div>", $lang->userManagement->recEmail);
        } else {
            printf("<div class='userForm'>%s<input type='checkbox' value='1' name='rec_email'></div>", $lang->userManagement->recEmail);
        }
        if ($user['enabled'] == '1') {
            printf("<div class='userForm'>%s<input type='checkbox' name='enabled' value='1' checked></div>", $lang->userManagement->enabled);
        } else {
            printf("<div class='userForm'>%s<input type='checkbox' name='enabled' value='1'></div>", $lang->userManagement->enabled);
        }

        echo"<input type='hidden' name='EditUserForm'><input type='hidden' name='id' value='$id'><input type='submit' class='actionBtn' value='Edit User'></form>";

        echo "<form action='" . $_SERVER['PHP_SELF'] . "?loc=UserManagement' method='POST' name='delUserForm' class='form' onsubmit=\"return confirm('" . $lang->userManagement->conf . "');\">";
        echo "<input type='hidden' name='id' value='$id'><input type='submit' name='delUser' value='" . $lang->userManagement->del . "' id='DelUserBtn' class='actionBtn'></form>";
    } else {
        die($lang->userManagement->err5);
    }
} elseif ($_SESSION['permissions'] & perms::root) {
    //new user form here
    echo "<form action='" . $_SERVER['PHP_SELF'] . "?loc=UserManagement' method='POST' name='NewUserForm' class='form'>";

    printf("<div class='userForm'>%s<input type='text' name='username' required></div>", $lang->userManagement->username);
    printf("<div class='userForm'>%s<input type='email' name='email' required></div>", $lang->userManagement->email);
    printf("<div class='userForm'>%s<input type='password' name='password' required></div>", $lang->userManagement->pass);
    printf("<div class='userForm'>%s<input type='password' name='password2' required></div>", $lang->userManagement->confirmP);
    //printf("<div class='userForm'>%s<input type='text' name='permissions' required></div>", $lang->userManagement->perms);i
printPermissions();
    printf("<div class='userForm'>%s<input type='checkbox' value='1' checked name='rec_email'></div>", $lang->userManagement->recEmail);
    printf("<div class='userForm'>%s<input type='checkbox' name='enabled' value='1' checked></div>", $lang->userManagement->enabled);

    echo"<input type='hidden' name='NewUserForm'><input type='submit' class='actionBtn'></form>";
}


$mysqli->close();
unset($mysqli);



function printPermissions($userPerms = null){

print("<div class='permsList'>");
print("<div class='perm'><input type='checkbox' ");
if(!is_null($userPerms) && $userPerms & perms::start){
	print("checked ");
}
print("onclick='setPerms()' name='p1' value='1' />Start</div>");


print("<div class='perm'><input type='checkbox' ");
if(!is_null($userPerms) && $userPerms & perms::stop){
	print("checked ");
}
print("onclick='setPerms()' name='p2' value='2' />Stop</div>");


print("<div class='perm'><input type='checkbox' ");
if(!is_null($userPerms) && $userPerms & perms::update){
	print("checked ");
}
print("onclick='setPerms()' name='p3' value='4' />Update</div>");


print("<div class='perm'><input type='checkbox' ");
if(!is_null($userPerms) && $userPerms & perms::edit){
	print("checked ");
}
print("onclick='setPerms()' name='p4' value='8' />Edit</div>");


print("<div class='perm'><input type='checkbox' ");
if(!is_null($userPerms) && $userPerms & perms::create){
	print("checked ");
}
print("onclick='setPerms()' name='p5' value='16' />Create</div>");


print("<div class='perm'><input type='checkbox' ");
if(!is_null($userPerms) && $userPerms & perms::delete){
	print("checked ");
}
print("onclick='setPerms()' name='p6' value='32' />Delete</div>");


print("<div class='perm'><input type='checkbox' ");
if(!is_null($userPerms) && $userPerms & perms::schedule){
	print("checked ");
}
print("onclick='setPerms()' name='p7' value='64' />Schedule</div>");


print("<div class='perm'><input type='checkbox' ");
if(!is_null($userPerms) && $userPerms & perms::rcon){
	print("checked ");
}
print("onclick='setPerms()' name='p8' value='128' />Rcon</div>");


print("<div class='perm'><input type='checkbox' ");
if(!is_null($userPerms) && $userPerms & perms::root){
	print("checked ");
}
print("onclick='setPerms()' id='rootCheckBox' value='511' />Root</div>");


print("<input type='hidden' id='perms' name='permissions' value='0' />");
print("</div>");
}
