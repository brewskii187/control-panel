<?php

/*
  ALTER TABLE dasf_server_order
  ADD CONSTRAINT id
  FOREIGN KEY (id) REFERENCES dasf_servers(id)
  ON UPDATE CASCADE
  ON DELETE CASCADE;
 */
if (!($_SESSION['permissions'] & perms::root)) {
    die('You do not have permissions to visit this page.');
}
$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
$srvOrderSql = $mysqli->query("SELECT * FROM `" . DB_PREFIX . "server_order` WHERE 1 ORDER BY `order`;")or die($mysqli->error . " " . $mysqli->errno);
$srvNamesSql = $mysqli->query("SELECT id,name_friendly FROM `" . DB_PREFIX . "servers` WHERE 1;")or die($mysqli->error . " " . $mysqli->errno);
$mysqli->close();
while ($srvNames = $srvNamesSql->fetch_array(MYSQLI_ASSOC)) {
    $names[$srvNames['id']] = $srvNames['name_friendly'];
}

while ($srvOrder = $srvOrderSql->fetch_array(MYSQLI_ASSOC)) {
    $order[$srvOrder['order']] = $srvOrder['id'];
}
//var_dump($names);

echo '<ul id="sortable" class="srvOrderList">';

foreach ($order as $server => $id) {
    echo "<li id='$id'class='ui-state-default'><span class='ui-icon ui-icon-arrowthick-2-n-s'></span>";
    echo $names[$id] . "</li>";
}
echo '</ul>';
echo '<div id="info">Drag and drop to change the order the servers will display on the main menu.</div>';
