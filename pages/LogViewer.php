<?php

if (!defined('HOMEPAGE')) {
    die('No direct access allowed.');
}
if (!defined('NineteenEleven')) {
    define('NineteenEleven', true);
}
if (!($_SESSION['permissions'] & perms::rcon)) {
    die('You do not have permissions to visit this page.');
}
$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

//Find out who many sql rows we have
$countSQL = $mysqli->query("SELECT COUNT(*) FROM `" . DB_PREFIX . "logs`;");

$count = $countSQL->fetch_array(MYSQLI_ASSOC);
$numLogs = $count['COUNT(*)'];
//if there is more than on page
if ($numLogs >= $settings['prefs']['log_page']) {
    //set the number of pages
    $pages = floor($numLogs / $settings['prefs']['log_page']);
}
//if user wants to view a specific page.
if (isset($_GET['page'])) {
    $page = filter_input(INPUT_GET, 'page', FILTER_SANITIZE_NUMBER_INT);
    //set the sql rows for given page
    $p1 = $page * $settings['prefs']['log_page'];
    $p2 = ($page * $settings['prefs']['log_page']) + $settings['prefs']['log_page'];
} else {
    //otherwise use the first rows
    $p1 = 0;
    $p2 = $settings['prefs']['log_page'];
}
//if the user is searching for something
if (isset($_GET['searchInput'])) {
    $search = filter_input(INPUT_GET, 'searchInput', FILTER_SANITIZE_SPECIAL_CHARS);
    $search = $mysqli->escape_string($search);
    $sql = "SELECT * FROM `" . DB_PREFIX . "logs` WHERE `log` LIKE '%$search%' OR `user` LIKE '%$search%' ORDER BY id DESC LIMIT $p1,$p2;";
} else {
    //else just get the pages the user wants
    $sql = "SELECT * FROM `" . DB_PREFIX . "logs` WHERE 1 ORDER BY id DESC LIMIT $p1,$p2;";
}
$result = $mysqli->query($sql)or die($mysqli->error . " " . $mysqli->errno);
echo '<div id="searchBox"><form action="home.php"  method="GET" id="searchForm">'
 . '<input type="text" size="30" id="searchInput" name="searchInput" />'
 . '<input type=\'hidden\' name=\'loc\' value=\'LogViewer\'>'
 . '<input type="image" src="images/search-button.png" id="searchButton" form="searchForm" /></form>';

//pages breadcrumbs
if (isset($pages)) {
    $i = 0;
    echo "<div class='pageSelect'>Page:";
    while ($i <= $pages) {
        $pgDsp = $i + 1; //pages start at 0 but are viewed as 1 (such user friendly)
        if (isset($search)) {
            echo "<a href='?loc=LogViewer&page=$i&searchInput=$search' class='inline pageNum'>$pgDsp</a>";
        } else {
            echo "<a href='?loc=LogViewer&page=$i' class='inline pageNum'>$pgDsp</a>";
        }
        $i++;
    }
    echo "</div>";
}

echo "<div class='logs'>";

//guess we should show the user the logs
echo "<table border='0' class='logList'>";
while ($row = $result->fetch_array(MYSQLI_ASSOC)) {

    $type = $row['type']; 
    if (is_numeric($type) && !isset($servers[$row['type']])) { //server logs are stored as their ID. so if the logs is for a server, and the server isnt in our cached array ($servers)
	//get the servers name
        $srv = $mysqli->query("SELECT `name_friendly` FROM `" . DB_PREFIX . "servers` WHERE id = $type;")or die($mysqli->error . " " . $mysqli->errno);


        if ($srv) {
            $srvRes = $srv->fetch_array(MYSQLI_ASSOC);
            $srvName = $srvRes['name_friendly'];
            //put it in our cache array
            $servers[$row['type']] = $srvRes['name_friendly'];
        }
    } elseif (is_numeric($type)) {
        $srvName = $servers[$row['type']];
    }

    echo "<tr><td>" . date($settings['prefs']['date_format'], $row['timestamp']) . "</td><td>" . $row['user'] . "</td>";

    if (isset($srvName)) {
        echo "<td>$srvName</td>";
        unset($srvName);
    } else {
        echo "<td>&nbsp</td>";
    }

    echo "<td>" . $row['log'] . "</td></tr>";
}
echo "</table></div>"; //logs div
$mysqli->close();
