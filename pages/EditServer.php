<?php

if (!defined('HOMEPAGE')) {
    die('No direct access allowed.');
}
if (!defined('NineteenEleven')) {
    define('NineteenEleven', true);
}
if (!($_SESSION['permissions'] & perms::edit)) {
    die('You do not have permissions to visit this page.');
}
if (!defined('ABSDIR')) {

    $folderName = '/control-panel/';
    $absDir = substr(__DIR__, 0, stripos(__DIR__, $folderName)) . $folderName;
    if ($absDir == $folderName) {
        define('ABSDIR', __DIR__ . '/');
    } else {

        define('ABSDIR', $absDir);
    }
}
require_once ABSDIR . 'configs/config.php';
require_once ABSDIR . 'includes/ClassLoader.php';


#javascript to load the server when an option is selected from the drop down box

echo "
<script>
window.onload=function(){
    var combo=document.getElementById('ServerSelector');
    combo.onchange=function(){
        if(this.value!='0') window.location='" . $_SERVER['PHP_SELF'] . "?loc=EditServer&id='+this.value;
    };
};
</script>
";
/**
 * edit server form
 */
#if the form was submitted, update the database with the new info
$pathing = new Pathing;
if (isset($_POST['EditServer']) && isset($_POST['id']) && $_SESSION['permissions'] & perms::edit) {

    $EditForm = filter_input_array(INPUT_POST, FILTER_SANITIZE_SPECIAL_CHARS);

    $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
//    if (empty($EditForm['auto_update']) || $EditForm['auto_update'] > 1 || $EditForm['auto_update'] < 0) {
//        $EditForm['auto_update'] = 0;
//    }

    if (isset($EditForm['auto_update'])) {
        $EditForm['auto_update'] = Misc::checkboxCheck($EditForm['auto_update']);
    } else {
        $EditForm['auto_update'] = 0;
    }

    if (isset($EditForm['auto_restart'])) {
        $EditForm['auto_restart'] = Misc::checkboxCheck($EditForm['auto_restart']);
    } else {
        $EditForm['auto_restart'] = 0;
    }

    if (isset($EditForm['enabled'])) {
        $EditForm['enabled'] = Misc::checkboxCheck($EditForm['enabled']);
    } else {
        $EditForm['enabled'] = 0;
    }
    $sql = "UPDATE `" . DB_PREFIX . "servers` SET "
            . "`name_friendly`='" . $mysqli->escape_string(trim($EditForm['name_friendly'])) . "',"
            . "`appid`='" . $mysqli->escape_string(trim($EditForm['appid'])) . "',"
            . "`path`='" . $mysqli->escape_string(addHomeDir($pathing->rBSlash(trim($EditForm['path'])))) . "',"
            . "`exec_path`='" . $mysqli->escape_string(addHomeDir($pathing->rBSlash(trim($EditForm['exec_path'])))) . "',"
            . "`cmd_line`='" . $mysqli->escape_string(trim($EditForm['cmd_line'])) . "',"
            . "`cmd_line_hidden`='" . $mysqli->escape_string(trim($EditForm['cmd_line_hidden'])) . "',"
            . "`pid_file`='" . $mysqli->escape_string(addHomeDir($pathing->rBSlash(trim($EditForm['pid_file'])))) . "',"
            . "`ip`='" . $mysqli->escape_string(trim($EditForm['ip'])) . "',"
            . "`port`='" . $mysqli->escape_string(trim($EditForm['port'])) . "',"
            . "`steam_inf_path`='" . $mysqli->escape_string(addHomeDir($pathing->rBSlash(trim($EditForm['steam_inf_path'])))) . "',"
            . "`rcon_pass`='" . $mysqli->escape_string(trim($EditForm['rcon_pass'])) . "',"
            . "`steamuser`='" . $mysqli->escape_string(trim($EditForm['steamuser'])) . "',"
            . "`steampass`='" . $mysqli->escape_string(trim($EditForm['steampass'])) . "',"
            . "`auto_update`='" . $mysqli->escape_string(trim($EditForm['auto_update'])) . "',"
            . "`auto_restart`='" . $mysqli->escape_string(trim($EditForm['auto_restart'])) . "',"
            . "`enabled`='" . $mysqli->escape_string(trim($EditForm['enabled'])) . "'"
            . "WHERE `id`='" . $mysqli->escape_string(trim($EditForm['id'])) . "';";
    $mysqli->query($sql) or die($mysqli->error . " " . $mysqli->errno);
    if ($mysqli->affected_rows == 1) {
        //$_SESSION['message'] = $EditForm['name_friendly'] . " " . $lang->editSrv[0]->msg1;
        $log->setType($EditForm['id'])->setLog('edited')->makeLog();
        Misc::uiMsg($EditForm['name_friendly'] . " " . $lang->editSrv[0]->msg1);
    }
    $mysqli->close();
    unset($mysqli);
    header("Location: home.php?loc=EditServer&id=" . $EditForm['id']);
}
$SCC = new ServerControl;
$SCC->makeComboBox('editCombo', 'ServerSelector');


$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
if (isset($_GET['id'])) {
    $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
    $result = $mysqli->query("SELECT * FROM `" . DB_PREFIX . "servers` WHERE `id`='$id';") or die($mysqli->error . " " . $mysqli->errno);
} else {
    $result = $mysqli->query("SELECT * FROM `" . DB_PREFIX . "servers` WHERE 1 LIMIT 0,1;") or die($mysqli->error . " " . $mysqli->errno);
}
echo "<form action='" . $_SERVER['PHP_SELF'] . "?loc=EditServer' method='POST' name='editServerForm' class='form'>";


if ($result && $_SESSION['permissions'] & perms::edit) {
    $server = $result->fetch_array(MYSQLI_ASSOC);
    $id = $server['id'];
    printf("<div class='srvInput'>" . $lang->srvForm->name_friendly . "<input type='text' id='srvNameFriendly' maxlength='64' name='name_friendly' value='%s' required></div>", $server['name_friendly']);
    printf("<div class='srvInput'>" . $lang->srvForm->appid . "<input type='text' id='srvAppid' maxlength='64' name='appid' value='%s' required></div>", $server['appid']);
    printf("<div class='srvInput'>" . $lang->srvForm->path . " " . homeDir . "<input type='text' id='srvPath' class='srvPathInput' maxlength='256' name='path' value='%s' required></div>", removeHomeDir($server['path']));
    printf("<div class='srvInput'>" . $lang->srvForm->exec_path . " " . homeDir . "<input type='text' id='srvExecPath' class='srvPathInput' maxlength='256' name='exec_path' value='%s' required></div>", removeHomeDir($server['exec_path']));

    //printf("<div class='srvInput'>". $lang->srvForm->cmd_line."<input type='text' id='srvCmdLine' maxlength='256' name='cmd_line' value='%s'></div>", $server['cmd_line']);
    printf("<div class='srvInput'>" . $lang->srvForm->cmd_line . "<textarea id='srvCmdLine' class='cmdLine' maxlength='256' name='cmd_line'>%s</textarea></div>", $server['cmd_line']);

    //printf("<div class='srvInput'>". $lang->srvForm->cmd_line_hidden."<input type='text' id='srvCmdLineHidden' maxlength='256' name='cmd_line_hidden' value='%s'></div>", $server['cmd_line_hidden']);
    printf("<div class='srvInput'>" . $lang->srvForm->cmd_line_hidden . "<textarea id='srvCmdLineHidden' class='cmdLine' maxlength='256' name='cmd_line_hidden'>%s</textarea></div>", $server['cmd_line_hidden']);

    printf("<div class='srvInput'>" . $lang->srvForm->pid_file . " " . homeDir . "<input type='text' id='srvPidFile' class='srvPathInput' maxlength='256' name='pid_file' value='%s'></div>", removeHomeDir($server['pid_file']));
    printf("<div class='srvInput'>" . $lang->srvForm->ip . "<input type='text' id='srvIp' maxlength='16' name='ip' value='%s'></div>", $server['ip']);
    printf("<div class='srvInput'>" . $lang->srvForm->port . "<input type='text' id='srvPort' maxlength='8'name='port' value='%s'></div>", $server['port']);
    printf("<div class='srvInput'>" . $lang->srvForm->infPath . " " . homeDir . "<input type='text' id='srvInfPath' class='srvPathInput' maxlength='128' name='steam_inf_path' value='%s'></div>", removeHomeDir($server['steam_inf_path']));
    printf("<div class='srvInput'>" . $lang->srvForm->rconPass . "<input type='text' id='srvRconPass' maxlength='64' name='rcon_pass' value='%s'></div>", $server['rcon_pass']);
    printf("<div class='srvInput'>" . $lang->createSrv->msg4 . "<input type='text' id='srvSteamUser' maxlength='64' name='steamuser' value='%s'></div>", $server['steamuser']);
    printf("<div class='srvInput'>" . $lang->createSrv->msg5 . "<input type='password' id='srvSteamPass' maxlength='64' name='steampass' value='%s'></div>", $server['steampass']);
    if ($server['auto_update'] == '1') {
        print("<div class='srvInput'>" . $lang->srvForm->autoUpdate . "<input type='checkbox' id='srvAutoUpdate' name='auto_update' value='1' checked></div>");
    } else {
        print("<div class='srvInput'>" . $lang->srvForm->autoUpdate . "<input type='checkbox' id='srvAutoUpdate' name='auto_update' value='1'></div>");
    }
    if ($server['auto_restart'] == '1') {
        print("<div class='srvInput'>" . $lang->srvForm->autoRestart . "<input type='checkbox' id='srvAutoRestart' name='auto_restart' value='1' checked></div>");
    } else {
        print("<div class='srvInput'>" . $lang->srvForm->autoRestart . "<input type='checkbox' id='srvAutoRestart' name='auto_restart' value='1'></div>");
    }
    if ($server['enabled'] == '1') {
        print("<div class='srvInput'>" . $lang->srvForm->enabled . "<input type='checkbox' id='srvEnabled' name='enabled' value='1' checked></div>");
    } else {
        print("<div class='srvInput'>" . $lang->srvForm->enabled . "<input type='checkbox' id='srvEnabled' name='enabled' value='1'></div>");
    }
    //printf("<div class='srvInput'>" . $lang->srvForm->enabled . "<input type='text' id='srvEnabled' name='enabled' value='%s'></div>", $server['enabled']);
    printf("<input type='hidden' name='id' value='%s'>", $id);
    echo "<input type='hidden' name='EditServer'>";
    echo "<input type='submit' value='" . $lang->srvForm->editSubmit . "' class='actionBtn'></form>";
} elseif ($result && ($_SESSION['permissions'] & perms::create || $_SESSION['permissions'] & perms::delete)) {
    #user cant edit but can copy or delete, get the server id for the copy/delete

    echo "<div class='error'>" . $lang->perms->err1 . " " . $lang->perms->edit . "</div>";
    $server = $result->fetch_array(MYSQLI_ASSOC);
    $id = $server['id'];
}
echo "<hr />";
/**
 * copy server form
 */
if ($_SESSION['permissions'] & perms::create) {

    echo "<div class='srvInput cpySrv'><label for='copyPath'>" . $lang->srvForm->copy . " </label>" . homeDir . "<input type='text' id='copyPath' maxlength='256' name='copyPath'>";
    echo "<div class='actionBtn' id='srvCtrlCopy' onclick='srvCtrlAjaxCopy($id,\"copy\",\"$token\")'>Copy Server</div></div>";
    echo "<script>
 function srvCtrlAjaxCopy(varId,varAction,varToken){
    var varNPath = document.getElementById('copyPath').value;
    if (varNPath ==''){
    return;
    };
    var timeDelay = 3000;
$(document).ready(function(){
      $('#messageArea').show().html('<img src=\"images/ajax-loader.gif\" class=\"ajaxLoader\"><div class=\"ajaxInfo upperCase\">Currently executing operation: '+varAction+'</div>');
      $.ajax({
          type: 'POST',
          url:'ajax/server-control.php',
          data:{token:varToken,id:varId,action:varAction,nPath:varNPath,ajax:1},
          success:function(result){
        $('#messageArea').html(result);
      }});
});
};

</script>";
}
/**
 * Delete server form
 */
if (isset($_POST['deleteServerForm']) && isset($_POST['id']) && $_SESSION['permissions'] & perms::delete) {
    $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
    $timer['start'] = date('U');
    if ($SCC->delete($id)) {
        $timer['stop'] = date('U');
        Misc::uiMsg("Your server was deleted it took " . Misc::timeConvert($timer), 'success');
    } else {

        Misc::uiMsg("failed to delete server", Misc::timeConvert($timer), 'error');
    }
}
if ($_SESSION['permissions'] & perms::delete) {
    echo "<form action='" . $_SERVER['PHP_SELF'] . "?loc=EditServer' method='POST' name='deleteServerForm' class='form' onsubmit=\"return confirm('" . $lang->srvForm->deleteConf . "');\">";
    echo "<input type='hidden' name='id' value='$id'>";
    echo "<input type='hidden' name='deleteServerForm'>";
    echo "<input type='submit' value='" . $lang->srvForm->deleteSubmit . "' class='actionBtn delSrv'></form>";
}




unset($SCC);

function removeHomeDir($path) {
    if (stristr($path, homeDir) !== false) {
        return str_ireplace(homeDir, '', $path);
    }
    return $path;
}

function addHomeDir($path) {
    return homeDir . $path;
}
