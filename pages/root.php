<?php

/**
 * Passwordless sudo needs to be set up for your user on this server to properly use this page.
 */
if (!defined('HOMEPAGE')) {
    die('No direct access allowed.');
}
if (!($_SESSION['permissions'] & perms::root)) {
    die('You do not have permissions to visit this page.');
}

$sh = new Shell;


if (isset($_POST['pid'])) {
    $pid = filter_input(INPUT_POST, 'pid', FILTER_SANITIZE_NUMBER_INT);
    $sh->prepareSh("sudo kill -9 " . $pid)->sanitize()->setExitStatus()->execute()->checkExitStatus();
    if ($sh->es) {
        $check = $sh->isRunning($pid);
        if ($check === false) {
            Misc::uiMsg("Killed Process $pid", "success");
        } else {
            Misc::uiMsg("We were unable to kill process $pid", "error");
        }
    } else {
        Misc::uiMsg("We were unable to kill process $pid", "error");
    }
}











echo "<div id='sSbtn'><span onclick='toggleId(\"rootSrvWrapper\");toggleId(\"sSbtn\")' class='actionBtn'>Show all running servers</span>";
echo "<div>This tool will find all running srcds process based of their TCP listening ports. It will also display if there are any duplicate servers"
 . "running on your system.</div></div>";
/*
 * Start page here
 */
//netstat -tulpn | grep srcds | grep LISTEN | awk '{ print $4,"/",$7; }'
$sh->prepareSh("sudo netstat -tulpn | grep srcds | grep LISTEN | awk '{ print $4,\"/\",$7; }'")->sanitize()->setExitStatus()->execute()->checkExitStatus();
//lsof -i -sTCP:LISTEN -P -n | grep -i srcds | awk ' { print $2"/" $9; } '
//$sh->prepareSh("sudo lsof -i -sTCP:LISTEN -P -n | grep -i srcds | awk ' { print $9\"/\" $2; } '")->sanitize()->setExitStatus()->execute()->checkExitStatus();
//var_dump($sh);
if ($sh->es) {
    $allPaths = array();
    echo "<div id='rootSrvWrapper' style='display:none'>";
    foreach ($sh->response as $process) {
        echo "<div class='rootSrvList'>";
        $proc = explode("/", $process); //192.168.1.22:27111 / 29151/srcds_linux

        $ip = trim($proc[0]);
        $pid = trim($proc[1]);
        $shell = new shell;
        $shell->prepareSh("sudo ls -l /proc/$pid/exe | awk '{ print $(NF); }'")->sanitize()->setExitStatus()->execute()->checkExitStatus();
        $path = $shell->response[0];
        array_push($allPaths, $path);
        unset($shell);

        echo "Pid - $pid <br />";
        echo "IP:port - $ip <br />";
        echo "Process - $path <br />";
        echo "<form action='" . $_SERVER['PHP_SELF'] . "?loc=root' method='POST' name='forceKill'>"
        . "<input type='hidden' value='$pid' name='pid' />"
        . "<input type='submit' class='actionBtn' value='Force Kill' />"
        . "</form>";
        //echo "<span onclick=\"kill('$pid')\" class='actionBtn' >Force Kill</span> ";
        echo "</div>";
    }
    //check $allPaths and see if there is duplicate instances of a server running

    $dupes = array_count_values($allPaths);
    foreach ($dupes as $name => $count) {
        if ($count > 1) {
            echo "<div class='rootDupes error'>Duplicate instanace found at $name</div>";
        }
    }
    echo "</div>"; //rootSrvWrapper
}