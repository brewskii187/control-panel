<?php
if (!defined('HOMEPAGE')) {
    die('No direct access allowed.');
}
if (!defined('NineteenEleven')) {
    define('NineteenEleven', true);
}
if (!($_SESSION['permissions'] & perms::create)) {
    die('You do not have permissions to visit this page.');
}
if (!defined('ABSDIR')) {

    $folderName = '/control-panel/';
    $absDir = substr(__DIR__, 0, stripos(__DIR__, $folderName)) . $folderName;
    if ($absDir == $folderName) {
        define('ABSDIR', __DIR__ . '/');
    } else {

        define('ABSDIR', $absDir);
    }
}

require_once ABSDIR . 'configs/config.php';
require_once ABSDIR . 'includes/ClassLoader.php';


if (isset($_POST['CreateServerForm']) && $_SESSION['permissions'] & perms::create) {

    $user_name = $_SESSION['username'];

    $CreateForm = filter_input_array(INPUT_POST, FILTER_UNSAFE_RAW);

    $user_name = $_SESSION['username'];
    if (isset($CreateForm['confEmail'])) {
        $confEmail = $CreateForm['confEmail'];
        #extra checks to make sure some idiot didnt mess with the inputs
        if (!empty($confEmail) && $confEmail == 1) {
            $confEmail = $_SESSION['email'];
        } else {
            $confEmail = 0;
        }
    } else {
        $confEmail = 0;
    }

    if (empty($CreateForm['appid']) || empty($CreateForm['path']) || empty($CreateForm['SteamUser'])) {
        die(printf("<div class='error'>%s</div>", $lang->createSrv->err1));
    }


    $pathing = new Pathing;
    #set absolute path
    $CreateForm['path'] = $pathing->makeAbs($CreateForm['path']);

    if (!empty($CreateForm['exec_path']) || !is_null($CreateForm['exec_path'])) {
        $CreateForm['exec_path'] = homeDir . $pathing->rBSlash($CreateForm['exec_path']);
    }

    if (!empty($CreateForm['pid_file']) || !is_null($CreateForm['pid_file'])) {
        $CreateForm['pid_file'] = homeDir . $pathing->rBSlash($CreateForm['pid_file']);
    }

    if (!empty($CreateForm['steam_inf_path']) || !is_null($CreateForm['steam_inf_path'])) {
        $CreateForm['steam_inf_path'] = homeDir . $pathing->rBSlash($CreateForm['steam_inf_path']);
    }
    #make the directories
    if (!file_exists($CreateForm['path']) and ! is_dir($CreateForm['path'])) {
        if (!mkdir($CreateForm['path'], 0770, true)) {
            die('Failed to create folders...');
        }
        #write a test file.
        $file = fopen($CreateForm['path'] . 'new_server.txt', 'w+');
        fwrite($file, $CreateForm['appid'] . "\n" . $CreateForm['path'] . "\n" . $CreateForm['SteamUser'] . "\n" . $CreateForm['SteamPass']);
        fclose($file);
    }
    if (isset($CreateForm['auto_update'])) {
        $CreateForm['auto_update'] = Misc::checkboxCheck($CreateForm['auto_update']);
    } else {
        $CreateForm['auto_update'] = '0';
    }
    if (isset($CreateForm['auto_restart'])) {
        $CreateForm['auto_restart'] = Misc::checkboxCheck($CreateForm['auto_restart']);
    } else {
        $CreateForm['auto_restart'] = '0';
    }
    if (isset($CreateForm['enabled'])) {
        $CreateForm['enabled'] = Misc::checkboxCheck($CreateForm['enabled']);
    } else {
        $CreateForm['enabled'] = '0';
    }
    #send info to mysql.
    $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    $sql = "INSERT INTO `" . DB_PREFIX . "new_servers` (`appid`,`path`,`steamuser`,`steampass`,`user_name`,`email_confirm`)VALUES('"
            . $mysqli->escape_string(trim($CreateForm['appid'])) . "','"
            . $mysqli->escape_string(trim($CreateForm['path'])) . "','"
            . $mysqli->escape_string(trim($CreateForm['SteamUser'])) . "','"
            . $mysqli->escape_string(trim($CreateForm['SteamPass'])) . "','"
            . $mysqli->escape_string(trim($user_name)) . "','"
            . $mysqli->escape_string(trim($confEmail)) . "');";
    if ($CreateForm['dlSrv'] == '1') {
        $mysqli->query($sql) or die($mysqli->error . " " . $mysqli->errno);
    }
    $sql = "INSERT INTO `" . DB_PREFIX . "servers` (`appid`,`name_friendly`,`path`,`exec_path`,`cmd_line`,`cmd_line_hidden`,`pid_file`,`ip`,`port`,`steam_inf_path`,`rcon_pass`,`steamuser`,`steampass`,`auto_update`,`auto_restart`,`enabled`)VALUES('"
            . $mysqli->escape_string(trim($CreateForm['appid'])) . "','"
            . $mysqli->escape_string(trim($CreateForm['name_friendly'])) . "','"
            . $mysqli->escape_string(trim($CreateForm['path'])) . "','"
            . $mysqli->escape_string(trim($CreateForm['exec_path'])) . "','"
            . $mysqli->escape_string(trim($CreateForm['cmd_line'])) . "','"
            . $mysqli->escape_string(trim($CreateForm['cmd_line_hidden'])) . "','"
            . $mysqli->escape_string(trim($CreateForm['pid_file'])) . "','"
            . $mysqli->escape_string(trim($CreateForm['ip'])) . "','"
            . $mysqli->escape_string(trim($CreateForm['port'])) . "','"
            . $mysqli->escape_string(trim($CreateForm['steam_inf_path'])) . "','"
            . $mysqli->escape_string(trim($CreateForm['rcon_pass'])) . "','"
            . $mysqli->escape_string(trim($CreateForm['SteamUser'])) . "','"
            . $mysqli->escape_string(trim($CreateForm['SteamPass'])) . "','"
            . $mysqli->escape_string(trim($CreateForm['auto_update'])) . "','"
            . $mysqli->escape_string(trim($CreateForm['auto_restart'])) . "','"
            . $mysqli->escape_string(trim($CreateForm['enabled'])) . "');";

    $mysqli->query($sql) or die($mysqli->error . " " . $mysqli->errno);

    $dbId = $mysqli->insert_id;
    $ord = $mysqli->query("SELECT `order` FROM " . DB_PREFIX . "server_order WHERE 1 ORDER BY `order` DESC LIMIT 0,1;");
    $num = $ord->fetch_array(MYSQLI_ASSOC);
    $order = $num['order'] + 1;
    $mysqli->query("INSERT INTO " . DB_PREFIX . "server_order(`id`,`order`) values ('$dbId','$order');");
    $mysqli->close();
    unset($mysqli);

    printf("<div class='success'>%s, %s", $user_name, $lang->createSrv->msg1);
    if ($confEmail !== 0) {
        printf("%s", $lang->createSrv->msg2);
    }
    echo"</div>";
    ;
    if ($CreateForm['dlSrv'] == '1') {
        $shell = new Shell;
        $shell->prepareSh("php cron/newserver.php")->sanitize()->execute_bg();
        unset($shell);
    }
}
?>

<form action='<?php echo $_SERVER['PHP_SELF']; ?>?loc=CreateServer' method='POST' name='CreateServerForm' class='form'>
    <div class='srvInput'>
        <div class='srvRadio'>
            <input type='radio' name='dlSrv' value='1' required><?php echo $lang->createSrv->msg11; ?>
        </div>
        <div class='srvRadio'>
            <input type='radio' name='dlSrv' value='0' required><?php echo $lang->createSrv->msg12; ?>
        </div>

        <?php print($lang->createSrv->msg7); ?>:<select name='appid2' class="comboBox" id="installAppIdBox" onclick="checkAppId()">
        <?php
        foreach ($appInfo as $game) {
            if (isset($game['steamCmdOptions'])) {
                echo "<option value='" . $game['srvAppId'] . " " . $game['steamCmdOptions'] . "'>" . $game['game'] . "</option>";
            } else {
                echo "<option value='" . $game['srvAppId'] . "'>" . $game['game'] . "</option>";
            }
        }
        echo "</select><br />";
        print("<div class='srvInput' id='srvAppId' style='display:none'>" . $lang->srvForm->appid . "<input type='text' id='srvAppIdInput' maxlength='64' name='appid'></div>");
        print("<div class='srvInput'>" . $lang->srvForm->name_friendly . "<input type='text' id='srvNameFriendly' maxlength='64' name='name_friendly' required></div>");
        print("<div class='srvInput'>" . $lang->createSrv->msg4 . "<input type='text' value='anonymous' name='SteamUser' required></div>");
        print("<div class='srvInput'>" . $lang->createSrv->msg5 . "<input type='password' name='SteamPass'></div>");
        print("<div class='srvInput'>" . $lang->createSrv->msg6 . "<input type='checkbox' checked value='1' name='confEmail'></div>");


        print("<div class='srvInput'>" . $lang->createSrv->msg10 . "</div>");
        print("<div class='srvInput'>" . $lang->srvForm->path . " " . homeDir . "<input type='text' id='srvPath' class='srvPathInput' maxlength='256' name='path' Onkeyup='autoFill()' required></div>");

        //print("<div class='srvInput'>" . $lang->createSrv->msg9 . "<input type='checkbox' id='dspExtaInfo' onclick='togExtraInfo()'></div>");
        print("<div id='CreateSrvInfo'><hr /><div class='CreateSrvInfoText'>" . $lang->createSrv->msg8 . "</div>");

        print("<div class='srvInput'>" . $lang->srvForm->exec_path . " " . homeDir . "<input type='text' id='srvExecPath' class='srvPathInput' maxlength='256' name='exec_path' ></div>");
        print("<div class='srvInput'>" . $lang->srvForm->pid_file . " " . homeDir . "<input type='text' id='srvPidFile' class='srvPathInput' maxlength='256' name='pid_file' Onkeyup='makeCmdLine()'></div>");
        print("<div class='srvInput'>" . $lang->srvForm->ip . "<input type='text' id='srvIp' maxlength='16' name='ip' value='" . $_SERVER['SERVER_ADDR'] . "' Onkeyup='makeCmdLine()'></div>");
        print("<div class='srvInput'>" . $lang->srvForm->port . "<input type='text' id='srvPort' maxlength='8'name='port' Onkeyup='makeCmdLine()'></div>");
        print("<div class='srvInput'>" . $lang->srvForm->infPath . " " . homeDir . "<input type='text' id='srvInfPath' class='srvPathInput' maxlength='128' name='steam_inf_path'></div>");
        print("<div class='srvInput'>" . $lang->srvForm->rconPass . "<input type='text' id='srvRconPass' maxlength='64' name='rcon_pass' Onkeyup='makeCmdLine()'></div>");

        print("<div class='srvInput'>" . $lang->srvForm->cmd_line . "<textarea id='srvCmdLine' class='cmdLine' maxlength='256' name='cmd_line'></textarea></div>");
        print("<div class='srvInput'>" . $lang->srvForm->cmd_line_hidden . "<textarea id='srvCmdLineHidden' class='cmdLine' maxlength='256' name='cmd_line_hidden'></textarea></div>");

        print("<div class='srvInput'>" . $lang->srvForm->autoUpdate . "<input type='checkbox' value='1' id='auto_update' name='auto_update'></div>");
        print("<div class='srvInput'>" . $lang->srvForm->autoRestart . "<input type='checkbox' value='1' id='auto_restart' name='auto_restart'></div>");
        print("<div class='srvInput'>" . $lang->srvForm->enabled . "<input type='checkbox' checked value='1' id='srvEnabled' name='enabled'></div>");


        print("</div>"); #CreateSrvInfo closing div
        ?>

            <input type='hidden' name='CreateServerForm'>
            <input type='submit' class='actionBtn'>
            </form>
            <input type='hidden' id='homeDir' value='<?php echo homeDir; ?>'>
            <script type="text/javascript">

//                document.getElementById('CreateSrvInfo').style.display = 'none';
//
//                document.getElementById('srvAppIdInput').value = document.getElementById('installAppIdBox').value;
                checkAppId();
            </script>


