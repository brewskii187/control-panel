<?php

if (!defined('HOMEPAGE')) {
    die('No direct access allowed.');
}
if (!defined('NineteenEleven')) {
    define('NineteenEleven', true);
}
if (!defined('ABSDIR')) {

    $folderName = '/control-panel/';
    $absDir = substr(__DIR__, 0, stripos(__DIR__, $folderName)) . $folderName;
    if ($absDir == $folderName) {
        define('ABSDIR', __DIR__ . '/');
    } else {

        define('ABSDIR', $absDir);
    }
}
require_once ABSDIR . 'configs/config.php';
require_once ABSDIR . 'includes/ClassLoader.php';
require_once ABSDIR . 'GameQ/GameQ.php';

if (isset($_GET['srvControl']) && isset($_GET['id'])) {




    $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);

    $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    $gq = new GameQ();

    $result = $mysqli->query("SELECT * FROM `" . DB_PREFIX . "servers` WHERE `id` = '$id';")or die($mysqli->error . " " . $mysqli->errno);

    if ($result->num_rows >= 1) {
        $server = $result->fetch_array(MYSQLI_ASSOC);
    } else {
        #cant find server id
        $_SESSION['message'] = sprintf($lang->srvControl[0]->startFailed2, $id);
        header('Location: home.php');
        die();
    }

    $lock = new LockFile($server['path']);
    $lockCheck = $lock->check();
    $gqQuery = array(
        array('id' => $server['name_friendly'],
            'type' => Misc::getGameTypeGQ($server['appid']),
            //'type' => 'source',
            'host' => $server['ip'] . ":" . $server['port'])
    );

    $gqResult = $gq->clearServers()->addServers($gqQuery)->requestData();

    if (isset($_POST['lock'])) {
        if ($gqResult[$server['name_friendly']]['gq_online']) {
            Misc::uiMsg("Please stop the server before locking it", "error");
        } else {
            $lock->lock();
        }
    }
    if (isset($_POST['unlock'])) {
        $lock->unlock();
    }
    if ($gqResult[$server['name_friendly']]['gq_online']) {
        echo "<div class='srvCtrlInfo'><div class='srvCtrlNameFriendly'>" . $server['name_friendly'] . " (" . $server['ip'] . ":" . $server['port'] . ")"
        . "</div><div class='srvCtrlHostName'>" . $gqResult[$server['name_friendly']]['hostname']
        . "</div><div class='srvCtrlNumPlayers'>" . $gqResult[$server['name_friendly']]['map'] . " " . $gqResult[$server['name_friendly']]['num_players']
        . "</div>/<div class='srvCtrlMax_players'>" . $gqResult[$server['name_friendly']]['max_players'] . "</div>";


        echo "</div>";
    } else {
        echo "<div class='srvCtrlInfo'><div class='srvCtrlNameFriendly'>" . $server['name_friendly']
        . "</div><div class='srvCtrlOffline'>(" . $lang->srvControl[0]->offline . ")</div></div>";
    }
    echo "<div class='srvCtrlBtns'>";
    if ($_SESSION['permissions'] & perms::start) {
        echo "<div class='actionBtn' id='srvCtrlStart' onclick='srvCtrlAjax($id,\"start\",\"$token\")'>" . $lang->buttons->start . "</div>";
    }
    if ($_SESSION['permissions'] & perms::stop) {
        echo "<div class='actionBtn' id='srvCtrlStop' onclick='srvCtrlAjax($id,\"stop\",\"$token\")'>" . $lang->buttons->stop . "</div>"
        . "<div class='actionBtn' id='srvCtrlRestart' onclick='srvCtrlAjax($id,\"restart\",\"$token\")'>" . $lang->buttons->restart . "</div>";
    }
    if ($_SESSION['permissions'] & perms::update) {
        echo "<div class='actionBtn' id='srvCtrlUpdate' onclick='srvCtrlAjax($id,\"update\",\"$token\")'>" . $lang->buttons->update . "</div>";
    }
    if ($_SESSION['permissions'] & perms::edit) {
        echo "<div class='actionBtn' id='srvCtrlEdit'><a href='home.php?loc=EditServer&id=$id'>" . $lang->buttons->edit . "</a></div>";
        echo "<div class='actionBtn' id='srvCtrlBackup' onclick='srvCtrlAjax($id,\"backup\",\"$token\")'>" . $lang->buttons->backup . "</div>";
        echo "<div class='lockers'>";
        if ($lockCheck === true) { // if server is locked display unlock button
            $lockUser = $lock->checkLocker();

            echo "<form action='" . self . "' method='POST' name='unlock'>"
            . "<input type='hidden' value='1' name='unlock' />"
            . "<input type='submit' class='actionBtn' value='Unlock'";
            echo "/></form>";
        } else {//display lock button
            echo "<form action='" . self . "' method='POST' name='lock'>"
            . "<input type='hidden' value='1' name='lock' />"
            . "<input type='submit' class='actionBtn' value='Lock' />"
            . "</form>";
        }
        echo "</div>"; // .lockers
    }
    echo"</div>"; #srvCtrlBtns
    unset($lock);
    if ($lockCheck === true) {
        echo "<div class='lockInfo'>locked by " . $lockUser[1] . " at " . date($settings['prefs']['date_format'], (int) $lockUser[0]) . "</div>";
    }

    echo "<input type='hidden' value='" . $server['id'] . "' id='srvId'><input type='hidden' value='$token' id='token'>";
    if ($_SESSION['permissions'] & perms::edit) {
        echo "<br /><textarea rows='3' cols='50' class='srvCtrlNotes' id='notesBox'>" . $server['notes'] . "</textarea>";
    }


    if (isset($gqResult[$server['name_friendly']]['players'])) {
        #if GameQ reports players in the servers, make a table of em.
        echo "<table class='playerList' id='playerList'>";
        foreach ($gqResult[$server['name_friendly']]['players'] as $player) {
            echo "<tr>";
            echo "<td>" . $player['name'] . "</td><td>" . gmdate("H:i:s", $player['time']) . "</td><td>" . $player['score'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";
    }
} else {
    die('Only follow system links!');
}
?>


