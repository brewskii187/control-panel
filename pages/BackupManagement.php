<?php

if (!defined('HOMEPAGE')) {
    die('No direct access allowed.');
}
if (!defined('NineteenEleven')) {
    define('NineteenEleven', true);
}
if (!($_SESSION['permissions'] & perms::create)) {
    die('You do not have permissions to visit this page.');
}
require_once ABSDIR . 'configs/config.php';
require_once ABSDIR . 'includes/ClassLoader.php';


echo "<div id='backupsWindow' style='display:none'></div>";


$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

//$namesSql = "SELECT `server_name`,`id`,`timestamp` FROM `" . DB_PREFIX . "backups` GROUP BY `server_name` ORDER BY `timestamp`";

$namesSql = "SELECT `server_name`,`id`,`timestamp` FROM (SELECT * FROM `" . DB_PREFIX . "backups` WHERE 1 ORDER BY `id` DESC) T2 GROUP BY `server_name` ORDER BY `timestamp` DESC";


$servers = $mysqli->query($namesSql);

if ($servers->num_rows > 0) {

    while ($name = $servers->fetch_array(MYSQLI_ASSOC)) {
        echo "<div class='buList' id='srvBuName" . $name['id'] . "'><a href='#' class='inline' onClick=\"loadBackupList('" . $name['id'] . "','$token')\">" . $name['server_name'] . "</a><div class = 'lastBackup inline'>Last backup:" . date('n/j/Y g:i:s A', $name['timestamp']) . "</div>";
    }
} else {
    echo "no backups";
}




