<?php

if (!defined('HOMEPAGE')) {
    die('No direct access allowed.');
}
if (isset($_GET['file'])) {
    $file = filter_input(INPUT_GET, 'file', FILTER_UNSAFE_RAW);

    $a = stripos($file, BACKUPS_DIR);

    $b = stripos($file, homeDir);

    if ($a === 0 || $b === 0) {

        Misc::makeDl($file);
    } else {
        die("I can't let you do that Michael");
    }
}