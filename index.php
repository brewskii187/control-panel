<!DOCTYPE html>
<?php
/*
 * login page.
 */
if (isset($_POST['loginSubmit'])) {
    $userIP = $_SERVER['REMOTE_ADDR'];
    define('NineteenEleven', TRUE);
    if (!defined('ABSDIR')) {

        $folderName = substr($_SERVER['REQUEST_URI'], 0, strrpos($_SERVER['REQUEST_URI'], "/")) . "/";
        $absDir = substr(__DIR__, 0, stripos(__DIR__, $folderName)) . $folderName;
        if ($absDir == $folderName) {
            define('ABSDIR', __DIR__ . '/');
        } else {

            define('ABSDIR', $absDir);
        }
    }
    require_once ABSDIR . 'configs/config.php';
    require_once ABSDIR . 'includes/ClassLoader.php';

    if (!DEBUG || !is_file(ABSDIR . 'configs/config.php')) {
        if (is_dir(ABSDIR . 'install/')) {
            die('<h1>Please remove the install directory before starting</h1>');
        }
    }


//get user input, esacpe it, and check it.
    $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME)or die($mysqli->error . " " . $mysqli->errno);
    $user_name = $mysqli->real_escape_string(stripslashes($_POST['user_name']));
    $password = $mysqli->real_escape_string(sha1(sha1(SALT . $_POST['password'])));

    $sql = "SELECT * FROM " . DB_PREFIX . "users WHERE username='{$user_name}' and password='{$password}' and enabled = '1';";

    //get number of rows retured from sql query, if there is a row, it must be our user
    $count = 0;
    if ($result = $mysqli->query($sql)or die($mysqli->error . " " . $mysqli->errno)) {
        $count = $result->num_rows;
    }

    if ($count === 1) {
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $permissions = $row['permissions'];
            $email = $row['email'];
            $id = $row['id'];
            $srvStats = $row['srvStats'];
            $rconPopOut = $row['rconPopOut'];
        }
        $mysqli->close();
        session_start();
        $_SESSION['id'] = $id;
        $_SESSION['username'] = $user_name;
        $_SESSION['permissions'] = $permissions;
        $_SESSION['email'] = $email;
        $_SESSION['srvStats'] = $srvStats;
        $_SESSION['rconPopOut'] = $rconPopOut;
        $json = @json_decode(@file_get_contents('http://1911.expert/linux-srcds/version.php'));

        if (!empty($json) && VERSION != $json->version) {

            $_SESSION['message'] = "There is an update available. ";

            if (isset($json->msg)) {
                $_SESSION['message'] .= $json->msg;
            }
        } else {

            $_SESSION['message'] = "Source-Linux Control Panel v" . VERSION . " by<a href='http://1911.expert' target='_BLANK'>NineteenEleven</a>";
        }
        print("<center><h1 class='success'> Welcome back {$user_name} </h1></center>");
        $log->setType('auth')->setLog("Logged in from $userIP")->makeLog();
        print("<script type='text/javascript'> setTimeout('reload()' , 1000)
		function reload(){
			window.location='home.php'
		}</script>");
        exit();
    } else {
        print "<center><h1 class='error'>Wrong Username or Password</h1></center>";
        //$log->logAction("Failed login attempt for user name: $user_name");
        $log->setType('auth')->forceUser('unknown')->setLog("Failed login attempt for user name: $user_name @ $userIP")->makeLog();
    }
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <div id='login'>
            <table width="300" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#CCCCCC">
                <tr>
                <form id="loginSubmit" method="POST" action="index.php">
                    <td>
                        <table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#FFFFFF">
                            <tr>
                                <td colspan="3"><strong>Login </strong></td>
                            </tr>
                            <tr>
                                <td width="78">Username</td>
                                <td width="6">:</td>
                                <td width="294"><input name="user_name" type="text" id="user_name"></td>
                            </tr>
                            <tr>
                                <td>Password</td>
                                <td>:</td>
                                <td><input name="password" type="password" id="password"></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td><input type="submit" name="loginSubmit" value="Login" form='loginSubmit' /><input type='button' id='hideLogin' value='Cancel' /></td>

                            </tr>
                        </table>
                    </td>
                </form>
                </tr>
            </table>
        </div>
    </body>
</html>
