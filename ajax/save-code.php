<?php

session_start();
$self = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$self = explode("/ajax/", $self);
$ref = explode('/home.php', $_SERVER['HTTP_REFERER']);
$search = array('http://', 'https://');
$ref[0] = str_ireplace($search, '', $ref[0]);

if ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {
    //Request identified as ajax request

    if (@isset($_SERVER['HTTP_REFERER']) && $ref[0] == $self[0]) {
        //HTTP_REFERER verification
        if (isset($_SESSION['permissions'])) {
            $permissions = $_SESSION['permissions'];
            session_write_close();

            if (isset($_POST['ajax'])) {
                if (!defined('NineteeEleven')) {
                    define('NineteenEleven', true);
                }
                if (!defined('ABSDIR')) {

                    $folderName = '/control-panel/';
                    $absDir = substr(__DIR__, 0, stripos(__DIR__, $folderName)) . $folderName;
                    if ($absDir == $folderName) {
                        define('ABSDIR', __DIR__ . '/');
                    } else {

                        define('ABSDIR', $absDir);
                    }
                }
                require_once ABSDIR . 'configs/config.php';
                require_once ABSDIR . 'includes/ClassLoader.php';
                $data = filter_input_array(INPUT_POST, FILTER_UNSAFE_RAW);
//var_dump($data);
                if (!is_writeable($data['file'])) {
                    die('Unable to gain write access to ' . $data['file']);
                }
                if (is_dir($data['file'])) {
                    die($data['file'] . ' is a directory!');
                }
                $fh = @fopen($data['file'], 'w');
                if ($fh) {
                    $fw = fwrite($fh, stripslashes($data['code']));
                    if ($fw === false) {
                        die('Unable to write file (err1)');
                    } else {
                        if (fclose($fh)) {
                            $log->setType('fe')->setLog("Edited file " . $data['file'])->makeLog();
                            echo "$fw bytes written.";
                        } else {
                            die('File written but unable to close resouce.');
                        }
                    }
                } else {
                    die('Unable to write file (err2)');
                }
            } else {

                die("ajax not found in POST");
            }
        } else {

            die("Token not found");
        }
    } else {

        die("Ajax request not send from server!");
    }
} else {

    die("Page not requested with ajax");
}
