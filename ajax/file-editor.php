<?php
session_start();
$self = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$self = explode("/ajax/", $self);
$ref = explode('/home.php', $_SERVER['HTTP_REFERER']);
$search = array('http://', 'https://');
$ref[0] = str_ireplace($search, '', $ref[0]);

if ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {
    //Request identified as ajax request

    if (@isset($_SERVER['HTTP_REFERER']) && $ref[0] == $self[0]) {
        //HTTP_REFERER verification
        if (isset($_SESSION['permissions'])) {
            $permissions = $_SESSION['permissions'];
            session_write_close();

            if (isset($_POST['ajax'])) {

                if (!defined('NineteeEleven')) {
                    define('NineteenEleven', true);
                }
                if (!defined('ABSDIR')) {

                    $folderName = '/control-panel/';
                    $absDir = substr(__DIR__, 0, stripos(__DIR__, $folderName)) . $folderName;
                    if ($absDir == $folderName) {
                        define('ABSDIR', __DIR__ . '/');
                    } else {

                        define('ABSDIR', $absDir);
                    }
                }

                require_once ABSDIR . 'configs/config.php';

                $filePath = $_POST['file'];
//$file = $_GET['file']; //for dev

                if (stripos($filePath, homeDir) !== 0) {//make sure no one is trying to get a file they cant have
                    die();
                }

                $file = @fopen($filePath, "r");

                if ($file) {
                    $cmDir = "http://" . $_SERVER['HTTP_HOST'] . '/control-panel/CodeMirror';
                    //<link rel=stylesheet href='$cmDir/doc/docs.css'>
                    //<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js'></script>
                    echo "



<link rel='stylesheet' href='$cmDir/lib/codemirror.css'>
                    <link rel='stylesheet' href='$cmDir/addon/fold/foldgutter.css'>
                    <link rel='stylesheet' href='$cmDir/addon/dialog/dialog.css'>
                    <link rel='stylesheet' href='$cmDir/theme/monokai.css'>
                    <script src='$cmDir/lib/codemirror.js'></script>
                    <script src='$cmDir/addon/search/searchcursor.js'></script>
                    <script src='$cmDir/addon/search/search.js'></script>
                    <script src='$cmDir/addon/dialog/dialog.js'></script>
                    <script src='$cmDir/addon/edit/matchbrackets.js'></script>
                    <script src='$cmDir/addon/edit/closebrackets.js'></script>
                    <script src='$cmDir/addon/comment/comment.js'></script>
                    <script src='$cmDir/addon/wrap/hardwrap.js'></script>
                    <script src='$cmDir/addon/fold/foldcode.js'></script>
                    <script src='$cmDir/addon/fold/brace-fold.js'></script>
                    <script src='$cmDir/mode/javascript/javascript.js'></script>
                    <script src='$cmDir/keymap/sublime.js'></script>

                        <textarea id='code'>";


                    while (($buffer = fgets($file, 4096)) !== false) {

                        echo "$buffer";
                    }

                    if (!feof($file)) {
                        die("Error: unexpected fgets() fail");
                    }
                } else {
                    die("Failed to open $file");
                }

                fclose($file);
                echo "</textarea>";
                ?>
                <style type='text/css'>
                    .CodeMirror {
                        border: 1px solid #eee;
                        height: 90%;
                        text-shadow: none;
                    }
                </style>
                <script>
                    var editor = CodeMirror.fromTextArea(document.getElementById("code"), {
                        lineNumbers: true,
                        mode: "javascript",
                        keyMap: "sublime",
                        autoCloseBrackets: true,
                        //styleActiveLine: true,
                        lineWrapping: true,
                        matchBrackets: true,
                        showCursorWhenSelecting: true,
                        theme: "monokai"
                    });</script>
                <script>
                    function saveCode() {
                        var varFile = "<?php echo $filePath; ?>";
                        var varCode = editor.getValue();
                        //console.log(varCode);
                        $(document).ready(function() {
                            $.ajax({
                                type: 'POST',
                                url: 'ajax/save-code.php',
                                data: {code: varCode, file: varFile, ajax: 1},
                                success: function(result) {
                                    $('#messageArea').show();
                                    $('#messageArea').html(result);
                                    codeChanged = false;
                                },
                                error: function() {
                                    $('#messageArea').html("unable to save file");
                                }
                            });
                        });
                    }
                    ;
                    function saveCloseCode() {
                        saveCode();
                        closeFileEditor();
                    }
                    codeChanged = false;
                    editor.on("change", function() {
                        codeChanged = true;
                    });
                </script>
                <button Onclick='saveCode()' class="actionBtn">Save</button>
                <button Onclick='saveCloseCode()' class='actionBtn'>Save/Close</button>
                <button Onclick='dialog("#commandList")' class="actionBtn">Editor Commands</button>
                <?php
            } else {

                die("ajax not found in POST");
            }
        } else {

            die("Token not found");
        }
    } else {

        die("Ajax request not send from server!");
    }
} else {

    die("Page not requested with ajax");
}
