<?php

session_start();
$self = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$self = explode("/ajax/", $self);
$ref = explode('/home.php', $_SERVER['HTTP_REFERER']);
$search = array('http://', 'https://');
$ref[0] = str_ireplace($search, '', $ref[0]);

if ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {
    //Request identified as ajax request

    if (@isset($_SERVER['HTTP_REFERER']) && $ref[0] == $self[0]) {
        //HTTP_REFERER verification
        if (isset($_SESSION['permissions'])) {
            $permissions = $_SESSION['permissions'];
            session_write_close();
            if (isset($_POST['ajax'])) {
                if (!defined('NineteeEleven')) {
                    define('NineteenEleven', true);
                }
                if (!defined('ABSDIR')) {

                    $folderName = '/control-panel/';
                    $absDir = substr(__DIR__, 0, stripos(__DIR__, $folderName)) . $folderName;
                    if ($absDir == $folderName) {
                        define('ABSDIR', __DIR__ . '/');
                    } else {

                        define('ABSDIR', $absDir);
                    }
                }
                require_once ABSDIR . 'configs/config.php';
                require_once ABSDIR . 'includes/ClassLoader.php';

                $pathing = new Pathing;

                if (isset($_POST['move']) || isset($_POST['delete'])) {

                    $files = json_decode(filter_input(INPUT_POST, 'files', FILTER_UNSAFE_RAW));
                }
                if (isset($_POST['move'])) {

                    $to = array_shift($files);
                    $to = $pathing->makeAbs($to);

                    if (!is_dir($to)) {
                        if (mkdir($to, 0750, true) === false) {
                            die("unable to make the directory $to");
                        }
                    }
//echo "move to $to <br />";
                    $moved = 0;
                    $failed = 0;
                    foreach ($files as $file) {
                        $fName = $pathing->rTSlash($file);
                        $fName = explode('/', $file);
                        $fName = array_pop($fName);
                        $nPath = $to . $fName;
                        //echo $nPath . "<br />";

                        if (rename($file, $nPath) === true) {
                            $moved++;
                        } else {
                            $failed++;
                        }
                    }
                    $log->setType('fe')->setLog('Moved file(s) ' . implode(',', $files))->makeLog();
                    if ($failed == 0) {
                        echo "Moved $moved files/directories.";
                    } else {
                        echo "Moved $moved files/directories. Failed to move $failed.";
                    }
                    //echo "<script>openDir('" . $pathing->removeHomeDir($to) . "');</script>";
                }
                /*
                 * delete code
                 */
                if (isset($_POST['delete'])) {
                    $failed = 0;
                    $dFcount = 0;
                    $dDcount = 0;


                    $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
                    $sql = "SELECT `path` from `" . DB_PREFIX . "servers` WHERE 1";
                    $result = $mysqli->query($sql) or die($mysqli->error . ' ' . $mysqli->errno);
                    $paths = array();
                    while ($path = $result->fetch_array(MYSQLI_ASSOC)) {
                        array_push($paths, $path['path']);
                    }
                    $mysqli->close();

                    foreach ($files as $file) {

                        $e = $file . '/';

                        $check = array_search($e, $paths);

                        if ($check !== false) {
                            echo "Please use the server delete function to delete game servers.<br />";
                            continue;
                        }

                        if (is_file($file)) {
                            if (unlink($file) === true) {
                                $dFcount++;
                            }
                        } elseif (is_dir($file)) {
                            $shell = new Shell;
                            $shell->prepareSh("rm -r $file")->setExitStatus()->execute()->checkExitStatus();
                            if ($shell->es === true) {
                                $dDcount++;
                            } else {
                                $failed++;
                            }
                        } else {
                            $failed++; //not a file or directory wtf?
                        }
                    }

                    $log->setType('fe')->setLog('Deleted file(s) ' . implode(',', $files))->makeLog();
                    if ($dFcount != 0) {
                        echo "Deleted $dFcount files.";
                    }
                    if ($dDcount != 0) {
                        echo "Deleted $dDcount directories.";
                    }
                    if ($failed != 0) {
                        echo "Failed to remove $failed files or directories.";
                    }
                }
                if (isset($_POST['unzip'])) {
                    $fname = $_POST['fName'];
                    if (empty($fname)) {
                        die();
                    }
                    $dir = $_POST['dir'];
                    if (empty($dir)) {
                        $dir = homeDir;
                    } else {
                        $dir = homeDir . $dir;
                    }
                    if (stripos($fname, homeDir) !== 0) { //make sure no one is trying to get a file they cant have
                        die();
                    }
                    $shell = new Shell;
                    $shell->prepareSh("unzip -o $fname -d $dir")->setExitStatus()->execute()->checkExitStatus();
                    if ($shell->es) {
                        echo "unzipped $fname";
                        $log->setType('fe')->setLog("Unzipped file $fname to $dir")->makeLog();
                    } else {
                        echo "unable to unzip error: " . $shell->es_val;
                    }
                }

                if (isset($_POST['new'])) {
// data: {fName: varFName, dir: varCurDir,new: 1, ajax: 1},
                    $fname = $_POST['fName'];
                    if (empty($fname)) {
                        die();
                    }
                    $fname = preg_replace('/[^a-zA-Z0-9_.-\/]/', '', $fname);
                    $dir = $_POST['dir'];
                    if (empty($dir)) {
                        $dir = homeDir;
                    } else {
                        $dir = homeDir . $dir;
                    }
                    $fDir = $dir . $fname;

                    $e = fopen($fDir, 'w');
                    if ($e) {
                        fclose($e);
                        $log->setType('fe')->setLog("Created file $fDir")->makeLog();
                        echo "<script>openFile('$fDir')</script>";
                    } else {
                        die('failed to make file');
                    }
                }

                if (isset($_POST['newDir'])) {
                    $dName = $_POST['dName'];
                    if (empty($dName)) {
                        die();
                    }
                    $dName = preg_replace('/[^a-zA-Z0-9_.-\/]/', '', $dName);
                    $dir = $_POST['dir'];
                    if (empty($dir)) {
                        $dir = homeDir;
                    } else {
                        $dir = homeDir . $dir;
                    }
                    $newDir = $dir . $dName;

                    $perms = filter_input(INPUT_POST, 'dirPerms', FILTER_SANITIZE_NUMBER_INT);

//                    var_dump($perms);
//                    die();
                    //$e = mkdir($newDir, 750, true);

                    $shell = new Shell;
                    $shell->prepareSh("mkdir $newDir -m $perms")->setExitStatus()->execute()->checkExitStatus();

                    if ($shell->es) {
                        $log->setType('fe')->setLog("Created directory $newDir")->makeLog();
                        echo "Created directory $newDir";
                    } else {
                        die('failed to make directory');
                    }
                }
            } else {

                die("ajax not found in POST");
            }
        } else {

            die("Token not found");
        }
    } else {

        die("Ajax request not send from server!");
    }
} else {

    die("Page not requested with ajax");
}