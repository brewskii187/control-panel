<?php

session_start();
$self = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$self = explode("/ajax/", $self);
$ref = explode('/home.php', $_SERVER['HTTP_REFERER']);
$search = array('http://', 'https://');
$ref[0] = str_ireplace($search, '', $ref[0]);
$token = $_SESSION['token'];
if ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {
    //Request identified as ajax request

    if (@isset($_SERVER['HTTP_REFERER']) && $ref[0] == $self[0]) {
        //HTTP_REFERER verification
        if (isset($_SESSION['permissions'])) {
            $permissions = $_SESSION['permissions'];
            session_write_close();
            //END AJAX CHECKS
            // BEGIN PAGE CODE HERE
            //

            if (!defined('NineteenEleven')) {
                define('NineteenEleven', true);
            }
            if (!defined('ABSDIR')) {

                $folderName = '/control-panel/';
                $absDir = substr(__DIR__, 0, stripos(__DIR__, $folderName)) . $folderName;
                if ($absDir == $folderName) {
                    define('ABSDIR', __DIR__ . '/');
                } else {

                    define('ABSDIR', $absDir);
                }
            }
            require_once ABSDIR . 'configs/config.php';
            require_once ABSDIR . 'includes/ClassLoader.php';
            require_once ABSDIR . 'GameQ/GameQ.php';

            function ajax($id, $i, $action) {
                global $token;
                print("<div class='actionBtn' id='" . $action . $i . "'>$action</div>");
                print("<script>
                    $(document).ready(function(){
                        $('#" . $action . $i . "').click(function(){
                            msgAreaShow();
                              $('#messageArea').html('<img src=\"images/ajax-loader.gif\" class=\"ajaxLoader\">');
                              $.ajax({
                                  type: 'POST',
                                  url:'ajax/server-control.php',
                                  data:{token:'$token',id:'$id',$action:'true',ajax:1},
                                  success:function(result){
                                $('#messageArea').html(result);

                              }});
                          });
                      });
                </script>");
            }

            $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
            $sys = new SysInfo;
            $gq = new GameQ();
//$gq->setOption('timeout', 200);

            $result = $mysqli->query("SELECT * FROM " . DB_PREFIX . "servers T1 INNER JOIN " . DB_PREFIX . "server_order T2 ON T1.id = T2.id WHERE T1.enabled = '1' ORDER BY T2.order;")or die($mysqli->error . " " . $mysqli->errno);

            //echo '<table>';
            $pCnt = 0;
            $mpCnt = 0;
            $i = 0;
            while ($server = $result->fetch_array(MYSQLI_ASSOC)) {
                $gqQuery = array(
                    array('id' => $server['id'],
                        'type' => Misc::getGameTypeGQ($server['appid']),
                        //'type' => 'source',
                        'host' => $server['ip'] . ":" . $server['port'])
                );
                switch ($server['status']) {

                    case '1':
                        $status = "online";
                        break;
                    case '0':
                        $status = "offline";
                        break;
                    case '3':
                        $status = "updating";
                        break;
                    case '4':
                        $status = "backingUp";
                        break;
                    default:
                        $status = "";
                }

                $gqResult = $gq->clearServers()->addServers($gqQuery)->requestData(); //$gq->addServers($gqQuery);
                echo "<div class='srv $status'>";

                echo "<div class='srvListInfo'><a href='home.php?loc=ControlServer&srvControl&id=" . $server['id'] . "' title='" . $server['notes'] . "'>" . $server['name_friendly'] . " (" . $server['id'] . ")</a></div>";

                #if we have a result of GameQ the server is online display the host name and stop/restart buttons
                if ($gqResult[$server['id']]['gq_online']) {
                    //echo "<div class='srvHostName'>" . wordwrap($gqResult[$server['id']]['hostname'], 20, '<br />', true) . "</div>";
                    echo "<div class='srvHostName'>" . $gqResult[$server['id']]['hostname'] . "</div>";
                    echo "<div class='srvPlayers'>" . $gqResult[$server['id']]['map'] . "<br />" . $gqResult[$server['id']]['num_players'] . "/" . $gqResult[$server['id']]['max_players'] . " " . $lang->srvList->players;

                    echo "<br />RAM: " . $sys->PidFileSetter($server['pid_file'])->PidMem()->PidMem . "</div>";
                    if (!$isMobile) {
                        echo "<br />";
                    }
                    echo "<div class='srvIp'>" . $server['ip'] . ":" . $server['port'] . "</div>";
                    //echo "<div class='srvRam'>RAM: " . $sys->PidFileSetter($server['pid_file'])->PidMem()->PidMem . "</div>";
                    if (!$isMobile) {
                        echo "<br />";
                    }
                    if ($permissions & perms::stop) {
                        echo"<div class='srvBtns'>";
                        ajax($server['id'], $i, 'stop');

                        ajax($server['id'], $i, 'restart');
                        echo"</div>";


                        $pCnt = $pCnt + $gqResult[$server['id']]['num_players'];
                        $mpCnt = $mpCnt + $gqResult[$server['id']]['max_players'];
                    }
                } elseif ($permissions & perms::start && $server['status'] == '0') {
                    #if server is not online display start button.
                    echo"<div class='srvBtns'>";
                    ajax($server['id'], $i, 'start');
                    echo"</div>";
                    //echo "<br />";
                } elseif ($server['status'] == '3' || $server['status'] == '4') {
                    if ($server['status'] == '4') {
                        $status = "backing up";
                    }
                    echo "<div class='srvStatus' style='text-align:center;'>The server is currently $status</div>";
                }

                $i++;
                echo '</div>'; #srvList
            }


            printf("<div class='playerCount'>" . $lang->srvList->pCnt . "</div>", $pCnt, $mpCnt);

            //js to make the divs all the same height.
            if (!$isMobile) {
                echo "
               <script type='text/javascript'>
                $(document).ready(function () {
                    var maxHeight = 0;
                    $('.srv').each(function () {
                        var this_div = $(this);
                        if (maxHeight < this_div.height()) {
                            maxHeight = this_div.height();
                        }
                    })

                    $('.srv').css({ 'height': maxHeight.toString() });
                })
            </script>
            ";
            }
            //
            // for ajax
        //
        } else {
            if (DEBUG) {
                die("Token not found");
            } else {
                die();
            }
        }
    } else {
        if (DEBUG) {
            die("Ajax request not send from server!");
        } else {
            die();
        }
    }
} else {
    if (DEBUG) {
        die("Page not requested with ajax");
    } else {
        die();
    }
}
