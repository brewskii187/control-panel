<?php

session_start();
$self = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$self = explode("/ajax/", $self);
$ref = explode('/home.php', $_SERVER['HTTP_REFERER']);
$search = array('http://', 'https://');
$ref[0] = str_ireplace($search, '', $ref[0]);

if ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {
    //Request identified as ajax request

    if (@isset($_SERVER['HTTP_REFERER']) && $ref[0] == $self[0]) {
        //HTTP_REFERER verification
        if ($_POST['token'] == $_SESSION['token']) {


            if (isset($_POST['ajax'])) {

                if (!defined('NineteenEleven')) {
                    define('NineteenEleven', true);
                }
                if (!defined('ABSDIR')) {

                    $folderName = '/control-panel/';
                    $absDir = substr(__DIR__, 0, stripos(__DIR__, $folderName)) . $folderName;
                    if ($absDir == $folderName) {
                        define('ABSDIR', __DIR__ . '/');
                    } else {

                        define('ABSDIR', $absDir);
                    }
                }
                require_once ABSDIR . 'configs/config.php';
                require_once ABSDIR . 'includes/ClassLoader.php';




                $data = filter_input_array(INPUT_POST, FILTER_SANITIZE_SPECIAL_CHARS);
                $fh = new FileHandler;
                if (isset($data['action'])) {
                    $data[$data['action']] = true;
                }
                if (isset($data['listBackups']) && $_SESSION['permissions'] & (perms::edit | perms::create)) {
                    $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

                    $sql = "SELECT * FROM `" . DB_PREFIX . "backups` WHERE `server_name` = (SELECT `server_name` FROM `" . DB_PREFIX . "backups` WHERE `id` = '" . $data['id'] . "');";

                    $result = $mysqli->query($sql);

                    if ($result->num_rows > 0) {

                        while ($bu = $result->fetch_array(MYSQLI_ASSOC)) {

                            $fh->setFile($bu['backup_path'])->getFileSize()->sizeDisp();

                            echo "<div class='backups'>"
                            . "<div class='buTime inline'>" . date('n/j/Y g:i:s A', $bu['timestamp']) . "</div>"
                            . "<div class='buName inline'>" . $bu['server_name'] . "</div>"
                            . "<div class='fileSize inline'>(" . $fh->sizeDisp . ")</div>"
                            . "<div class='buUser inline'>backed up by: " . $bu['username'] . "</div>"
                            . "<div class='actionBtn' onClick='srvCtrlAjax(\"" . $bu['backup_path'] . "\",\"restore\",\"" . $_POST['token'] . "\")'>Restore</div>"
                            . "<div class='actionBtn'><a href='home.php?loc=download&file=" . urlencode($bu['backup_path']) . "' target=_BLANK>Download</a></div>"
                            . "<div class='actionBtn' onClick='srvCtrlAjax(\"" . $bu['backup_path'] . "\",\"delBackup\",\"" . $_POST['token'] . "\")'>Delete</div>"
                            . "</div>";
                        }
                    } else {
                        echo "no backups";
                    }

                    echo"<br /><div class='actionBtn closeBtn' onClick='closeBuList()'>Close</div>";
                }
            } else {

                die("ajax not found in POST");
            }
        } else {

            die("Token not found");
        }
    } else {

        die("Ajax request not send from server!");
    }
} else {

    die("Page not requested with ajax");
}