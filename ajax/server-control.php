<?php

session_start();
$self = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$self = explode("/ajax/", $self);
$ref = explode('/home.php', $_SERVER['HTTP_REFERER']);
$search = array('http://', 'https://');
$ref[0] = str_ireplace($search, '', $ref[0]);

if ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {
    //Request identified as ajax request

    if (@isset($_SERVER['HTTP_REFERER']) && $ref[0] == $self[0]) {
        //HTTP_REFERER verification
        if ($_POST['token'] == $_SESSION['token']) {
            $permissions = $_SESSION['permissions'];
            session_write_close();

            if (isset($_POST['ajax'])) {

                if (!defined('NineteeEleven')) {
                    define('NineteenEleven', true);
                }
                if (!defined('ABSDIR')) {

                    $folderName = '/control-panel/';
                    $absDir = substr(__DIR__, 0, stripos(__DIR__, $folderName)) . $folderName;
                    if ($absDir == $folderName) {
                        define('ABSDIR', __DIR__ . '/');
                    } else {

                        define('ABSDIR', $absDir);
                    }
                }
                require_once ABSDIR . 'configs/config.php';
                require_once ABSDIR . 'includes/ClassLoader.php';

                $sc = new ServerControl;

                $data = filter_input_array(INPUT_POST, FILTER_SANITIZE_SPECIAL_CHARS);

                if (isset($data['action'])) {
                    $data[$data['action']] = true;
                }
                /**
                 * start
                 */
                if (isset($data['start']) && isset($data['id']) && $permissions & perms::start) {
                    $start = $sc->start($data['id']);
                    if ($start === true) {
                        printf("<div class='success'>" . $lang->srvControl[0]->started . "</div>", $sc->getField($data['id'], 'name_friendly'));
                    } else {
                        printf("<div class='error'>%s</div>", $start);
                    }
                    exit();
                }
                /**
                 * stop
                 */
                if (isset($data['stop']) && isset($data['id']) && $permissions & perms::stop) {
                    $stop = $sc->stop($data['id']);
                    if ($stop === true) {
                        printf("<div class='success'>" . $lang->srvControl[0]->stopped . "</div>", $sc->getField($data['id'], 'name_friendly'));
                    } else {
                        printf("<div class='error'>%s</div>", $stop);
                    }
                    exit();
                }
                /**
                 * restart
                 */
                if (isset($data['restart']) && isset($data['id']) && $permissions & perms::stop) {

                    $restart = $sc->restart($data['id']);

                    if ($restart === true) {
                        printf("<div class='success'>" . $lang->srvControl[0]->restarted . "</div>", $sc->getField($data['id'], 'name_friendly'));
                    } else {
                        echo "problem restarting";
                    }
                }
                /**
                 * copy
                 */
                if (isset($data['copy']) && isset($data['id']) && isset($data['nPath']) && $permissions & perms::create) {

                    $copy = $sc->copy($data['id'], $data['nPath']);

                    if ($copy[0]) {
                        printf($lang->srvControl[0]->copied . " " . $copy[1]); //"Server has been copied. It took x seconds"
                    } elseif (!$copy[0]) {
                        printf($lang->srvControl[0]->fcopied . " " . $copy[1]); //"Failed to copy server."
                    } else {
                        echo "An unknown error has occured.";
                    }
                }
                /**
                 * Delete
                 */
                if (isset($data['delete']) && isset($data['id']) && $permissions & perms::delete) {

                    $delete = $sc->delete($data['id']);

                    if ($delete === true) {
                        print("<div class='success'>" . $lang->srvControl[0]->deleted . "</div>");
                    }
                }
                /**
                 * update
                 */
                if (isset($data['update']) && isset($data['id']) && $permissions & perms::update) {
                    require_once ABSDIR . 'configs/email.php';
                    require_once ABSDIR . 'includes/email.php';
                    $steam = new SteamCmd();
                    $update = $steam->updateCheck($data['id'], $noticeOnly = false, $sendEmail = true, $restart = true, $force = true);
                    if ($update[0]) {
                        print("<div class='success'>" . $update[1] . "</div>");
                    } elseif (!$update[0]) {
                        print("<div class='error'>" . $update[1] . "</div>");
                    }
                }
                /**
                 * backup
                 */
                if (isset($data['backup']) && isset($data['id']) && $permissions & perms::edit) {

                    $bu = $sc->backup($data['id']);

                    if ($bu[0]) {
                        print("<div class='success'>" . $bu[1] . "</div>");
                    } elseif (!$bu[0]) {
                        print("<div class='error'>" . $bu[1] . "</div>");
                    }
                }
                /**
                 * restore
                 */
                if (isset($data['restore']) && isset($data['id']) && $permissions & perms::create) {
                    //die('woot');
                    $restore = $sc->restore($data['id']);

                    if ($restore[0]) {
                        print("<div class='success'>" . $restore[1] . "</div>");
                    } elseif (!$restore[0]) {
                        print("<div class='error'>" . $restore[1] . "</div>");
                    }
                }
                /**
                 * delete backup
                 */
                if (isset($data['delBackup']) && isset($data['id']) && $permissions & perms::delete) {

                    $delBu = $sc->delBackup($data['id']);

                    if ($delBu) {
                        print("<div class='success'>Backup removed successfully</div>");
                    } else {
                        print("<div class='success'>Failed to remove backup</div>");
                    }
                }

                /**
                 * Rcon command
                 */
                if (isset($data['rcon']) && isset($data['id']) && $permissions & perms::rcon) {
                    require_once ABSDIR . "includes/RconClass.php";
                    $srcds_rcon = new srcds_rcon();
                    if ($data['allServers'] == 'true') {
                        $fail = 0;
                        $success = 0;
                        $result = $sc->db->query("SELECT `ip`,`port`,`rcon_pass` FROM " . DB_PREFIX . "servers WHERE `status` = '1' AND `enabled` = '1';");
                        while ($server = $result->fetch_array(MYSQLI_ASSOC)) {
                            $OUTPUT = @$srcds_rcon->rcon_command($server['ip'], $server['port'], $server['rcon_pass'], $data['cmd']);
                            if ($OUTPUT === false) {
                                $OUTPUT = "Unable to connect!";
                                $fail = $fail + 1;
                            } else {
                                $success = $success + 1;
                            }
                            echo $server['ip'] . ":" . $server['port'] . " Response\n" . $OUTPUT;
                        }
                        if ($fail === 0) {
                            print("$success Game Servers Successfully Queried.");
                        } else {
                            print("$success Game Servers Successfully Queried. <br /> $fail servers were unable to connect.");
                        }
                    } else {
                        $server = $sc->getField($data['id'], '`ip`,`port`,`rcon_pass`');
                        $OUTPUT = @$srcds_rcon->rcon_command($server['ip'], $server['port'], $server['rcon_pass'], $data['cmd']);
                        if ($OUTPUT === false) {
                            $OUTPUT = "Unable to connect!";
                        }

                        echo $server['ip'] . ":" . $server['port'] . " Response\n" . $OUTPUT;
                    }
                }
                /**
                 * Updates notes
                 */
                if (isset($data['changeNotes']) && isset($data['id']) && $permissions & perms::edit) {
                    $sql = "UPDATE `" . DB_PREFIX . "servers` SET `notes`='" . $sc->db->escape_string($data['notes']) . "' WHERE `id`='" . $data['id'] . "';";
                    $sc->db->query($sql) or die($sc->sb->error);
                    if ($sc->db->affected_rows == 1) {
                        echo "<div class='success'>Notes Updated</div>";
                    }
                }
                unset($sc);
            } else {

                die("ajax not found in POST");
            }
        } else {

            die("Token not found");
        }
    } else {

        die("Ajax request not send from server!");
    }
} else {

    die("Page not requested with ajax");
}
