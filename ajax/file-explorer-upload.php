<?php

session_start();
$self = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$self = explode("/ajax/", $self);
$ref = explode('/home.php', $_SERVER['HTTP_REFERER']);
$search = array('http://', 'https://');
$ref[0] = str_ireplace($search, '', $ref[0]);

if ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {
    //Request identified as ajax request

    if (@isset($_SERVER['HTTP_REFERER']) && $ref[0] == $self[0]) {
        //HTTP_REFERER verification
        if ($_POST['token'] == $_SESSION['token']) {


            if (!defined('NineteenEleven')) {
                define('NineteenEleven', true);
            }
            if (!defined('ABSDIR')) {

                $folderName = '/control-panel/';
                $absDir = substr(__DIR__, 0, stripos(__DIR__, $folderName)) . $folderName;
                if ($absDir == $folderName) {
                    define('ABSDIR', __DIR__ . '/');
                } else {

                    define('ABSDIR', $absDir);
                }
            }
            require_once ABSDIR . 'configs/config.php';
            require_once ABSDIR . 'includes/LoggerClass.php';
            $output_dir = homeDir . $_POST['dir'];
//            echo "<pre>";
//            var_dump($_FILES);
//            echo "</pre>";
//
//            if (isset($_FILES["uploadedFile"])) {
//
//                if ($_FILES["uploadedFile"]["error"] > 0) {
//                    echo "Error: " . $_FILES["uploadedFile"]["error"] . "<br>";
//                } else {
//
//                    move_uploaded_file($_FILES["uploadedFile"]["tmp_name"], $output_dir . $_FILES["uploadedFile"]["name"]);
//                    $log->setType('fe')->setLog('Uploaded file:' . $output_dir . $_FILES["uploadedFile"]["name"])->makeLog();
//                    echo "Uploaded File: " . $_FILES["uploadedFile"]["name"];
//                }
//            }
            if (isset($_FILES["uploadedFile"])) {
                for ($i = 0; $i < count($_FILES['uploadedFile']['name']); $i++) {
                    if ($_FILES["uploadedFile"]["error"][$i] > 0) {
                        echo "Error: " . $_FILES["uploadedFile"]["error"][$i] . "<br />";
                    } else {
                        $tmpFilePath = $_FILES['uploadedFile']['tmp_name'][$i];
                        if ($tmpFilePath != "") {
                            if (move_uploaded_file($tmpFilePath, $output_dir . $_FILES["uploadedFile"]["name"][$i])) {
                                $log->setType('fe')->setLog('Uploaded file:' . $output_dir . $_FILES["uploadedFile"]["name"][$i])->makeLog();
                                echo "Uploaded File: " . $_FILES["uploadedFile"]["name"][$i] . "<br />";
                            }
                        }
                    }
                }
            }
        } else {

            die("Token not found");
        }
    } else {

        die("Ajax request not send from server!");
    }
} else {

    die("Page not requested with ajax");
}