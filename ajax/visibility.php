<?php

session_start();
if (!isset($_SESSION['permissions'])) {
    die();
}

if (!defined('NineteeEleven')) {
    define('NineteenEleven', true);
}
if (!defined('ABSDIR')) {

    $folderName = '/control-panel/';
    $absDir = substr(__DIR__, 0, stripos(__DIR__, $folderName)) . $folderName;
    if ($absDir == $folderName) {
        define('ABSDIR', __DIR__ . '/');
    } else {

        define('ABSDIR', $absDir);
    }
}
require_once ABSDIR . 'configs/config.php';


if (isset($_POST['ajax'])) {
    $data = filter_input_array(INPUT_POST, FILTER_SANITIZE_SPECIAL_CHARS);

    $_SESSION[$data['div']] = $data['visibility'];

    $sql = "UPDATE " . DB_PREFIX . "users SET " . $data['div'] . "=" . $data['visibility'] . " WHERE id=" . $_SESSION['id'] . ";";

    $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    $mysqli->query($sql);
    $mysqli->close();
}