<?php

session_start();
$self = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$self = explode("/ajax/", $self);
$ref = explode('/home.php', $_SERVER['HTTP_REFERER']);
$search = array('http://', 'https://');
$ref[0] = str_ireplace($search, '', $ref[0]);

if ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {
    //Request identified as ajax request

    if (@isset($_SERVER['HTTP_REFERER']) && $ref[0] == $self[0]) {
        //HTTP_REFERER verification
        if (isset($_SESSION['permissions'])) {
            $permissions = $_SESSION['permissions'];
            session_write_close();

            if (isset($_POST['ajax'])) {
                if (!defined('NineteenEleven')) {
                    define('NineteenEleven', true);
                }
                if (!defined('ABSDIR')) {

                    $folderName = '/control-panel/';
                    $absDir = substr(__DIR__, 0, stripos(__DIR__, $folderName)) . $folderName;
                    if ($absDir == $folderName) {
                        define('ABSDIR', __DIR__ . '/');
                    } else {

                        define('ABSDIR', $absDir);
                    }
                }
                require_once ABSDIR . 'configs/config.php';
                require_once ABSDIR . 'includes/ClassLoader.php';


                $fh = new FileHandler;
                $pathing = new Pathing;
                if (isset($_POST['s']) && !empty($_POST['s'])) {

                    //$subDir = filter_input(INPUT_GET, 's', FILTER_UNSAFE_RAW);

                    $subDir = $pathing->rTSlash(urldecode($_POST['s']));
                    $subDir = $pathing->rBSlash(urldecode($subDir));
                    //var_dump($subDir);
                    $fh->setDir($subDir)->makeAbs();
                    //var_dump($fh);
                } else {
                    $fh->setDir(homeDir);
                }
                echo "<div class='breadcrumbs'>";
                $fh->breadcrumbs();
//var_dump($fh);
                foreach ($fh->breadcrumbs as $breadcrumbs) {
                    echo $breadcrumbs;
                }
                echo "</div>";
                echo "<br />";
                echo "<div class='feList'>";
                $fh->dispDir();
                echo "</div>";
            } else {

                die("ajax not found in POST");
            }
        } else {

            die("Token not found");
        }
    } else {

        die("Ajax request not send from server!");
    }
} else {

    die("Page not requested with ajax");
}
?>
