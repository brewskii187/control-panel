<?php

if (!defined('NineteenEleven')) {
    define('NineteenEleven', true);
}
if (!defined('ABSDIR')) {

    $folderName = '/control-panel/';
    $absDir = substr(__DIR__, 0, stripos(__DIR__, $folderName)) . $folderName;
    if ($absDir == $folderName) {
        define('ABSDIR', __DIR__ . '/');
    } else {

        define('ABSDIR', $absDir);
    }
}
require_once ABSDIR . 'configs/config.php';
require_once ABSDIR . 'includes/ClassLoader.php';
session_start();
if (!($_SESSION['permissions'] & perms::root)) {
    die('You do not have permissions to visit this page.');
}
session_write_close();
$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
$components = explode('|', $_POST['components']);

$i = 1;
$vals = '';
foreach ($components as $srv) {
    //$list[$srv] = $i;
    $vals .= "(" . $mysqli->escape_string($srv) . "," . $i . "),";
    $i++;
}
$vals = substr($vals, 0, -1) . ";";


$sql = "INSERT INTO `" . DB_PREFIX . "server_order` (`id`, `order`) VALUES $vals";

$mysqli->query("TRUNCATE `" . DB_PREFIX . "server_order`;")or die($mysqli->error . " " . $mysqli->errno);

$mysqli->query($sql)or die($mysqli->error . " " . $mysqli->errno);
echo "<div class='success'>Server order updated.</div>";
$mysqli->close();
