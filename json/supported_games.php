<?php
/* This file is used by our javascript to auto fill out forms and such.
* THIS FILE IS PUBLICLY VISIBLE, DO NOT ANY ANY INFORMATION YOU DO
* NOT WANT THE WORLD TO KNOW!
*/
if (!defined('NineteenEleven')) {
    define('NineteenEleven', true);
}
if (!defined('ABSDIR')) {

    $folderName = '/control-panel/';
    $absDir = substr(__DIR__, 0, stripos(__DIR__, $folderName)) . $folderName;
    if ($absDir == $folderName) {
        define('ABSDIR', __DIR__ . '/');
    } else {

        define('ABSDIR', $absDir);
    }
}
require_once ABSDIR . 'includes/supported_games.php';

echo "{\"games\":";
echo json_encode($appInfo, JSON_PRETTY_PRINT);
echo "}";
