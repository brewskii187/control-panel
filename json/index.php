<?php
/*
* This page will display all enabled servers status' in json format. 
* THIS FILE IS PUBLICLY VISIBLE DO NOT ADD ANY INFORMATION YOU DO NOT WANT 
* THE WORLD TO KNOW!:
*/
if (!defined('NineteenEleven')) {
    define('NineteenEleven', true);
}
if (!defined('ABSDIR')) {

    $folderName = '/control-panel/';
    $absDir = substr(__DIR__, 0, stripos(__DIR__, $folderName)) . $folderName;
    if ($absDir == $folderName) {
        define('ABSDIR', __DIR__ . '/');
    } else {

        define('ABSDIR', $absDir);
    }
}
require_once ABSDIR . 'configs/config.php';
require_once ABSDIR . 'includes/ClassLoader.php';
require_once ABSDIR . 'GameQ/GameQ.php';

$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
$sys = new SysInfo;
$gq = new GameQ();
//$gq->setOption('timeout', 200);

$result = $mysqli->query("SELECT * FROM `" . DB_PREFIX . "servers` WHERE `enabled` = '1';")or die($mysqli->error . " " . $mysqli->errno);
$mysqli->close();
if ($result) {
    $srvInfo = array();
    while ($server = $result->fetch_array(MYSQLI_ASSOC)) {

        $gqQuery = array(
            array('id' => $server['name_friendly'],
                'type' => Misc::getGameTypeGQ($server['appid']),
                //'type' => 'source',
                'host' => $server['ip'] . ":" . $server['port'])
        );

        $gqResult = $gq->clearServers()->addServers($gqQuery)->requestData();
//        echo "<pre>";
//        var_dump($gqResult);
//        die();

        if (isset($gqResult[$server['name_friendly']]['hostname'])) {

            array_push($srvInfo, array(
                'online' => true,
                'ip' => $server['ip'],
                'port' => $server['port'],
                'hostname' => $gqResult[$server['name_friendly']]['hostname'],
                'map' => $gqResult[$server['name_friendly']]['map'],
                'num_players' => $gqResult[$server['name_friendly']]['num_players'],
                'max_players' => $gqResult[$server['name_friendly']]['max_players'],
                'join_link' => $gqResult[$server['name_friendly']]['gq_joinlink']
            ));
        } else {
            array_push($srvInfo, array(
                'online' => false,
                'ip' => $server['ip'],
                'port' => $server['port']
            ));
        }
    }
}
echo "<pre>" . json_encode($srvInfo, JSON_FORCE_OBJECT | JSON_PRETTY_PRINT) . "</pre>";
