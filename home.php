<?php
/*
 * This is the main page once logged in.
 * This page loads all other pages that are to be displayed
 */
session_start();
if (!isset($_SESSION['username'])) {
    header("location:index.php");
} else {
    $user_name = $_SESSION['username'];
    $permissions = $_SESSION['permissions'];
}
define('HOMEPAGE', true);
if (!defined('NineteeEleven')) {
    define('NineteenEleven', true);
}
if (!defined('ABSDIR')) {

    $folderName = substr($_SERVER['REQUEST_URI'], 0, strrpos($_SERVER['REQUEST_URI'], "/")) . "/";
    $absDir = substr(__DIR__, 0, stripos(__DIR__, $folderName)) . $folderName;
    if ($absDir == $folderName) {
        define('ABSDIR', __DIR__ . '/');
    } else {

        define('ABSDIR', $absDir);
    }
}
require_once ABSDIR . 'configs/config.php';
require_once ABSDIR . 'includes/ClassLoader.php';

#this sets the url of the page. this includes any get requests
define('self', "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
#token for ajax
$token = sha1(SALT . Misc::random(20, true));
$_SESSION['token'] = $token;
$sc = new ServerControl;

function loadServerList() {
    global $token;
    print("<div id='serverList' class='serverList'></div>");
    print("<script>

    $('#serverList').html('<img src=\"images/ajax-loader.gif\" class=\"ajaxLoader\">');
    $(document).ready(function loadServerList(){
          $.ajax({
              type: 'POST',
              url:'ajax/server-list.php',
              data:{token:'$token'},
              success:function(result){
            $('#serverList').html(result);
	    setTimeout(loadServerList," . $GLOBALS['settings']['ajax']['ServerRefreshRate'] . "); //refresh server list every 30 seconds
          }});
      });
</script>");
}

if (DEBUG) {
    if ($_SERVER['SERVER_ADDR'] != '64.94.238.230') {
        echo "<h1 style='position:absolute; top: 70px;z-index:999;color:red;left:45%'>DEV SITE</h1>";
    } else {
        echo "<h1 style='position:absolute; top: 70px;z-index:999;color:green;left:45%'>LIVE SITE</h1>";
    }
}

if (isset($_GET['file'])) {
    $file = filter_input(INPUT_GET, 'file', FILTER_UNSAFE_RAW);

    $a = stripos($file, BACKUPS_DIR);

    $b = stripos($file, homeDir);

    if ($a === 0 || $b === 0) {

        Misc::makeDl($file);
    } else {
        die("I can't let you do that Michael");
    }
    die();
}
?>
<html>
    <head>
        <title>NineteenElevens Control Panel</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!--Add some jquery -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

        <!--Jquery UI -->
        <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css" />
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>

<?php
if (isset($_SESSION['theme']) && !$isMobile) {
    loadSources($_SESSION['theme']);
} elseif ($isMobile) {
    loadSources('mobile');
} else {
    loadSources('default');
}

function loadSources($dir) {
    $json = file_get_contents(ABSDIR . 'themes/' . $dir . '/load.json');
    $files = json_decode($json);
    foreach ($files as $file => $type) {
        if ($type == 'js') {
            echo "<script src=\"themes/$file\" type=\"text/javascript\"></script>";
        } elseif ($type == 'css') {
            echo "<link type=\"text/css\" rel=\"stylesheet\" href=\"themes/$file\" />";
        } else {
            continue;
        }
    }
}
?>
    </head>
    <body>
<?php require_once ABSDIR . 'includes/nav-bar.php'; ?>
        <div class="wrapper" id='wrapper'>
        <?php
        echo"<input type='hidden' id='token' value='$token'>";
        //if (!$isMobile) {

        if ($_SESSION['srvStats'] == '1') {

            print("<div class='srvStats' id='srvStats' ><img src='images/minimize_button.jpg' class='minimizeBtn' id='minsrvStats' Onclick='toggleVisibility(\"srvStats\")'>");
        } else {

            print("<div class='srvStats' id='srvStats' style='display:none'><img src='images/minimize_button.jpg' class='minimizeBtn' id='minsrvStats' Onclick='toggleVisibility(\"srvStats\")'>");
        }
        $sys = new SysInfo;
        if ($settings['display']['uptime']) {
            print("<div class='sysText'>Up Time</div>");
            print($sys->upTime()->upTime);
        }
        if ($settings['display']['load']) {
            print("<div class='sysText'>System Load</div>");
            $sys->sysLoad();
            print("5min:" . $sys->load5Min . "<br /> 10min:" . $sys->load10Min . "<br /> 15min:" . $sys->load15Min);
        }
        if ($settings['display']['ram']) {
            $mem = $sys->MemStats()->MemStats;
            print("<div class='sysText'>Memory usage</div>");
            print("Free: " . $mem['free'] . "<br />");
            print("Total: " . $mem['total'] . "<br />");
            print("Cached: " . $mem['cached'] . "<br />");
            print("<div id='memBar' class='progressBar'></div><script>

                        $(function() {
                        $('#srvStats').draggable();
                            $( '#memBar' ).progressbar({
                                value: " . (100 - $mem['precent']) . ",
                                max: 100
                            });
                        });
                        </script>");
        } else {
            print("<script>

                        $(function() {
                            $('#srvStats').draggable();
                        });
                        </script>");
        }

        if ($settings['display']['cpu']) {
            print("<div class='sysText'>Cpu Usage</div>");
            $cpu = $sys->cpuStats()->cpuStats;
            print($cpu . " %");
            print("<div id='cpuBar' class='progressBar'></div><script>

                        $(function() {
                            $( '#cpuBar' ).progressbar({
                                value: " . ($cpu) . ",
                                max: 100
                            });
                        });
                        </script>");
        }
        print("</div>");
        //}
        print("<div class='contentArea'>");


        printf("<div class='welcome'>%s %s</div>", $lang->home[0]->welcome, $user_name);

#uses a get request to load one of the files in pages/* loads home.php if no page is set.
        if (isset($_GET['loc']) && !empty($_GET['loc'])) {

            $loc = "pages/" . $_GET['loc'];

            if (is_file($loc . ".php")) {

                require_once $loc . ".php";
            } elseif (is_file($loc . ".phtml")) {

                require_once $loc . ".phtml";
            } else {

                printf("<div class='error'>%s</div>", $lang->home[0]->missingPage);
                loadServerList();
            }
        } else {
            loadServerList();
        }
        print("</div>"); //contentArea
        ?>

        </div>
<?php
print("<div id='messageArea' style='display:none;'>");

#if there is a message from a previous page/script print and clear it.
#using javascript instead, this should be going away;
if (isset($_SESSION['message'])) {
    Misc::uiMsg($_SESSION['message']);

    unset($_SESSION['message']);
}
print("</div>"); #messageArea div



/*
 * Rcon Pop Up
 */
if ($_SESSION['permissions'] & perms::rcon) {
    require_once ABSDIR . "includes/RconClass.php";

    if ($_SESSION['rconPopOut'] == '0') {
        echo "<div class='rconPopOut' id='rconPopOut' style='display:none;'>";
    } else {
        echo "<div class='rconPopOut' id='rconPopOut'>";
    }
    echo"<img src='images/minimize_button.jpg' class='minimizeBtn' id='minimizeRcon'>";

    echo "<div id='rconResponse' class='rconResponse'><textarea readonly id='rconOutput'></textarea></div>
            <br />";

    $sc->makeComboBox('rconCombo inline', 'rconCombo', true);
    unset($sc);
    echo"<div class='rconCtrls'><input type='text' placeholder='Rcon command' id='rconCmd'></div>"
    . "<div class='rconCtrls'><input type='submit' class='actionBtn' value='Send Command' onclick='sendRcon()' id='submitRcon'></div>"
    . "<div class='rconCtrls'><input type='checkbox' value='1' id='allServers'></div>";
    echo $lang->rcon->allSrv . "</div>";
    /*
     * File Editor
     */
    echo "<div class='fileEditor resizeable' id='fileEditor' style='display:none;position:fixed;'>";

    echo "<img src='images/button_X.png' class='minimizeBtn' id='minimizeFileEditor' onclick='confirmClose()' title='Do NOT save, and close' ><br />";
    echo "<div class='resizeable' id='fileEditorContents'></div>";
    echo "
            <div id='commandList' title='Editor commands' style='display:none;'>
             <p>Alt-Left: goSubwordLeft</p>
             <p>Alt-Right: goSubwordRight</p>
             <p>Ctrl-Up: scrollLineUp</p>
             <p>Ctrl-Down: scrollLineDown</p>
             <p>Shift-Ctrl-L: splitSelectionByLine</p>
             <p>Shift-Tab: indentLess</p>
             <p>Esc: singleSelectionTop</p>
             <p>Ctrl-L: selectLine</p>
             <p>Shift-Ctrl-K: deleteLine</p>
             <p>Ctrl-Enter: insertLineAfter</p>
             <p>Shift-Ctrl-Enter: insertLineBefore</p>
             <p>Ctrl-D: selectNextOccurrence</p>
             <p>Shift-Ctrl-Space: selectScope</p>
             <p>Shift-Ctrl-M: selectBetweenBrackets</p>
             <p>Ctrl-M: goToBracket</p>
             <p>Shift-Ctrl-Up: swapLineUp</p>
             <p>Shift-Ctrl-Down: swapLineDown</p>
             <p>Ctrl-/: toggleComment</p>
             <p>Ctrl-J: joinLines</p>
             <p>Shift-Ctrl-D: duplicateLine</p>
             <p>Ctrl-T: transposeChars</p>
             <p>F9: sortLines</p>
             <p>Ctrl-F9: sortLinesInsensitive</p>
             <p>F2: nextBookmark</p>
             <p>Shift-F2: prevBookmark</p>
             <p>Ctrl-F2: toggleBookmark</p>
             <p>Shift-Ctrl-F2: clearBookmarks</p>
             <p>Alt-F2: selectBookmarks</p>
             <p>Alt-Q: wrapLines</p>
             <p>Shift-Alt-Up: selectLinesUpward</p>
             <p>Shift-Alt-Down: selectLinesDownward</p>
             <p>Ctrl-F3: findUnder</p>
             <p>Shift-Ctrl-F3: findUnderPrevious</p>
             <p>Shift-Ctrl-[: fold</p>
             <p>Shift-Ctrl-]: unfold</p>
             <p>Ctrl-H: replace</p>
             <p>Ctrl-K Ctrl-Backspace: delLineLeft</p>
             <p>Ctrl-K Ctrl-K: delLineRight</p>
             <p>Ctrl-K Ctrl-U: upcaseAtCursor</p>
             <p>Ctrl-K Ctrl-L: downcaseAtCursor</p>
             <p>Ctrl-K Ctrl-Space: setSublimeMark</p>
             <p>Ctrl-K Ctrl-A: selectToSublimeMark</p>
             <p>Ctrl-K Ctrl-W: deleteToSublimeMark</p>
             <p>Ctrl-K Ctrl-X: swapWithSublimeMark</p>
             <p>Ctrl-K Ctrl-Y: sublimeYank</p>
             <p>Ctrl-K Ctrl-G: clearBookmarks</p>
             <p>Ctrl-K Ctrl-C: showInCenter</p>
             <p>Ctrl-K Ctrl-j: unfoldAll</p>
             <p>Ctrl-K Ctrl-0: unfoldAll</p>
            </div>";
    echo "</div>"; //file editor
    /*
     * Bottom nav bar for minimized items
     */
    echo"<div class='bottomNav'>";
    if ($_SESSION['rconPopOut'] == '0') {
        echo"<div id='showRcon' class='cp-ui-minimize'>Rcon Tool</div>";
    } else {
        echo"<div id='showRcon' class='cp-ui-minimize' style='display:none;'>Rcon Tool</div>";
    }
}
if ($_SESSION['srvStats'] == '1') {
    echo"<div id='maxsrvStats' class='cp-ui-minimize' style='display:none' Onclick='toggleVisibility(\"srvStats\")'>Show Server Stats</div>";
} else {
    echo "<div id='maxsrvStats' class='cp-ui-minimize' Onclick='toggleVisibility(\"srvStats\")'>Show Server Stats</div>";
}
echo "</div>"; //bottomNav
?>

    </body>
</html>
