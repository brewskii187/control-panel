function srvCtrlAjax(varId, varAction, varToken) {
    if (varAction == 'start' || varAction == 'backup') {
        var timeDelay = 6000;
    } else {
        var timeDelay = 3000;
    }
    ;
    $(document).ready(function() {
        msgAreaShow();
        $('#messageArea').html('<img src=\"images/ajax-loader.gif\" class=\"ajaxLoader\"><div class=\"ajaxInfo upperCase\">Currently executing operation: ' + varAction + '</div>');
        $.ajax({
            type: 'POST',
            url: 'ajax/server-control.php',
            data: {token: varToken, id: varId, action: varAction, ajax: 1},
            success: function(result) {
                $('#messageArea').html(result);
            }});
    });
}
;


function selectUser() {
    var comboBox = document.getElementById('selectUserBox').value;
    window.location = 'home.php?loc=UserManagement&userSelect&id=' + comboBox;
}
;

/*Create Server Page*/
/* This will auto fill the app id box, or display it if its set to other*/
function checkAppId() {
    var appId = document.getElementById('installAppIdBox').value;
    var box = document.getElementById('srvAppId').style
    document.getElementById('srvAppId').value;
    //console.log(appId);
    if (appId === "other") {

        box.display = 'inline-block';

    } else {
        box.display = 'none';
        $(document).ready(function() {
            $.getJSON('json/supported_games.php', function(data) {
                for (var i in data.games) {
                    if (data.games[i].srvAppId == appId) {
                        gamesFolderName = data.games[i].folderName;
                        gamesCmdLineGame = data.games[i].cmdLineGame;
                        gamesCAppId = data.games[i].clientAppId;
                        gamesCmdLineOptions = data.games[i].cmdLineOptions;
                        autoFill();
                        makeCmdLine();
                        break;
                    }
                    ;
                }
            });
        });
    }
    ;

    document.getElementById('srvAppIdInput').value = appId;
}
;


function autoFill() {
    //var checkbox = $('#dspExtaInfo').prop('checked');

    // if (checkbox) {
    $(document).ready(function() {
        var path = $('#srvPath').val();

        $('#srvExecPath').val(path + '/srcds_run');


        $('#srvPidFile').val(path + '/pid.file');
        $('#srvInfPath').val(path + '/' + gamesFolderName + '/steam.inf');
        makeCmdLine();
    });
    //}
    ;
}
;

function makeCmdLine() {
    $(document).ready(function() {
        var csIp = $('#srvIp').val();
        var csPort = $('#srvPort').val();
        var csRcon = $('#srvRconPass').val();
        var homeDir = $('#homeDir').val();
        var csPidFile = $('#srvPidFile').val();
        if (csRcon == '') {
            $('#srvCmdLineHidden').val('-game ' + gamesCmdLineGame + ' -console -nocrashdialog -pingboost 2 +ip ' + csIp + ' +port ' + csPort + ' -pidfile ' + homeDir + csPidFile);
        } else {
            $('#srvCmdLineHidden').val('-game ' + gamesCmdLineGame + ' -console -nocrashdialog -ping_boost 2 +ip ' + csIp + ' +port ' + csPort + ' +rcon_password ' + csRcon + ' -pidfile ' + homeDir + csPidFile);
        }
        ;
        $('#srvCmdLine').val(gamesCmdLineOptions);
    });
}
;
/*end CreateServer page*/

/* BackupManagement */

function loadBackupList(varId, varToken) {
    $(document).ready(function() {

        $('#messageArea').html('<img src=\"images/ajax-loader.gif\" class=\"ajaxLoader\"><div class=\"ajaxInfo\">Getting list of backups.</div>');
        $.ajax({
            type: 'POST',
            url: 'ajax/backups-list.php',
            data: {token: varToken, id: varId, action: 'listBackups', ajax: 1},
            success: function(result) {
                $('#backupsWindow').effect('fade', 700).draggable();
                $('#backupsWindow').html(result);

            }});
    });
}
;

function closeBuList() {
    $(document).ready(function() {
        $('#backupsWindow').effect('fade', 700);
    });
}
;

/**
 *
 * Rcon page
 */
$(document).ready(function() {
    $('#rconCmd').keypress(function(e) {
        if (e.which == 13) { //submit on enter
            sendRcon();
        }
    });
});

function sendRcon() {
    $(document).ready(function() {
        var rconCmd = $('#rconCmd').val();
        var srvId = $('#rconCombo').val();
        var allSrv = $('#allServers').prop('checked');
        var varToken = $('#token').val();
        //console.log(rconCmd + " " + srvId + " " + allSrv); //debug code

        if (srvId == 0 && !allSrv) {
            $('#rconOutput').val('Please select a server first');
            return;

        }
        ;
        //$('#messageArea').html('<img src=\"images/ajax-loader.gif\" class=\"ajaxLoader\"><div class=\"ajaxInfo\">Sending command to server.</div>');
        $.ajax({
            type: 'POST',
            url: 'ajax/server-control.php',
            data: {token: varToken, id: srvId, action: 'rcon', cmd: rconCmd, allServers: allSrv, ajax: 1},
            success: function(result) {
                var currentText = $('#rconOutput').val();
                $('#rconResponse').show();
                currentText = currentText + '\n' + result;
                console.log(currentText);
                $('#rconOutput').val(currentText);
                $('#rconOutput').scrollTop($('#rconOutput')[0].scrollHeight);
                $('#rconCmd').val('');
            }});
    });
}


/**
 *
 * update notes
 */
var timeoutReference;
var notesInterval = 1000;  //time in ms,
$(document).ready(function() {
    $('#notesBox').keypress(function() {
        var _this = $(this); // copy of this object for further usage

        if (timeoutReference)
            clearTimeout(timeoutReference);
        timeoutReference = setTimeout(function() {
            updateNotes();
        }, notesInterval);
    });
});




function updateNotes() {
    $(document).ready(function() {
        var varNotes = $('#notesBox').val();
        var varId = $('#srvId').val();
        var varToken = $('#token').val();
        console.log('send ajax');
        $.ajax({
            type: 'POST',
            url: 'ajax/server-control.php',
            data: {token: varToken, id: varId, action: 'changeNotes', notes: varNotes, ajax: 1},
            success: function(result) {
                msgAreaShow();
                $('#messageArea').html(result);
                setTimeout(function() {
                    msgAreaHide();
                }, 1000
                        );
            }});
    });
}


function toggleVisibility(varDiv) {
    $(document).ready(function() {
        $('#' + varDiv).effect('fade', 1000);
        $('#max' + varDiv).effect('fade', 1000);
        setTimeout(function() {
            if ($('#' + varDiv).is(':visible')) {
                var varVisible = 1;

            } else {
                var varVisible = 0;
            }
            console.log(varVisible);
            $.ajax({
                type: 'POST',
                url: 'ajax/visibility.php',
                data: {div: varDiv, visibility: varVisible, ajax: 1}
            });
        }, 1010);

    });
}

function msgAreaShow() {
    $(document).ready(function() {
        if ($('#messageArea').is(':hidden')) {
            $('#messageArea').effect('fade', 500);
            //$('#messageArea').dialog();
        }
        ;
    });
}
;

function msgAreaHide() {
    $(document).ready(function() {
        if ($('#messageArea').is(':visible')) {
            $('#messageArea').effect('fade', 1000);
        }
        ;
    });
}
;
function addMsg(msg) {
    msgAreaShow();
    $(document).ready(function() {
        $('#messageArea').html(msg);
    });
}


function openFile(varFile) {
    $(document).ready(function() {
        varToken = $('#token').val();
        $('#messageArea').show().html('<img src=\"images/ajax-loader.gif\" class=\"ajaxLoader\">');
        $.ajax({
            type: 'POST',
            url: 'ajax/file-editor.php',
            data: {token: varToken, file: varFile, ajax: 1},
            success: function(result) {



                $('#fileEditorContents').html(result);
                $('#messageArea').hide();
                $('#fileEditor').toggle("slide",
                        {
                            direction: 'up',
                            easing: 'easeOutQuart'
                        }, 1000);
                editor.refresh();
                $('.resizeable').resizable({disabled: false}, "option", "alsoResize", "#fileEditorContents");

            }});
    });
}
function dialog(divId) {
    $(divId).show().dialog();
}
function openDir(varDir) {
    $(document).ready(function() {
        $.ajax({
            type: 'POST',
            url: 'ajax/file-explorer.php',
            data: {s: varDir, ajax: 1},
            success: function(result) {
                $('.feListWrapper').html(result);
                feHideMsgArea();
            }});
    });

}
function closeFileEditor() {
    $(document).ready(function() {
        $("#fileEditor").toggle("slide",
                {
                    direction: 'up',
                    easing: 'easeOutQuart'
                }, 1000);
    });
}
function confirmClose() {
    if (codeChanged === true) {
        var e = confirm('Discard changes?');
        if (e === true) {
            closeFileEditor();
        }
    } else {
        closeFileEditor();
    }
}


function unzipFile(varArchive) {
    $(document).ready(function() {
        $('#feMsgArea').show().html('<img src=\"images/ajax-loader.gif\" class=\"ajaxLoader\">');
        var varCurDir = $('#feCurDir').val();

        $.ajax({
            type: 'POST',
            url: 'ajax/file-explorer-actions.php',
            data: {fName: varArchive, dir: varCurDir, unzip: 1, ajax: 1},
            success: function(result) {
                // $('#messageArea').html('').hide();

                $('#feMsgArea').show().html(result);
                openDir(varCurDir);

            },
            error: function() {
                $('#feMsgArea').show().html('unable to unzip file');
            }
        });


    });

}
function feMvSel() {
    $(document).ready(function() {
        var varFiles = $(".feList input:checkbox:checked").map(function() {
            return $(this).val();
        }).get();
        console.log(varFiles);
        var varMvTo = $('#feMvTo').val();

        if (varFiles.length == 0) {
            $('#feMsgArea').show().html('Nothing selected!');
            return;
        }
        if (varMvTo == '') {
            $('#feMsgArea').show().html('New path not set!');
            return;
        }
        varFiles.unshift(varMvTo);
        varFiles = JSON.stringify(varFiles);

        $.ajax({
            type: 'POST',
            url: 'ajax/file-explorer-actions.php',
            data: {files: varFiles, move: 1, ajax: 1},
            success: function(result) {
                $('#feMsgArea').show().html(result);
                openDir($('#feCurDir').val());
            }});
    });

}

function feDelSel() {
    $(document).ready(function() {
        var varFiles = $(".feList input:checkbox:checked").map(function() {
            return $(this).val();
        }).get();
        console.log(varFiles);
        var list = varFiles.join("\n");
        if (varFiles.length == 0) {
            $('#feMsgArea').show().html('Nothing selected!');
            return;
        }
        var e = confirm('Confirm Deletion \n ' + list);
        if (e === true) {


            varFiles = JSON.stringify(varFiles);

            $.ajax({
                type: 'POST',
                url: 'ajax/file-explorer-actions.php',
                data: {files: varFiles, delete: 1, ajax: 1},
                success: function(result) {
                    $('#feMsgArea').show().html(result);
                    openDir($('#feCurDir').val());
                }});
        }
    });

}
function feHideMsgArea() {
    $(document).ready(function() {
        if ($('#feMsgArea').is(':visible')) {
            $('#feMsgArea').delay(2000).effect('fade', 500);
        }
        ;
    });
}

function feNewFile() {
    $(document).ready(function() {
        var varFName = $('#newFName').val();
        var varCurDir = $('#feCurDir').val();

        $.ajax({
            type: 'POST',
            url: 'ajax/file-explorer-actions.php',
            data: {fName: varFName, dir: varCurDir, new : 1, ajax: 1},
            success: function(result) {
                openDir(varCurDir);
                $('#feMsgArea').show().html(result);
            }});


    });
}

function feNewDir() {
    $(document).ready(function() {
        var varFName = $('#newDName').val();
        var varCurDir = $('#feCurDir').val();
        var varPerms = $('#dirPerms').val();
        console.log(varPerms);
        $.ajax({
            type: 'POST',
            url: 'ajax/file-explorer-actions.php',
            data: {dName: varFName, dir: varCurDir, dirPerms: varPerms, newDir: 1, ajax: 1},
            success: function(result) {
                openDir(varCurDir);
                $('#feMsgArea').show().html(result);
            }});


    });
}

function toggleId(div) {
    $('#' + div).toggle("slow");
}

function setPerms(){
    if($("#rootCheckBox").prop('checked')){
 	$('#perms').val($('#rootCheckBox').val());
    }else{
        var varFiles = $(".permsList input:checkbox:checked").map(function() {
            return $(this).val();
        }).get();
        //console.log(varFiles);
        var varPerms = 0;
        for (var i = varFiles.length - 1; i >= 0; i--) {
            varPerms = Number(varPerms) + Number(varFiles[i]);
        };
	    $('#perms').val(varPerms);
	};
};

$(document).ready(function() {
    $(".resizeable").resizable({disabled: true});
    $("#rconPopOut").draggable();
    $('#messageArea').click(function() {
        $(this).effect('fade', 1000);
    });
    $("#minimizeRcon").click(function() {
        $("#rconPopOut").toggle("slide",
                {
                    direction: 'down',
                    easing: 'easeOutQuart'
                }, 1000);
        $("#showRcon").effect('fade', 1000);
        $.ajax({
            type: 'POST',
            url: 'ajax/visibility.php',
            data: {div: 'rconPopOut', visibility: '0', ajax: 1}
        });
    });

    $("#showRcon").click(function() {
        $("#rconPopOut").toggle("slide",
                {
                    direction: 'down',
                    easing: 'easeOutQuart'
                }, 1000);
        $("#showRcon").effect('fade', 1000);
        $.ajax({
            type: 'POST',
            url: 'ajax/visibility.php',
            data: {div: 'rconPopOut', visibility: '1', ajax: 1}

        });
    });
    $('.feUndoClose').click(function() {
        closeFileEditor();
    });
    $('#checkAll').click(function() {

        var cb = $('#cb').val();
        if (cb == '0') {
            $('input:checkbox').prop('checked', true);
            $('#cb').val('1');
            $('#checkAll').val('Uncheck All');
        }
        if (cb == '1') {
            $('input:checkbox').prop('checked', false);
            $('#cb').val('0');
            $('#checkAll').val('Check All');
        }
    });

    $("#sortable").sortable({
        update: function(event, ui) {

            var list = $(this).sortable("toArray").join("|");
            $.ajax({
                type: 'POST',
                url: "ajax/persistListOrder.php",
                data: {
                    'section': this.id,
                    'components': list
                },
                success: function(result) {
                    addMsg(result);
                    setTimeout(function() {
                        msgAreaHide();
                    }, 500);
                }

            });
        }
    });
});











function search() { //not working as intented unused.
    var src_str = $(".dirList").html();
    var term = $("#feSearchInput").val();
    console.log(term);
    term = term.replace(/(\s+)/, "(<[^>]+>)*$1(<[^>]+>)*");
    var pattern = new RegExp("(" + term + ")", "gi");

    src_str = src_str.replace(pattern, "<mark>$1</mark>");
    src_str = src_str.replace(/(<mark>[^<>]*)((<[^>]+>)+)([^<>]*<\/mark>)/, "$1</mark>$2<mark>$4");

    $('.dirList').html(src_str);

}
