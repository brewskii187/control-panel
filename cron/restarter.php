<?php

set_time_limit(290);
if (!defined('NineteeEleven')) {
    define('NineteenEleven', true);
}
if (!defined('ABSDIR')) {

    $folderName = '/control-panel/';
    $absDir = substr(__DIR__, 0, stripos(__DIR__, $folderName)) . $folderName;
    if ($absDir == $folderName) {
        define('ABSDIR', __DIR__ . '/');
    } else {

        define('ABSDIR', $absDir);
    }
}
require_once ABSDIR . 'configs/config.php';
require_once ABSDIR . 'includes/ClassLoader.php';
$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
$sc = new ServerControl;
$sql = "SELECT `id` FROM `" . DB_PREFIX . "servers` where auto_restart = 1 AND status = 1 AND enabled = 1";
$result = $mysqli->query($sql)or die($mysqli->error . " " . $mysqli->errno);

while ($server = $result->fetch_array(MYSQLI_ASSOC)) {
    $r = $sc->restart($server['id']);
    if ($r) {
        $log->setType($server['id'])->setLog('restarted on schedule')->makeLog();
    } else {
        $log->setType($server['id'])->setLog("$r")->makeLog();
    }
    sleep(10);
}
