<?php

/*
 * This script will hang if there is an error with the previous script and the
 * database wasnt updated
 */

if (!defined('NineteeEleven')) {
    define('NineteenEleven', true);
}
if (!defined('ABSDIR')) {

    $folderName = '/control-panel/';
    $absDir = substr(__DIR__, 0, stripos(__DIR__, $folderName)) . $folderName;
    if ($absDir == $folderName) {
        define('ABSDIR', __DIR__ . '/');
    } else {

        define('ABSDIR', $absDir);
    }
}
require_once ABSDIR . 'configs/config.php';
require_once ABSDIR . 'configs/email.php';
require_once ABSDIR . 'includes/ClassLoader.php';
require_once ABSDIR . 'includes/email.php';

$steamcmd = new SteamCmd;

$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

$sql = "SELECT * FROM `" . DB_PREFIX . "new_servers` WHERE `created` = 0;";

$result = $mysqli->query($sql) or die($mysqli->error . " " . $mysqli->errno);

while ($data = $result->fetch_array(MYSQL_ASSOC)) {

    $cmdLine = $steamcmd->setCmdLine($data);

    #if statement to check if file we made previously exists and delete it.
    $cfile = $data['path'] . 'new_server.txt';
    if (is_file($cfile) && is_readable($cfile) && is_writable($cfile)) {
        //stuff really isnt needed
        $file = file($cfile);

        if (is_array($file) && !empty($file)) {

            if (trim($file[0]) != $data['appid'] || trim($file[1]) != $data['path'] || trim($file[2]) != $data['steamuser']) {
                failed(sprintf("%s", $lang->newserver[0]->fail1));
                continue;
            } else {
                unlink($data['path'] . 'new_server.txt');
            }
        } else {
            #couldnt find creation file, or doesnt have permissions to open
            failed(sprintf("%s", $lang->newserver[0]->fail2));
            continue;
        }
        unlink($data['path'] . 'new_server.txt');
    } else {
        #couldnt find creation file
        failed(sprintf("%s", $lang->newserver[0]->fail3));
        continue;
    }


    if (!ALLOW_DL) { //debugging stuff
        $cmdLine = "+login anonymous +quit";
    }

    if ($output = $steamcmd->SteamCmdExec($cmdLine)) {

        $mysqli->query("UPDATE `" . DB_PREFIX . "new_servers` set steampass='null', created='" . date('U') . "' WHERE id='" . $data['id'] . "';") or die($mysqli->error . " " . $mysqli->errno);

        if ($data['email_confirm'] != '0') {



            $mail_body = "";
            $handle = fopen($data['path'] . 'install.log', 'a');
            foreach ($output as $d) {
                //echo $data;
                fwrite($handle, "$d \n");
                $mail_body = $mail_body . "$d \n";
            }
            fclose($handle);
            unset($d);

            $subject = sprintf("%s", $lang->newserver[0]->subject1);


            send_email($data['email_confirm'], $subject, $mail_body);
        }
    }
}

function failed($mail_body) {

    global $lang, $mail;


    if ($GLOBALS['data']['email_confirm'] != '0') {
        require_once ABSDIR . 'includes/email.php';
        $subject = sprintf("%s", $lang->newserver[0]->failSubject);
        if (DEBUG) {
            echo $mail_body;
        }
        send_email($GLOBALS['data']['email_confirm'], $subject, $mail_body);
    }
    exit();
}

$mysqli->close();
unset($mysqli);
unset($steamcmd);
