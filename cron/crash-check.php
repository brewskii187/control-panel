<?php

if (!defined('NineteeEleven')) {
    define('NineteenEleven', true);
}
if (!defined('ABSDIR')) {

    $folderName = '/control-panel/';
    $absDir = substr(__DIR__, 0, stripos(__DIR__, $folderName)) . $folderName;
    if ($absDir == $folderName) {
        define('ABSDIR', __DIR__ . '/');
    } else {

        define('ABSDIR', $absDir);
    }
}
require_once ABSDIR . 'configs/config.php';
require_once ABSDIR . 'configs/email.php';
require_once ABSDIR . 'includes/ClassLoader.php';
require_once ABSDIR . 'includes/email.php';


/*
 * status 1 = running, 0 = stopped, 3 = updating, 4 = backingup,
 */

$sc = new ServerControl;
$sql = "SELECT `pid_file`,`id`,`ip`,`port`,`name_friendly` FROM " . DB_PREFIX . "servers WHERE `status` = '1';";
$result = $sc->db->query($sql) or die($sc->db->error . ' ' . $sc->db->errno);

if ($result->num_rows >= 1) {


    while ($server = $result->fetch_array(MYSQLI_ASSOC)) {
        if (!$sc->isRunning($server['pid_file'], $isFile = true)) { #if the pid id not found
            $secondaryCheck = @fsockopen($server['ip'], $server['port']);
            if ($secondaryCheck === false) {
                #the server is not running on ip/port
                //do restart here
                $log->setType($server['id'])->setLog('crashed')->makeLog();
                $sc->stop($server['id'], $force = true);
                sleep(2);
                if ($sc->start($server['id'])) {
                    $newStatus[$server['id']] = array(1, $server['name_friendly']);
                } else {
                    $newStatus[$server['id']] = array(0, $server['name_friendly']);
                }
            }
            #close the handle.
            @fclose($secondaryCheck);
        }
    }

    if (isset($newStatus) && !empty($newStatus)) {
        //update mysql with all servers new status;

        $sql = "SELECT `email` FROM `" . DB_PREFIX . "users` WHERE `rec_email` = '1';";
        $result = $sc->db->query($sql);
        if ($result->num_rows >= 1) {
            $addys = null;
            while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
                $addys .= $row['email'] . ',';
            }
            $addys = substr($addys, 0, -1);
        }
        $sql = null;
        foreach ($newStatus as $svId => $info) {
            $sql .= "UPDATE `" . DB_PREFIX . "servers` SET `status`='" . $info[0] . "' WHERE `id`='$svId';";
            //email peeps about crashes, and if we were able to restart it or not.
            if ($info[0]) {
                $subject = sprintf($lang->emails->subject1, $info[1]);
                $mail_body = sprintf($lang->emails->body1, $info[1]);
                if (DEBUG) {
                    echo $mail_body;
                }
            } else {
                $subject = sprintf($lang->emails->subject1, $info[1]);
                $mail_body = sprintf($lang->emails->body1, $info[1], $info[1]);
                if (DEBUG) {
                    echo $mail_body;
                }
            }
            send_email($addys, $subject, $mail_body);
        }
        $sc->db->query($sql);
    }
}