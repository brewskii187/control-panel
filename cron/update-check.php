<?php

if (!defined('NineteeEleven')) {
    define('NineteenEleven', true);
}
if (!defined('ABSDIR')) {

    $folderName = '/control-panel/';
    $absDir = substr(__DIR__, 0, stripos(__DIR__, $folderName)) . $folderName;
    if ($absDir == $folderName) {
        define('ABSDIR', __DIR__ . '/');
    } else {

        define('ABSDIR', $absDir);
    }
}
require_once ABSDIR . 'configs/config.php';
require_once ABSDIR . 'configs/email.php';
require_once ABSDIR . 'includes/ClassLoader.php';
require_once ABSDIR . 'includes/email.php';

$lock = new LockFile(ABSDIR . 'cron/');

if ($lock->check()) {
    die('Update is locked, probably already running');
} else {
    $lock->lock();
}

set_time_limit(0);

$steamcmd = new SteamCmd;

$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

$sql = "SELECT `id` FROM `" . DB_PREFIX . "servers` WHERE `enabled` = '1' AND `auto_update`='1';";

$result = $mysqli->query($sql) or die($mysqli->error . " " . $mysqli->errno);

while ($server = $result->fetch_array(MYSQL_ASSOC)) {
    $updateResult = $steamcmd->updateCheck($server['id']);
    var_dump($updateResult);
}
$lock->unlock();
