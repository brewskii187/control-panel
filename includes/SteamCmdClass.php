<?php

if (!defined('NineteenEleven')) {
    die('No direct access allowed.');
}

class SteamCmd extends Shell {

    function __construct() {
        $this->SteamCmdDir = homeDir . "steamcmd/steamcmd.sh";
        $this->mkTmp();
        $this->callServerControl();
    }

    function __destruct() {
        if (isset($this->sc)) {
            unset($this->sc);
        }
    }

    public function callServerControl() {
        $this->sc = new ServerControl;
    }

    /**
     * Execute a command to steam cmd and return its output
     * @param type steamcmd Command line options.
     * @return array with output or boolean false
     */
    public function SteamCmdExec($cmdLine) {
        set_time_limit(0);
        //$output = $this->exec($this->SteamCmdDir . " " . trim($cmdLine));

        $output = $this->prepareSh($this->SteamCmdDir . " " . trim($cmdLine))
                ->sanitize()
                ->setExitStatus()
                ->execute()
                ->checkExitStatus();
        if ($this->es && !empty($this->response)) {
            return $output->response;
        }
        return false;
    }

    /**
     * Retrieve the server version from the steam.inf file.
     * @param type $id server id
     * @return string|boolean returns the server version, false if error;
     */
    public function getServerVersion($id, $SteamInfVersion) {

        $inf_file = $this->sc->getField($id,'steam_inf_path');

        $lines = @file($inf_file);
        if (!is_array($lines)) {
            return false;
        }
        foreach ($lines as $line) {
            if (stristr($line, $SteamInfVersion."=")) {
                $version = str_ireplace($SteamInfVersion, '', $line);  
                $version = str_ireplace('=', '', $version);
                return $version;


            }
        }
        return false;
    }

    /**
     * Checks version of server against Steam Servers.
     * @param type $id
     * @return boolean|int|string true if up to date, false if needs update, string with error message
     */
    public function checkServerVersionSteam($id) {
        #https://developer.valvesoftware.com/wiki/Steam_Application_IDs
        global $appInfo;
        $appid = $this->sc->getField($id, 'appid');

        foreach ($appInfo as $game) {
        if ($appid == $game['srvAppId']) {
              $appid = $game['clientAppId'];
                 $SteamInfVersion = $game['SteamInfVersion'];
        break;
              }
        }

        $sVersion = $this->getServerVersion($id, $SteamInfVersion);
        if ($sVersion === false) {
            return "Unable to open file";
        }


        $json = json_decode(file_get_contents("http://api.steampowered.com/ISteamApps/UpToDateCheck/v0001?appid=$appid&version=" . $sVersion . "&format=json"));
        if (DEBUG) {
            var_dump($json);
        }
        if ($json->response->success) {
            return $json->response->up_to_date;
        } else {
            $fp = fsockopen("api.steampowered.com", 80, $errno, $errstr, 30);
            if (!$fp) {
                return "Unable to connect to steam servers";
            } else {
                return "Unable to locate app id. or something is messed up with steam. really? who the fuck knows.";
            }
        }
    }

    /**
     * Sets the command line for steamcmd
     * @param type $data array contining steamuser/steampass/path/appid
     * @return type string
     */
    public function setCmdLine($data) {


        $this->cmdLine = "+login " . $data['steamuser'];

        if (isset($data['steampass']) && !empty($data['steampass'])) {
            $this->cmdLine .= " " . $data['steampass'];
        }

        $this->cmdLine .= " +force_install_dir " . $data['path'] . " +app_update " . $data['appid'] . " +quit";
        if (DEBUG) {
            echo $this->cmdLine . "<br />";
        }
        return $this->cmdLine;
    }

    /**
     * Check if server is out of date, if so. Stop it, update it and restart it.
     * @param type $id Server ID to check
     * @param type $noticeOnly DEFAULT-FALSE Only check if server is out of date, return true if out of date, false if not.
     * @param type $sendEmail DEFAULT-TRUE send email to users notifying them of update?
     * @param type $restart DEFAULT-TRUE restart server after update
     * @return array key 0 true(success)|false(fail) / key1 message
     */
    public function updateCheck($id, $noticeOnly = false, $sendEmail = true, $restart = true, $force = false) {
        global $lang, $log;

        $checkVersion = $this->checkServerVersionSteam($id);
        if ($checkVersion === false || $force === true) {
            if ($noticeOnly) {
                return array(true, "Server is out of date");
            }

            $this->data = $this->sc->getField($id, 'path,steamuser,steampass,appid,name_friendly,pid_file');

            $running = $this->isRunning($this->data['pid_file'], $isFile = true);
            if ($running === true) {
                $stop = $this->sc->stop($id);
                if (!$stop) {
                    $handle = fopen($this->data['path'] . 'update.err', 'a');
                    fwrite($handle, date("F j, Y, g:i a") . " " . $lang->update->err . "\n");
                    fwrite($handle, $stop);
                    fclose($handle);
                    return array(false, $lang->update->err1);
                }
            } else {
                $restart = false;
            }
            #write the lock file to the server cant be started.
            $lock = new LockFile($this->data['path']);
            $lockit = $lock->lock();
            if ($lockit[0] === false) {
                return array(false, $lockit[1]);
            }
            $this->callServerControl();

            $this->sc->setStatus($id, 3);

            if (!$this->setCmdLine($this->data)) {
                return array(false, $lang->update->err2);
            }

            if (!ALLOW_DL) { //debugging stuff
                $this->cmdLine = "+login anonymous +quit";
            }
            $log->setType($id)->setLog('updated')->makeLog();
            if ($output = $this->SteamCmdExec($this->cmdLine)) {
                $mail_body = "";
                $handle = fopen(trim($this->data['path']) . 'update.log', 'a');
                foreach ($output as $d) {
                    fwrite($handle, "$d \n");
                    if (DEBUG) {
                        echo "$d <br />";
                    }
                    if ($sendEmail) {
                        $mail_body = $mail_body . "$d \n";
                    }
                }
                fclose($handle);
                unset($d);
                unset($output);
                $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
                $em = $mysqli->query("SELECT `email` from " . DB_PREFIX . "users WHERE `rec_email` = '1';");

                if ($em->num_rows >= 1) {
                    $emails = '';
                    while ($emailAddy = $em->fetch_array(MYSQLI_ASSOC)) {
                        $emails .= $emailAddy['email'] . ", ";
                    }

                    $emails = substr(trim($emails), 0, -1);
                    $subject = sprintf($lang->update->succ, $this->data['name_friendly']);
                    send_email($emails, $subject, $mail_body);
                }
                $mysqli->close();
            }

            #unlock the server
            $lock->unlock();
            unset($lock);
            #start the server if it was previously running.
            if ($restart) {
                if ($this->sc->start($id) !== true) {
                    return array(true, $lang->update->succ1);
                }
            }
            return array(true, $lang->update->succ2);
        } elseif ($checkVersion !== true) { #returns teh error message from checkServerVersionSteam
            return array(false, $checkVersion);
        }
        return array(false, $lang->update->toDate);
    }

}
