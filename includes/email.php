<?php
if (!defined('NineteenEleven')) {
    die('No direct access allowed.');
}
if(!defined('ABSDIR')){

    $folderName = '/control-panel/';
    $absDir = substr(__DIR__,0,stripos(__DIR__, $folderName)).$folderName;
    if ($absDir == $folderName) {
        define('ABSDIR', __DIR__ .'/');
    }else{

    define('ABSDIR', $absDir);
    }
}
function send_email($to,$subject,$mail_body){
    global $mail;
    $mailHeader = "From: ". $mail['name'] . " <" . $mail['email'] . ">\r\n";


    if ($mail['adminDump']) {
        $to = $to .', ' . $mail['BCC'];
    }

    @mail($to, $subject, $mail_body, $mailHeader); 
}