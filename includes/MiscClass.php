<?php

if (!defined('NineteenEleven')) {
    die('No direct access allowed.');
}

Class Misc {

    /**
     * Generates a random string.
     * @param type $length length of randomness you want.
     * @param type $chars default false, if true add special characters to alphabet
     * @return type string containing random characters
     */
    public static function random($length, $chars = false) {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        if ($chars) {
            $alphabet = $alphabet . "!@#$%^&*()_+";
        }
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); #turn the array into a string
    }

    public static function getGameTypeGQ($appid) {
        global $appInfo;
        foreach ($appInfo as $game) {
            if ($game['srvAppId'] == $appid) {
                return $game['gq'];
            }
        }

//        switch ($appid) {
//            case '232250':
//                return 'tf2';
//                break;
//
//            case '730':
//                return 'source';
//                break;
//            default:
//                return 'source';
//                break;
//        }
    }

    public static function timeConvert($timer) {

        $timer['time'] = $timer['stop'] - $timer['start'];
//var_dump($timer);
        if ($timer['time'] < 60) {
            return $timer['time'] . " seconds";
        } else {
            return gmdate('i:s', $timer['time']) . " minutes";
        }
    }

    public static function secondsToTime($seconds) {
        $dtF = new DateTime("@0");
        $dtT = new DateTime("@$seconds");
        return $dtF->diff($dtT)->format('%ad%hh%im%ss');
    }

    public static function insertFromArray($array, $table) {
        return "INSERT INTO " . DB_PREFIX . $table . " (`" . implode("`, `", array_keys($array)) . "`) VALUES ('" . implode("', '", $array) . "')";
    }

    public static function checkboxCheck($field) {
        if (empty($field) || $field > 1 || $field < 0) {
            return 0;
        }
        return $field;
    }

    public static function uiMsg($msg, $class = '') {
        print("<script>addMsg(\"<div class='$class'>$msg</div>\")</script>");
    }

    public static function makeDl($file) {
        $filename = explode("/", $file);

        $filename = array_pop($filename);
//send the headers
        header("Content-type: application/octet-stream");

        header("Content-disposition: attachment;filename=$filename");
//send the file to the client.
        readfile($file);
    }

}
