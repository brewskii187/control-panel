<?php

if (!defined('NineteenEleven')) {
    die('Direct Access Not Permitted');
}

class perms {
    /*
     * can start servers
     */

    const start = 1;

    /*
     * can stop servers
     * can restart servers
     */
    const stop = 2;

    /*
     * can manually update servers
     */
    const update = 4;

    /*
     * can edit servers configs
     * can back-up servers
     */
    const edit = 8;

    /*
     * can create servers
     * can copy servers
     * can restore servers
     */
    const create = 16;

    /*
     * can delete servers
     */
    const delete = 32;
    const schedule = 64;
    const rcon = 128;
    /*
     * full root
     */
    const root = 256; //permissions to give should be 511 for full admin, that will include all the previous permissions

    function __destruct() {
        if (isset($this->db)) {
            $this->db->close();
        }
    }

    private function connectDb() {
        $this->db = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    }

    function getUserPerms($id) {

        if (!isset($this->db)) {
            self::connectDb();
        }

        $sql = "SELECT `permissions` FROM " . DB_PREFIX . "users WHERE `id` = '$id'";

        $result = $this->db->query($sql) or die($this->db->error . " " . $this->db->errno);

        if ($result->num_rows == 1) {
            $userPerms = $result->fetch_array(MYSQLI_ASSOC);
            return $userPerms['permissions'];
        }

        return false;
    }

    function setUserPerms($id, $perms) {

        if (!isset($this->db)) {
            self::connectDb();
        }

        $sql = "UPDATE " . DB_PREFIX . "users SET `permissions`='$perms' WHERE `id` = '$id'";

        $this->db->query($sql) or die($this->db->error . " " . $this->db->errno);

        if ($this->db->affected_rows == 1) {
            return true;
        }

        return false;
    }

}
