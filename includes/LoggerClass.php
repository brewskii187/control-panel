<?php

if (!defined('NineteenEleven')) {
    die('Direct Access Not Permitted');
}
$log = new sqlLog;

/**
 * $log->setType($id)->setLog('started')->makeLog();
 */
Class sqlLog {

    function __construct() {
        $this->db = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    }

    function __destruct() {
        $this->db->close();
    }

    public function setType($type) {
        $this->type = $type;
        return $this;
    }

    public function setLog($log) {
        $this->log = $log;
        return $this;
    }

    protected function setUser() {
        if (isset($this->forcedUser)) {
            $this->user = $this->forcedUser;
            unset($this->forcedUser);
        } elseif (isset($_SESSION['username'])) {
            $this->user = $_SESSION['username'];
        } else {
            $this->user = 'Automated Task';
        }
        return $this;
    }

    public function forceUser($name) {
        $this->forcedUser = $name;
        return $this;
    }

    public function makeLog() {
        if (!isset($this->log)) {
            $this->lastResult = false;
            return $this;
        }
        if (!isset($this->type)) {
            $this->type = 'other';
        }

        $this->setUser();

        $sql = sprintf("INSERT INTO `" . DB_PREFIX . "logs` (`type`,`user`,`log`,`timestamp`) VALUES ('%s','%s','%s','%s');", $this->db->escape_string($this->type), $this->db->escape_string($this->user), $this->db->escape_string($this->log), date('U'));
        $this->db->query($sql);
        if ($this->db->affected_rows >= 1) {
            $this->lastResult = true;
        } else {
            $this->lastResult = $this->db->error . " " . $this->db->errno;
        }
        return $this;
    }

}

/*
Class logSQLite extends SQLite3 {

    function __construct($dbName) {
        $this->open("$dbName.db");
    }

    function __destruct() {
        $this->close();
    }

    protected function num_rows($table, $row, $data) {

        $rowCnt = $this->query("SELECT COUNT(*) as count FROM $table WHERE $row='$data';");

        $row = $rowCnt->fetchArray();

        return $row['count'];
    }

}

Class logFile {

    function __construct($fileName) {
        $this->file = fopen($fileName, 'a+');
        if (!$this->file) {
            die("Failed to open or create file");
        }
    }

    function __destruct() {
        fclose($this->file);
    }

}
*/