<?php

if (!defined('NineteenEleven')) {
    die('No direct access allowed.');
}

class ServerControl extends Shell {

    function __construct() {
        $this->db = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
        $this->mkTmp();
    }

    function __destruct() {
        $this->db->close();
    }

    /**
     *
     * @param type $id Server ID
     * @param type $field Field name to fetch from database, can be multiple seperated by comma
     * @return array|string|boolean containing field(s) from database if multiple fields are selected, will return string if single. boolean false if no result.
     */
    public function getField($id, $field) {
        $result = $this->db->query("SELECT $field FROM `" . DB_PREFIX . "servers` WHERE id='$id';")or die($this->db->error . " " . $this->db->errno . " " . __LINE__);
        if ($result->num_rows == 1) {
            $data = $result->fetch_array(MYSQLI_ASSOC);

            if (strpos($field, ',') !== false || $field == '*') {
                return $data;
            }
            return $data[$field];
        }
        return false;
    }

    /**
     *
     * @param type $old String to search for
     * @param type $new String to replace
     * @param type $data data to search for replacements.
     * @return type string Will return new string if match was made, old string if not.
     */
    public function pathChanger($old, $new, $data) {
        $check = str_replace($old, $new, $data, $c);
        if ($c >= 1) {
            return $check;
        }
        return $data;
    }

    /**
     *
     * @param type $id server id to start
     * @return boolean|string true if started, string with error if error.
     */
    public function start($id) {
        global $lang, $log;



        #get info from database
        $server = $this->getField($id, 'path,cmd_line,cmd_line_hidden,exec_path,name_friendly,pid_file,ip,port,enabled');

        $lock = new LockFile($server['path']);
        if ($lock->Check()) {
            return print($lang->lock->err1);
        }
        unset($lock);
        #server id isnt in the database
        if ($server === false) {
            return sprintf("<div class='error'>" . $lang->srvControl[0]->startFailed2 . "</div>", $id);
        }

        #check if the server is enabled.
        if ($server['enabled'] == '0') {
            return sprintf($lang->srvControl[0]->notEnabled, $server['name_friendly']);
        }

        $server['pid_file'] = trim($server['pid_file']);

        #see if the server is already running
        $initialCheck = $this->isRunning($server['pid_file'], $isFile = true);
        if ($initialCheck === true) {
            #the pid is running
            return sprintf("<div class='error'>" . $lang->srvControl[0]->startFailed3 . "</div>", $this->pidFromFile($server['pid_file']));
        } else {
            $secondaryCheck = @fsockopen($server['ip'], $server['port']);
            if ($secondaryCheck !== false) {
                #the server is running on ip/port
                return sprintf("<div class='error'>" . $lang->srvControl[0]->startFailed4 . "</div>", $server['ip'], $server['port']);
            }
            #close the handle.
            @fclose($secondaryCheck);
        }
        #start the server in the background
        $pid = $this->prepareSh($server['exec_path'] . " " . $server['cmd_line'] . " " . $server['cmd_line_hidden'])
                ->sanitize()
                ->execute_bg();

        #wait for it... wait for it
        sleep(11); //if server is running as root, this might cause a problem with the 10 seconds timeout of the srcds_run script.
        #see if server has started
        $pidCheck = $this->isRunning($server['pid_file'], $isFile = true);
        $kill = $this->prepareSh("kill -9 " . $this->pidFromFile($pid->pid_file))->sanitize()->setExitStatus()->execute()->checkExitStatus();
        unlink($pid->pid_file);

        if ($pidCheck === true) {

            $this->setStatus($id, 1);
            $log->setType($id)->setLog('started')->makeLog();
            return true;
        }


        return sprintf("<div class='error'>" . $lang->srvControl[0]->startFailed . " " . $pidCheck['errMessage'] . "</div>", $server['name_friendly']);
    }

    /**
     *
     * @param type $id server ID to stop
     * @return boolean|string boolean true if stopped, string with error if error.
     */
    public function stop($id, $force = false) {
        global $lang, $log;
        #get pid file loc from database
        $pidFile = trim($this->getField($id, 'pid_file'));

        #check if the server is running
        $pidCheck = $this->isRunning($pidFile, $isFile = true);

        if ($pidCheck === true || $force) {
            #if indeed it is running get the pid number
            $pid = trim($this->pidFromFile($pidFile));
            if ($pid !== false) {

                #if we got the number... fuckin kill it.
                $kill = $this->prepareSh("kill -9 " . $pid)->sanitize()->setExitStatus()->execute()->checkExitStatus(); // kill -15?
                if ($kill->es) {
                    $this->setStatus($id, 0);
                    $log->setType($id)->setLog('stopped')->makeLog();
                    return true;
                } else {
                    return $lang->srvControl[0]->stopFailed3;
                }
            } else {
                #unable to locate or open pid file
                if (DEBUG) {
                    echo "error 3 <br />";
                }

                return $lang->srvControl[0]->stopFailed;
            }
        } else {
            #server is already stopped
            if (DEBUG) {
                echo "error 4 <br />";
            }

            return $lang->srvControl[0]->stopFailed2;
        }
    }

    /**
     * Restarts a server
     * @param type $id server id to restart
     * @return boolean|string true if successfult string containing error on fail.
     */
    public function restart($id) {
        $stop = $this->stop($id);
        if ($stop === true) {
            sleep(2);
            $start = $this->start($id);
            if ($start === true) {
                return true;
            } else {
                return $start;
            }
        } else {
            return $stop;
        }
    }

    /**
     * Copys a servers files and database entries, does the renaming automatically
     * @param type $id Server ID to copy
     * @param type $newPath where to put the copy of the server.
     * @param type $restart DEFAULT=TRUE if server is running, restart it.
     * @return type array key 0 boolen result,true success false fail. key 1 error message on fail, true if copied in foreground.
     */
    public function copy($id, $newPath, $restart = true) {
        global $lang, $log;
        set_time_limit(1000);
        $path = $this->getField($id, 'path');
        $pathing = new Pathing;
        $lock = new LockFile($path);
        $lockit = $lock->lock();
        if ($lockit[0] === false) {
            return array(false, $lockit[1]);
        }
        $newPath = $pathing->makeAbs($newPath);


        #make sure we can write to these directories
        if (!mkdir($newPath, 0770, true)) {
            //"Unable to make the paths to the new server."
            return array(false, $lang->copy->err1);
        }

        $test = fopen($newPath . "test.file", 'a');
        if ($test === false) {
            echo $newPath . "<br /> unable to make file";
            //"Unable to write files to the new server path."
            return array(false, $lang->copy->err2);
        }
        fclose($test);
        @unlink($newPath . "test.file");

        #stop the server if its running
        if ($this->isRunning($this->getField($id, 'pid_file'), $isFile = true)) {
            if ($this->stop($id) !== true) {
                // "Unable to stop the running server."
                return array(false, $lang->copy->err3);
            }
        } else {
            $restart = false;
        }

        if ($path !== false) {

            $result = $this->db->query("SELECT * FROM `" . DB_PREFIX . "servers` WHERE id='$id';")or die($this->db->error . " " . $this->db->errno);

            if ($result->num_rows == 1) {
                $data = $result->fetch_array(MYSQLI_ASSOC);
            }

            $data['exec_path'] = $this->pathChanger($data['path'], $newPath, $data['exec_path']);

            $data['cmd_line'] = $this->pathChanger($data['path'], $newPath, $data['cmd_line']);

            $data['cmd_line_hidden'] = $this->pathChanger($data['path'], $newPath, $data['cmd_line_hidden']);

            $data['pid_file'] = $this->pathChanger($data['path'], $newPath, $data['pid_file']);

            $data['steam_inf_path'] = $this->pathChanger($data['steam_inf_path'], $newPath, $data['steam_inf_path']);

            $sql = sprintf("INSERT INTO `" . DB_PREFIX . "servers` (`appid`,`name_friendly`,`path`,`exec_path`,`cmd_line`,`cmd_line_hidden`,`pid_file`,`ip`,`port`,`rcon_pass`,`steam_inf_path`,`steamuser`,`steampass`,`auto_update`,`enabled`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');", $data['appid'], $data['name_friendly'] . "(2)", $newPath, $data['exec_path'], $data['cmd_line'], $data['cmd_line_hidden'], $data['pid_file'], $data['ip'], $data['port'], $data['rcon_pass'], $data['steam_inf_path'], $data['steamuser'], $data['steampass'], $data['auto_update'], $data['enabled']);


            $this->db->query($sql)or die($this->db->error . " " . $this->db->errno);
            $path = $path . "*";
            $shell = $this->prepareSh("cp -r $path $newPath")->sanitize()->setExitStatus()->execute()->checkExitStatus();
            $unlock = $lock->unlock();
            $timer['start'] = $unlock[1];
            $timer['stop'] = date('U');
            $newLock = new LockFile($newPath);
            $newLock->unlock();
            unset($newLock);
            unset($lock);

            if ($restart) {
                sleep(2);
                if ($this->start($id) === false) {
                    //"Server has been copied, but failed to start."
                    return array(false, $lang->copy->err4);
                }
            }

            //send an email here?

            if (!$shell->es) {
                return array(false, $lang->shellClass[0]->err3);
            }
            if ($unlock[0] === true) {
                $log->setType($id)->setLog('copied')->makeLog();
                return array(true, Misc::timeConvert($timer));
            } else {
                return array(false, $unlock[1]);
            }
        } else {
            //"Cannot get needed information from the database."
            return array(false, $lang->copy->err5);
        }
    }

    /**
     * deletes a servers files, and database entries
     * @param type $id ID of server to delete
     * @return boolean true on success. false on fail
     */
    public function delete($id) {
        global $log;
        $server = $this->getField($id, 'path,pid_file');

        if ($this->isRunning($server['pid_file'], $isFile = true)) {
            if ($this->stop($id) !== true) {
                return false;
            }
        }
        set_time_limit(600);
        $shell = $this->prepareSh("rm -r " . $server['path'])->sanitize()->setExitStatus()->execute()->checkExitStatus();
        if ($shell->es) {
            $this->db->query("DELETE FROM " . DB_PREFIX . "server_order WHERE id='$id';");
            $this->db->query("DELETE FROM " . DB_PREFIX . "servers WHERE id='$id';")or die($this->db->error . " " . $this->db->errno);
            
            if ($this->db->affected_rows >= 1) {
                $log->setType($id)->setLog('deleted')->makeLog();
                return true;
            }
        }

        return false;
    }

    /**
     *
     * @param type $id
     * @param type $compression
     * @param type $password
     * @return type
     */
    public function backup($id, $compression = 'z', $password = false) {
        //z is gunzip j is bzip2
        global $lang, $log;
        $timer['start'] = date('U');
        if (empty($id)) {
            return array(false, $lang->backup->err1);
        }
        set_time_limit(6000);
        $server = $this->getField($id, "*");
        $compression = strtolower($compression);
        $sql_fail = false;
        if (!is_dir(BACKUPS_DIR)) {
            mkdir(BACKUPS_DIR, 0770, true) or die($lang->backup->err2);
        }
        $lock = new LockFile($server['path']);
        $lockit = $lock->lock();
        if ($lockit[0] === false) {
            return array(false, $lockit[1]);
        }

        if ($this->isRunning($server['pid_file'], $isFile = true)) {
            $this->stop($id);
            $restart = true;
        } else {
            $restart = false;
        }
        $this->setStatus($id, 4);

        #take the sql row and write it to a file.
        $sql_backup = Misc::insertFromArray($server, 'servers');
        $sql_backup_path = $server['path'] . "sql_backup";
        $handle = @fopen($sql_backup_path, 'w+');
        if ($handle === false) {
            $sql_fail = true;
        } else {
            @fwrite($handle, $sql_backup);
            @fclose($handle);
        }

        #file name of the backed up file.
        $backupPath = BACKUPS_DIR . str_replace(" ", "_", $server['name_friendly']) . "_" . date('U');

        if ($password === false) {
            if ($compression == 'j') {
                $backupPath = $backupPath . ".tar.bz";
            } elseif ($compression == 'z') {
                $backupPath = $backupPath . ".tar.gz";
            }

            $shell = $this->prepareSh("tar -pc" . $compression . "f " . $backupPath . " " . $server['path'])
                    ->sanitize()
                    ->setExitStatus()
                    ->execute()
                    ->checkExitStatus();
            $has_pw = 0;
        } else {
            $has_pw = 1;

            $zip = $this->prepareSh("which zip")->sanitize()->execute();
            $compression = 'zip';
            $backupPath = $backupPath . ".zip";
            if (!empty($zip->response) && $zip->es) {
                $shell = $this->prepareSh($zip->response[0] . "-r -q -P $password " . $server['path'] . " " . $backupPath)
                        ->sanitize()
                        ->setExitStatus()
                        ->execute()
                        ->checkExitStatus();
            } else {
                return array(false, $lang->backup->err3);
            }
        }

        if (!$shell->es) {
            return array(false, $lang->shellClass[0]->err3);
        }

        $sql = sprintf("INSERT INTO `" . DB_PREFIX . "backups` (`server_name`,`srv_path`,`backup_path`,`compression`,`has_pw`,`timestamp`,`username`) VALUES ('%s','%s','%s','%s','%s','%s','%s');", $server['name_friendly'], $server['path'], $backupPath, $compression, $has_pw, date('U'), $_SESSION['username']);


        $this->db->query($sql) or die($this->db->error);
        $msg = '';
        if ($this->db->affected_rows == 0) {
            $msg = $msg . $lang->backup->succ1;
        }

        if (!$sql_fail) {
            @unlink($sql_backup_path);
            $msg = $msg . $lang->backup->succ2;
        } else {
            $msg = $msg . $lang->backup->succ3;
        }

        $lock->unlock();
        unset($lock);

        if ($restart) {
            $this->start($id);
        } else {
            $this->setStatus($id, 0);
        }
        $timer['stop'] = date('U');
        $msg = $msg . sprintf($lang->misc->timer, Misc::timeConvert($timer));
        $log->setType($id)->setLog('backed up')->makeLog();
        return array(true, $msg);
    }

    public function delBackup($file) {
        if (is_file($file)) {
            $this->db->query("DELETE FROM `" . DB_PREFIX . "backups` WHERE `backup_path` = '" . $this->db->escape_string($file) . "';");

            if ($this->db->affected_rows == 1) {
                if (unlink($file)) {
                    return true;
                }
            }
            return false;
        }
    }

    /**
     *
     * @param type $file
     * @param type $password
     * @return type
     */
    public function restore($file, $password = false) {
        //also restore server row in sql
        //be sure to remove the lock file.-done
        //
        //tar zxvf linux-source<version>.tar.gz
        //tar xjvf linux-source<version>.tar.bz2
        //mv server to random, restore server, then delete old server.

        $sql = "SELECT * FROM `" . DB_PREFIX . "backups` WHERE `backup_path` = '" . $this->db->escape_string($file) . "';";
        $result = $this->db->query($sql);

        if ($result->num_rows != 1) {
            return array(false, 'Recieved unexpected results from database (' . $this->db->errno . ')');
        }

        $bu = $result->fetch_array(MYSQLI_ASSOC);

        if (is_dir($bu['srv_path'])) {
            $pathing = new Pathing;

            $srvRestoreMvPath = $pathing->rTSlash($bu['srv_path']) . "_" . $pathing->aTSlash(Misc::random(6));

            $mv = $this->prepareSh('mv -v ' . $bu['srv_path'] . ' ' . $srvRestoreMvPath)->sanitize()->setExitStatus()->execute()->checkExitStatus();


            if (!$mv->es) {
                return array(false, "unable to move files at current restore location.");
            }

            if (!mkdir($bu['srv_path'], 0770, true)) {
                return array(false, "unable to create the needed directory.");
            }
        }


        if ($bu['compression'] == ('j' || 'z') && !$bu['has_pw']) {

            $output = $this->prepareSh("tar -" . $bu['compression'] . "xvf " . $bu['backup_path'] . " --directory /")
                    ->sanitize()
                    ->setExitStatus()
                    ->execute()
                    ->checkExitStatus();
        } elseif ($bu['compression'] == 'zip' && !$bu['has_pw']) {
            //this code should never get used as zip is only when there is a password.
            $unzip = $this->prepareSh("which unzip")->sanitize()->setExitStatus()->execute()->checkExitStatus();

            if ($unzip->es) {

                $output = $this->prepareSh($unzip->response[0] . " " . $bu['backup_path'] . " -d" . $bu['srv_path'])
                        ->sanitize()
                        ->setExitStatus()
                        ->execute()
                        ->checkExitStatus();
            } else {

                return array(false, "You must have the unzip utility installed!");
            }
        } elseif ($bu['compression'] == 'zip' && $bu['has_pw']) {
            $unzip = $this->prepareSh("which unzip")->sanitize()->setExitStatus()->execute()->checkExitStatus();

            if ($unzip->es) {

                $output = $this->prepareSh($unzip->response[0] . " -v -P $password " . $bu['backup_path'] . " -d" . $bu['srv_path'])
                        ->sanitize()
                        ->setExitStatus()
                        ->execute()
                        ->checkExitStatus();
            } else {

                return array(false, "You must have the unzip utility installed!");
            }
        }

        $lock = new LockFile($bu['srv_path']);
        $lock->unlock();

        #if the server was restored and we moved the old server
        if ($output->es && isset($srvRestoreMvPath)) {
            #delete the old server
            $rm = $this->prepareSh("rm -r -v " . $srvRestoreMvPath)->sanitize()->setExitStatus()->execute()->checkExitStatus();

            //$sql_restore = file_get_contents($bu['srv_path'].'sql_dump');


            if ($rm->es) {
                return array(true, "Your server has been restored.");
            }
            #if the server didnt get restored
        } elseif (!$output->es && isset($srvRestoreMvPath)) {
            #delete the new server directory
            $rm = $this->prepareSh("rm -r -v " . $bu['srv_path'])->sanitize()->setExitStatus()->execute()->checkExitStatus();
            if (!mkdir($bu['srv_path'], 0770, true)) {
                if ($rm->es) {
                    $mvBack = $this->prepareSh('mv -v ' . $srvRestoreMvPath . ' ' . $bu['srv_path']);
                }
            }
            if ($mvBack->es) {
                return array(false, "Something went wrong an we were unable to restore the server.");
            }
        }
    }

    /**
     * status 1 = running, 0 = stopped, 3 = updating, 4 = backingup
     * @param type $id
     * @param type $status
     * @return boolean
     */
    public function setStatus($id, $status) {
        $sql = "UPDATE `" . DB_PREFIX . "servers` SET `status`='$status' WHERE `id`='$id';";

        $result = $this->db->query($sql);
        if ($this->db->affected_rows == 1) {
            return true;
        }

        return false;
    }

    public function getStatus($id) {
        $sql = "SELECT `status` FROM `" . DB_PREFIX . "servers` WHERE `id`='$id';";
        $result = $this->db->query($sql);
        if ($result->num_rows == 1) {
            $array = $result->fetch_array(MYSQLI_ASSOC);
            return $array['status'];
        }
        return false;
    }

    public function rconQueryAll($cmd) {
        require_once ABSDIR . "includes/RconClass.php";
        $srcds_rcon = new srcds_rcon();

        $fail = 0;
        $result = $this->db->query("SELECT `ip`,`port`,`rcon_pass` FROM " . DB_PREFIX . "servers WHERE `status` = '1' AND `enabled` = '1';");
        while ($server = $result->fetch_array(MYSQLI_ASSOC)) {
            $OUTPUT = @$srcds_rcon->rcon_command($server['ip'], $server['port'], $server['rcon_pass'], $data['cmd']);
            if ($OUTPUT === false) {
                $fail++;
            }
        }
        if ($fail === 0) {
            return true;
        } else {
            return $fail;
        }
    }

    /**
     * Prints a combo box with list of servers friendly names, value of options is the server ID
     * @return boolean true if combobox was made, print error if not.
     */
    public function makeComboBox($class = '', $id = '', $enabledOnly = false) {
        global $lang;

        $sql = "SELECT `id`,`name_friendly` FROM `" . DB_PREFIX . "servers` ";

        if ($enabledOnly) {
            $sql = $sql . "WHERE enabled = 1;";
        } else {
            $sql = $sql . "WHERE 1;";
        }

        $result = $this->db->query($sql) or die($this->db->error . " " . $this->db->errno);

        if ($result->num_rows > 0) {

            echo "<div class='comboBox $class'><select name='server' id='$id'>"
            . "<option value='0'>Select a server</option>";

            while ($server = $result->fetch_array(MYSQLI_ASSOC)) {
                printf("<option value='%s'>%s</option>", $server['id'], $server['name_friendly']);
            }
            echo "</select></div>";
            return true;
        } else {
            //if there is nothing in the database print error, die.
            $_SESSION['message'] = sprintf("<div class='error'>%s</div>", $lang->editSrv[0]->err1);
            //$this->db->close(); //shouldnt need this because of deconstructor.
            exit();
        }
    }

}
