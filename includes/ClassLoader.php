<?php

/*
 * loads all our classes.
 */
if (!defined('NineteenEleven')) {
    die('No direct access allowed.');
}
if (!defined('ABSDIR')) {

    $folderName = '/control-panel/';
    $absDir = substr(__DIR__, 0, stripos(__DIR__, $folderName)) . $folderName;
    if ($absDir == $folderName) {
        define('ABSDIR', __DIR__ . '/');
    } else {

        define('ABSDIR', $absDir);
    }
}
$isMobile = preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);

//$isMobile = true;

require_once ABSDIR . 'includes/LanguageClass.php';
require_once ABSDIR . 'includes/ShellClass.php';
require_once ABSDIR . 'includes/SteamCmdClass.php';
require_once ABSDIR . 'includes/ServerControllerClass.php';
require_once ABSDIR . 'includes/PermissionsClass.php';
require_once ABSDIR . 'includes/FileHandlerClass.php';
require_once ABSDIR . 'includes/SysInfoClass.php';
require_once ABSDIR . 'includes/MiscClass.php';
require_once ABSDIR . 'includes/supported_games.php';
require_once ABSDIR . 'includes/LoggerClass.php';
