<?php

/*
 * Build the naviagion bar. outside urls allowed, otherwise name of the file in
 * the pages/ folder to be loaded. only .php and .phtml will be loaded.
 * for sub-nav menus the first item must be 'href' with the location of the link.
 */
if (!defined('NineteenEleven')) {
    die('No direct access allowed.');
}
$nav = array('Home' => '');

if ($_SESSION['permissions'] & (perms::edit | perms::create | perms::delete)) {
    $nav['Server Management'] = array('href' => 'EditServer');
    $nav['Server Management']['Edit Servers'] = 'EditServer';
}
if ($_SESSION['permissions'] & perms::create) {
    $nav['Server Management']['Create Server'] = 'CreateServer';
    //$nav['Create Server'] = array('href' => 'CreateServer', 'Create Server' => 'CreateServer', 'Import Server' => 'ImportServer');
}
if ($_SESSION['permissions'] & (perms::edit | perms::create)) {
    $nav['Server Management']['Backup Management'] = 'BackupManagement';
    if (is_file(ABSDIR . 'pages/adverts.php')) {
        $nav['Server Management']['Advertisements'] = 'adverts';
    }
}
if ($_SESSION['permissions'] & perms::root) {
    $nav['Root'] = array('href' => 'UserManagement');
    $nav['Root']['User Management'] = 'UserManagement';
    $nav['Root']['Root Controls'] = 'root';
    $nav['Root']['Re-order Servers'] = 'ReorderServers';
} else {
    $nav['Account Management'] = 'UserManagement';
}
if ($_SESSION['permissions'] & perms::rcon) {
    $nav['File Browser'] = 'FileExplorer';
    $nav['Log Viewer'] = 'LogViewer';
}

$nav['Log Out'] = 'LogOut';
//echo"<pre>";
//var_dump($nav);
//echo"</pre>";






if ($isMobile) {

    echo "<center>
      <div id=\"dl-menu\" class=\"dl-menuwrapper\">
      <button class=\"dl-trigger\">Open Menu</button>
	<ul class=\"dl-menu\">";

    foreach ($nav as $name => $loc) {

        if (is_array($loc)) {

            echo "<li><a href='home.php?loc=" . $loc['href'] . "'>$name</a>";

            array_shift($loc); #remove the href value of the sub-menu

            echo "<ul class=\"dl-submenu\">";

            foreach ($loc as $name => $loc) {

                echo checkLink($name, $loc);
            }

            echo "</ul></li>";

            continue;
        }

        echo checkLink($name, $loc);
    }

    echo "</ul></div></center>";
} else {


    echo "<nav>
	<ul class='mainMenu'>";

    foreach ($nav as $name => $loc) {

        if (is_array($loc)) {

            echo "<li><a href='home.php?loc=" . $loc['href'] . "'>$name</a>";

            array_shift($loc); #remove the href value of the sub-menu

            echo "<ul>";

            foreach ($loc as $name => $loc) {

                echo checkLink($name, $loc);
            }

            echo "</ul></li>";

            continue;
        }

        echo checkLink($name, $loc);
    }

    echo "</ul></nav>";
}

function checkLink($name, $path) {

    if (preg_match('#^http(s)?://#', $path)) {
        return "<li><a href='$path' target='_BLANK'>$name</a></li>";
    }

    return "<li><a href='home.php?loc=$path'>$name</a></li>";
}
