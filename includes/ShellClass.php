<?php

/**
 * exec exec_quiet escape are being phased out, as of now they shoudnt be used at all. were going with the strait OOP
 * method below.
 */
if (!defined('NineteenEleven')) {
    die('No direct access allowed.');
}

class Shell {

    function __construct() {
        $this->mkTmp();
    }

    public function mkTmp() {
        $this->tmpDir = '/tmp/control-panel/';
        if (!is_dir($this->tmpDir)) {
            mkdir($this->tmpDir, 0770);
        }
    }

    /**
     *  --- UNUSED CODE ---replace double quotes with single and,escape a shell command,
     * @param type shell Command
     * @return String escaped
     */
    public function escape($cmd) {
        //echo  "&#34";
        $cmd = str_replace('&#34;', "'", $cmd);
        //return escapeshellcmd($cmd);
        return $cmd;
    }

    /**
     * --- UNUSED CODE ---Send out a command to the linux shell and reurn its output as an array
     * @param type $cmd = linux shell command
     * @return array with output or boolean false
     */
    public function exec($cmd) {
        exec($this->escape($cmd), $output);
        if (isset($output) && is_array($output)) {
            return $output;
        }
        return false;
    }

    /**
     *  --- UNUSED CODE ---Send out a command to the linux shell and redirect its output to /dev/null/
     * PHP will NOT wait for an output to execute the rest of the script
     * @param type $cmd command to execute in background
     * @return type string containing the name of the PID file for the running process
     */
    public function exec_quiet($cmd) {
        $cmd = $this->escape($cmd);
        $this->pid_file = "pid_" . Misc::random(8);
        #redirect stdio and stderr to /dev/null so it produces no output.
        exec($cmd . " > /dev/null 2>/dev/null & echo $! >> " . $this->tmpDir . $this->pid_file);
        return $this->tmpDir . $this->pid_file;
    }

    /**
     * Check if a process is running.
     * @param type $pid pid number to check
     * @param type $isFile DEFAULT=FALSE will read the first line of a file for a pid
     * @return boolean true if running, false if not. array with error first key is error code 2nd key is message.
     */
    public function isRunning($pid, $isFile = false) {
        global $lang;

        if ($isFile && is_file($pid)) {

            $pid = $this->pidFromFile($pid);
            if ($pid === false) {
                $err = array('errCode' => '0', 'errMessage' => sprintf($lang->shellClass[0]->err1, $pid));

                if (DEBUG) {
                    echo "error 0 <br />";
                }
                return $err;
            }
        }


        if (is_dir("/proc/$pid/")) {
            return true;
        } else {
            //fall back, shouldnt get used
            $result = shell_exec(sprintf("ps %d", $pid));
            if (count(preg_split("/\n/", $result)) > 2) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get a pid from the first line of a file.
     * @param type $file
     * @return string|boolean string with pid or false if file doesnt exist
     */
    public function pidFromFile($file) {
        if (is_file($file)) {
            $pid = file_get_contents($file);
            if ($pid !== false) {

                return trim($pid);
            }
        }

        return false;
    }

    //new shell code.
    /**
     *
     * usage example:
     * $this->prepareSh("cp -r $path $newPath")->sanitize()->setExitStatus()->execute()->checkExitStatus();
     * $this->prepareSh("cp -r $path $newPath")->sanitize()->execute_bg();
     */

    /**
     * set out command to be sent to shell
     * @param type $cmd
     * @return \Shell
     */
    public function prepareSh($cmd) {
        $this->cmd = $cmd;
        return $this;
    }

    /**
     * escape it or something
     * @return \Shell
     */
    public function sanitize() {
        //echo  "&#34";
        $this->cmd = str_replace('&#34;', "'", $this->cmd);
        //$this->cmd = escapeshellcmd($this->cmd);
        return $this;
    }

    /**
     * prepare the exit status result
     * @return \Shell
     */
    public function setExitStatus() {
        $this->cmd .="; echo $?";
        return $this;
    }

    /**
     * execute a shell command and return it in the array $this->response
     * @return \Shell
     */
    public function execute() {
        exec($this->cmd, $this->response);
        return $this;
    }

    /**
     * runs a shell command in the background so PHP doesnt wait for a response.
     * @return \Shell
     */
    public function execute_bg() {
        $this->pid_file = $this->tmpDir . "pid_" . Misc::random(8);
        #redirect stdio and stderr to /dev/null so it produces no output.
        exec($this->cmd . " > /dev/null 2>/dev/null & echo $! >> " . $this->pid_file);
        return $this;
    }

    /**
     * check the exit status of the shell command we sent
     * @return \Shell ->es_val raw result from shell, ->es true if exit status of 0, false otherwise.
     */
    public function checkExitStatus() {
        $this->es_val = array_pop($this->response);
        if (stristr($this->es_val, '0') !== false) {
            $this->es = true;
        } else {
            $this->es = false;
        }
        return $this;
    }

}
