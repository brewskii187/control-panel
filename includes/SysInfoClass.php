<?php

if (!defined('NineteenEleven')) {
    die('Direct Access Not Permitted');
}

class SysInfo extends Shell {
    /*
     * Created array wil file contents, null if no file.
     */

    protected function readFile() {
        if (is_file($this->file)) {
            $this->fileContents = @file($this->file);
        } else {
            $this->fileContents = array(null);
        }
        return $this;
    }

    protected function getVal() {
        $units = array('kB', 'mb', 'gb');

        return trim(str_ireplace($units, '', str_ireplace($this->search . ":", '', $this->line)));


        //return $this;
    }

    public function memInfo() {
        $this->file = "/proc/meminfo";
        $this->readFile();
        if (isset($this->fileContents)) {
            foreach ($this->fileContents as $this->line) {

                $this->search = "MemFree";
                if (preg_match('/^' . $this->search . '/i', $this->line)) {
                    $this->MemFree = $this->getVal();
                }

                $this->search = "MemTotal";
                if (preg_match('/^' . $this->search . '/i', $this->line)) {
                    $this->MemTotal = $this->getVal();
                }
                $this->search = "Cached";

                if (preg_match('/^' . $this->search . '/i', $this->line)) {
                    $this->Cached = $this->getVal();
                }
            }
        }
        return $this;
    }

    public function MemStats() {
        $fh = new FileHandler;

        if (!isset($this->MemFree) || !isset($this->MemTotal)) {
            $this->memInfo();
        }

        $this->MemStats = array('free' => $fh->sizeDispGetter($this->MemFree * 1024),
            'total' => $fh->sizeDispGetter($this->MemTotal * 1024),
            'cached' => $fh->sizeDispGetter($this->Cached * 1024),
            'precent' => round(($this->MemFree / $this->MemTotal) * 100, 3));
        return $this;
    }

    public function sysLoad() {
        $this->file = "/proc/loadavg";
        $this->readFile();
        $this->fileContents = explode(" ", $this->fileContents[0]);

        $this->load5Min = $this->fileContents[0];
        $this->load10Min = $this->fileContents[1];
        $this->load15Min = $this->fileContents[2];
        return $this;
    }

    public function upTime() {
        $this->file = "/proc/uptime";
        $this->readFile();
        $this->fileContents = explode(" ", $this->fileContents[0]);

        $this->upTime = Misc::secondsToTime(round($this->fileContents[0]));
        return $this;
    }

    public function PidFileSetter($file) {
        $this->PidFile = $file;
        return $this;
    }

    public function PidMem() {
        if (isset($this->PidFile) && !empty($this->PidFile)) {

            $this->pid = $this->pidFromFile($this->PidFile);
            //var_dump($this);
            if (empty($this->pid)) {
                $this->PidMem = '0';
                return $this;
            }

            $this->file = "/proc/" . $this->pid . "/status";

            $this->readFile();
            $fh = new FileHandler;
            foreach ($this->fileContents as $this->line) {
                $this->search = "VmRSS";
                if (preg_match('/^' . $this->search . '/i', $this->line)) {
                    $this->PidMem = $fh->sizeDispGetter($this->getVal() * 1024);
                    return $this;
                }
            }
            $this->PidMem = '<div class="error inline">Err</div>';
            return $this;
        }
    }

    public function cpuStats() {
        $top = $this->prepareSh('top -b -n 1 | grep -i ^\%cpu')->setExitStatus()->execute()->checkExitStatus();
        if ($top->es) {
            $top = explode(",", $top->response[0]);
            $top = trim(preg_replace('/^\%?Cpu\(s\)?:?/i', '', $top[0]));
            $this->cpuStats = trim(str_ireplace('us', '', $top));
            return $this;
        }
    }

}
