<?php

/* All information in this file is made publicy visible via json/supported_games.php,
 * DO NOT add passwords or any other sensative information to this file!
 */
if (!defined('NineteenEleven')) {
    die('Direct Access Not Permitted');
}

//https://developer.valvesoftware.com/wiki/Dedicated_Servers_List
//(''server appid','game name','client appid','folder name','[OPTION] for steamcmd');
$appInfo = array(
    //tf2-
    array('srvAppId' => '232250', 'game' => 'Team Fortress 2', 'clientAppId' => '440', 'folderName' => 'tf', 'cmdLineGame' => 'tf', 'cmdLineOptions' => '+map ctf_2fort +maxplayers 32', 'gq' => 'tf2', 'SteamInfVersion' => 'ServerVersion'),
    //cs:go-
    array('srvAppId' => '740', 'game' => 'Counter-Strike Global Offfensive', 'clientAppId' => '730', 'folderName' => 'csgo', 'cmdLineGame' => 'csgo', 'cmdLineOptions' => '+game_type 1 +game_mode 0 -maxplayers 24 +mapgroup mg_armsrace +map ar_baggage -usercon', 'gq' => 'csgo', 'SteamInfVersion' => 'PatchVersion'),
    //dod:S-
    array('srvAppId' => '232290', 'game' => 'Day of Defeat:Source', 'clientAppId' => '300', 'folderName' => 'dod', 'cmdLineGame' => 'dod', 'cmdLineOptions' => '+map dod_anzio +maxplayers 24', 'gq' => 'dod', 'SteamInfVersion' => 'ServerVersion'),
    //hl2dm
    array('srvAppId' => '232370', 'game' => 'Half-Life 2: Deathmatch', 'clientAppId' => '320', 'folderName' => 'hl2mp', 'gq' => 'hl2dm', 'SteamInfVersion' => 'PatchVersion'),
    //l4d2-
    array('srvAppId' => '222860', 'game' => 'Left 4 Dead 2', 'clientAppId' => '550', 'folderName' => 'left4dead2', 'cmdLineGame' => 'left4dead2', 'cmdLineOptions' => '+map c5m1_waterfront +maxplayers 8', 'gq' => 'l4d2', 'SteamInfVersion' => 'PatchVersion'),
    //l4d-
    array('srvAppId' => '222840', 'game' => 'Left 4 Dead', 'clientAppId' => '500', 'folderName' => 'left4dead', 'cmdLineGame' => 'left4dead', 'cmdLineOptions' => '+map l4d_hospital01_apartment.bsp +maxplayers 8', 'gq' => 'l4d', 'SteamInfVersion' => 'PatchVersion'),
    //nuclear dawn-
    array('srvAppId' => '111710', 'game' => 'Nuclear Dawn', 'clientAppId' => '17710', 'folderName' => 'nucleardawn', 'cmdLineGame' => 'nucleardawn', 'cmdLineOptions' => '+map downtown +maxplayers 32', 'gq' => 'source', 'SteamInfVersion' => 'PatchVersion'),
    //Age of Chivalry
    array('srvAppId' => '17515', 'game' => 'Age of Chivalry', 'clientAppId' => '17510', 'folderName' => 'ageofchivalry', 'gq' => 'aoc', 'SteamInfVersion' => 'PatchVersion'),
    //Arma 3
    array('srvAppId' => '233780', 'game' => 'Arma 3', 'clientAppId' => '107410', 'folderName' => 'arma3', 'anonLogin' => false, 'gq' => 'armedassault3', 'SteamInfVersion' => 'PatchVersion'), //needs login
    //Blade Symphony
    array('srvAppId' => '228780', 'game' => 'Blade Symphony', 'clientAppId' => '225600', 'folderName' => 'berimbau', 'anonLogin' => false, 'gq' => 'source', 'SteamInfVersion' => 'PatchVersion'), //needs login //gq might not be right
    //Counter-Strike: Condition Zero
    array('srvAppId' => '232330', 'game' => 'Counter-Strike: Condition Zero', 'clientAppId' => '80', 'folderName' => 'czero', 'steamCmdOptions' => '+app_set_config "90 mod czero"', 'gq' => 'cscz', 'SteamInfVersion' => 'PatchVersion'),
    //Counter-Strike: Source-
    array('srvAppId' => '232330', 'game' => 'Counter-Strike: Source', 'clientAppId' => '240', 'folderName' => 'cstrike', 'cmdLineGame' => 'cstrike', 'cmdLineOptions' => '+maxplayers 20 +map de_dust', 'gq' => 'css', 'SteamInfVersion' => 'ServerVersion'),
    //Garry's Mod-
    array('srvAppId' => '4020', 'game' => 'Garry\'s Mod', 'clientAppId' => '4000', 'folderName' => 'garrysmod', 'cmdLineGame' => 'garrysmod', 'cmdLineOptions' => '+map gm_flatgrass +maxplayers 32', 'gq' => 'gmod', 'SteamInfVersion' => 'PatchVersion'),
    //Half-Life Deathmatch: Source
    array('srvAppId' => '255470', 'game' => 'Half-Life Deathmatch: Source', 'clientAppId' => '360', 'folderName' => 'hl1mp', 'gq' => 'hldm', 'SteamInfVersion' => 'PatchVersion'),
//
//      array('srvAppId' => '237410', 'game' => 'Insurgency 2014', 'clientAppId' => '', 'folderName' => ''),
//
//    array('srvAppId' => '17705', 'game' => 'Insurgency', 'clientAppId' => '', 'folderName' => ''),
    //Just Cause 2: Multiplayer
    array('srvAppId' => '261140', 'game' => 'Just Cause 2: Multiplayer', 'clientAppId' => '8190', 'folderName' => 'Just Cause 2', 'gq' => 'jc2', 'SteamInfVersion' => 'PatchVersion'),
    //Killing Floor
    array('srvAppId' => '215360', 'game' => 'Killing Floor - Linux', 'clientAppId' => '1250', 'folderName' => 'killingfloor', 'gq' => 'killingfloor', 'SteamInfVersion' => 'PatchVersion'),
    //Natural Selection 2
    array('srvAppId' => '4940', 'game' => 'Natural Selection 2', 'clientAppId' => '4920', 'folderName' => 'Natural Selection 2', 'anonLogin' => false, 'gq' => 'ns2', 'SteamInfVersion' => 'PatchVersion'), //needs login
    //No More Room in Hell
    array('srvAppId' => '224260', 'game' => 'No More Room in Hell', 'clientAppId' => '224260', 'folderName' => 'nmrih', 'anonLogin' => false, 'gq' => 'source', 'SteamInfVersion' => 'PatchVersion'), //needs login
    //Pirates, Vikings, and Knights II
    array('srvAppId' => '17575', 'game' => 'Pirates, Vikings, and Knights II', 'clientAppId' => '17570', 'folderName' => 'pvkii', 'SteamInfVersion' => 'PatchVersion'), //gq???
    //Red Orchestra
    array('srvAppId' => '223250', 'game' => 'Red Orchestra Linux', 'clientAppId' => '35450', 'folderName' => 'ro2', 'anonLogin' => false, 'gq' => 'redorchestra2', 'SteamInfVersion' => 'PatchVersion'), //needs login
    //Serious Sam 3
    array('srvAppId' => '41080', 'game' => 'Serious Sam 3', 'clientAppId' => '41070', 'folderName' => 'SeriousSam3', 'anonLogin' => false, 'SteamInfVersion' => 'PatchVersion'), //needs login //gq?
    //The Ship
    array('srvAppId' => '2403', 'game' => 'The Ship', 'clientAppId' => '2400', 'folderName' => 'ship', 'anonLogin' => false, 'SteamInfVersion' => 'PatchVersion'), //needs login //gq?
    //Zombie Panic Source
    array('srvAppId' => '17505', 'game' => 'Zombie Panic Source', 'clientAppId' => '17500', 'folderName' => 'zps', 'gq' => 'zps', 'SteamInfVersion' => 'PatchVersion'),
    //array('srvAppId' => '90', 'game' => 'Team Fortress Classic', 'clientAppId' => '', 'folderName' => '', 'steamCmdOptions' => '+app_set_config "90 mod tfc"'),
    //array('srvAppId' => '90', 'game' => 'Day of Defeat', 'clientAppId' => '', 'folderName' => '', 'steamCmdOptions' => '+app_set_config "90 mod dod"'),
    //array('srvAppId' => '90', 'game' => 'Deathmatch Classic', '', 'folderName' => '', 'steamCmdOptions' => '+app_set_config "90 mod dmc"'),
    //array('srvAppId' => '90', 'game' => 'Ricochet', 'clientAppId' => '', 'folderName' => '', 'steamCmdOptions' => '+app_set_config "90 mod ricochet"'),
    //array('srvAppId' => '90', 'game' => 'Half-Life', 'clientAppId' => '', 'folderName' => ''),
    //array('srvAppId' => '90', 'game' => 'Half-Life: Opposing Force Server', 'clientAppId' => '', 'folderName' => '', 'steamCmdOptions' => '+app_set_config "90 mod gearbox"'),
    //array('srvAppId' => '90', 'game' => 'Counter-Strike', 'clientAppId' => '', 'folderName' => ''),
    array('srvAppId' => 'other', 'other')
);

