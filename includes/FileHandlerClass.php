<?php

if (!defined('NineteenEleven')) {
    die('Direct Access Not Permitted');
}

//http://www.php.net/manual/en/class.splfileinfo.php
class FileHandler {

    function __construct() {

    }

    public function setFile($file) {
        $this->file = $file;
        $this->splFile = new SplFileInfo($this->file);
        return $this;
    }

    public function setDir($dir) {
        $this->dir = $dir;
        return $this;
    }

    public function makeAbs() {
        $dir = $this->hideParent();
        $dir = homeDir . $this->dir . '/';
        $this->dir = $dir;
        return $this;
    }

    public function getFileSize() {
        $this->size = filesize($this->file);
        return $this;
    }

    /**
     * Will replace ../ in $this->dir
     * @return \FileHandler
     */
    private function hideParent() {
//        if (strstr($this->dir, "../")) {
//            $this->dir = str_replace("../", "", $this->dir);
//        }
        $this->dir = preg_replace('/(\.\.|\.\.\/)/', '', $this->dir);
        return $this;
    }

    public function dirSizeDispGetter($size) {
        $this->dirSize = $size;
        return $this->dirSize()->dirSizeDisp;
    }

    /**
     * changes $this->dirSize to the size of the current directory from the dir path
     * @return \FileHandler
     */
    private function dirSize() {
        $this->size = 0;
        foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($this->dirSize)) as $file) {
            $this->size+=$file->getSize();
        }
        $this->dirSizeDisp = $this->sizeDisp()->sizeDisp;
        unset($this->size);
        return $this;
    }

    /**
     * will return a pretty size. kb,mb,gb ect
     * @param type $size size in bytes
     * @return type
     */
    public function sizeDispGetter($size) {
        $this->size = $size;
        return $this->sizeDisp()->sizeDisp;
    }

    /**
     * sets this->sizeDisplay to pretty size
     * @param $this->size size in bytes
     * @return \FileHandler
     */
    public function sizeDisp() {
        $this->sizeDisp = $this->size;
        if ($this->size < 1024) {
            $this->sizeDisp = $this->sizeDisp . " bytes";
            return $this;
        }

        if ($this->size >= 1024 && $this->size < 1048576) {
            $this->sizeDisp = round($this->size / 1024, 2) . " kb";
            return $this;
        }

        if ($this->size >= 1048576 && $this->size < 1073741824) {
            $this->sizeDisp = round($this->size / 1048576, 2) . " mb";

            return $this;
        }

        if ($this->size >= 1073741824) {
            $this->sizeDisp = round($this->size / 1073741824, 2) . " gb";

            return $this;
        }
    }

    public function removeHomeDir($path) {
        if (stristr($path, homeDir) !== false) {
            return str_ireplace(homeDir, '', $path);
        }
        return $path;
    }

    /**
     * sets $this->breadcrumbs to array with breadcrumbs
     * @param type $subDir dir to make breadcrumbs from
     * @return \FileHandler
     */
    public function breadcrumbs() {

        $dir = $this->removeHomeDir($this->dir);
        if (isset($dir) && !empty($dir)) {

            $dirList = explode("/", $dir);
            array_pop($dirList);
            $c = count($dirList) - 1;

            while (0 <= $c) {

                $i = $c;

                $path = array();

                while ($i >= 0) {

                    $path[$i] = $dirList[$i];

                    $i--;
                }

                if (is_array($path)) {

                    $path = array_reverse($path);
//var_dump($path);
                    $pDisp = end($path);

                    reset($path);

                    $path = implode("/", $path);

                    //$pLink = "home.php?loc=FileExplorer&s=" . urlencode($path);
                    //$pLink = $httpPath . "&s=" . urlencode($path); //change for nineteeneleven.info

                    $breadcrumbs[$c] = sprintf("<div class='breadcrumbs'><span onclick=\"openDir('%s')\" class='pointer'>%s/</span></div>", $path, $pDisp);
                }

                $c--;
            }

            array_push($breadcrumbs, sprintf("<div class='breadcrumbs'><span onclick=\"openDir('')\" class='pointer'>%s</span></div>", homeDir));
            //$breadcrumbs[0] =sprintf("<div class='breadcrumbs'><a href='%s'>%s</a></div>", $httpPath, $httpPath);
            $this->breadcrumbs = array_reverse($breadcrumbs);

            return $this;
        } else {
            $this->breadcrumbs[0] = sprintf("<div class='breadcrumbs'><span onclick=\"openDir('')\" class='pointer'>%s</span></div>", homeDir);

            return $this;
        }
    }

    /**
     * checks if the file size is within the limits set in the config
     * @return \FileHandler
     */
    public function checkSize() {
        if (filesize($this->file) <= $GLOBALS['settings']['file_manager']['maxSize']) {
            $this->sizeGood = true;
        } else {
            $this->sizeGood = false;
        }
        return $this;
    }

    /**
     * Sets $this->openable based on settings to see if file can be viewed in browser
     * @return \FileHandler
     */
    private function checkOpen() {
        $extOpen = implode("|", $GLOBALS['settings']['file_manager']['extensions']['open']);

        $extOpen = str_replace(".", "\.", $extOpen);

        if (preg_match("/" . $extOpen . "$/i", $this->file) !== 1) {

            $this->openable = false;
        } else {
            $this->openable = true;
        }
        return $this;
    }

    /**
     * sets $this->fileProps
     *   8 if zip
     *   4 if directory
     *   2 if file is openable and downloadable
     *   0 if Downloadable only
     */
    public function checkFile() {

        if (is_file($this->file)) {
            $this->checkSize();
            //$this->openable;
            $this->checkOpen();
            if ($this->splFile->getExtension() == 'zip') {
                $this->fileProps = 8;
            } elseif ($this->sizeGood && $this->openable) {

                $this->fileProps = 2;
            } else {
                $this->fileProps = 0;
            }
        } elseif (is_dir($this->file)) {

            $this->fileProps = 4;
        }

        return $this;
    }

    public function scanDir() {
        if (is_dir($this->dir) === true) {
            $contents = scandir($this->dir);
            unset($contents[0]);
            unset($contents[1]);
            $this->dirContents = $contents;
        } else {

            die('We were unable to get the contents of the directory you requested.');
        }
    }

    public function getPerms() {
        $this->perms = $this->splFile->getPerms() & 0777;
        return $this;
    }

    public function dispDir() {
        $this->scanDir();
        echo "<input type='hidden' value='" . $this->removeHomeDir($this->dir) . "' id='feCurDir'>";
        echo "<script>
            $('#uploadDir').val('" . $this->removeHomeDir($this->dir) . "');
                </script>
";
        foreach ($this->dirContents as $contents) {
            $fullPath = $this->dir . $contents;
            $this->setFile($fullPath);
            $relPath = $this->removeHomeDir($fullPath);
            $this->checkFile($fullPath);
            if ($GLOBALS['settings']['file_manager']['allowhidden'] === false) {
                if (preg_match('/^(\.[a-z]|\.[A-Z]|\.[0-9])/', $contents)) {
                    continue;
                }
            }
//            var_dump($this);
//            die();

            switch ($this->fileProps) {
                case '8':
                    clearstatcache();

                    printf("<li class='dirList'><input type='checkbox' name='file' value='%s'><div class='fileList file'>%s</div>"
                            . "<div class='fileList size'>(%s)</div>"
                            . "<div class='fileList perms'>%o</div>"
                            . "<span Onclick='unzipFile(\"%s\")' class='feBtn pointer'>unzip</span></li>", $fullPath, $contents, $this->sizeDispGetter($this->splFile->getSize()), $this->getPerms()->perms, $fullPath);
                    break;

                case '4':

                    printf("<li class='dirList'><input type='checkbox' name='file' value='%s'><span onclick=\"openDir('%s')\" class='fileList dir pointer'>%s/</span>"
                            . "<div class='size'>(%s)</div></li>", $fullPath, $relPath, $contents, $this->dirSizeDispGetter($fullPath));
                    //echo "dir: $contents <br />";
                    break;
                case '2':
                    clearstatcache();

                    printf("<li class='dirList'><input type='checkbox' name='file' value='%s'><div class='fileList file'>%s</div>"
                            . "<div class='fileList size'>(%s)</div>"
                            . "<div class='fileList perms'>%o</div>"
                            . "<span OnClick='openFile(\"%s\")' class='feBtn pointer'>open</span>"
                            . "<a href='home.php?loc=download&file=%s' target='_BLANK' class='feBtn'>download</a></li>", $fullPath, $contents, $this->sizeDispGetter($this->splFile->getSize()), $this->getPerms()->perms, $fullPath, $fullPath);
                    // echo "Openable: $contents <br />";
                    break;
                case '0':
                    clearstatcache();

                    printf("<li class='dirList'><input type='checkbox' name='file' value='%s'><div class='fileList file'>%s</div>"
                            . "<div class='fileList size'>(%s)</div>"
                            . "<div class='fileList perms'>%o</div>"
                            . "<a href='home.php?loc=download&file=%s' class='feBtn'>download</a></li>", $fullPath, $contents, $this->sizeDispGetter($this->splFile->getSize()), $this->getPerms()->perms, $fullPath);
                    break;
                default:
                    //shouldnt be here;
                    break;
            }
        }
    }

}

class Pathing {

    /**
     * removes forward slash at the beginning of a path.
     * @param type $path
     * @return type string
     */
    public function rBSlash($path) {
        if (strpos($path, "/") === 0) {
            $path = substr($path, 1);
        }
        return $path;
    }

    /**
     * Adds trailing slash if not present
     * @param string $path
     * @return string string
     */
    public function aTSlash($path) {
        #make sure trailing slash is there
        if (substr($path, -1) != '/') {
            $path = $path . '/';
        }
        return $path;
    }

    /**
     * removes forward slash at the end of a path.
     * @param type $path
     * @return type
     */
    public function rTSlash($path) {
        if (strrpos($path, '/') === (strlen($path) - 1)) {
            return substr($path, 0, -1);
        }
        return $path;
    }

    /**
     * makes a path absolute using the homeDir define.
     * @param type $path
     * @return type
     */
    public function makeAbs($path) {
        $path = preg_replace('/[^a-zA-Z0-9_.-\/]/', '', $path); //not sure why this was here, removed it because it was messing with moving files
        $path = $this->rBSlash($path);
        $path = $this->aTSlash($path);
        return homeDir . $path;
    }

    public function removeHomeDir($path) {
        if (stristr($path, homeDir) !== false) {
            return str_ireplace(homeDir, '', $path);
        }
        return $path;
    }

    public function addHomeDir($path) {
        return homeDir . $path;
    }

}

class LockFile {

    function __construct($dir) {
        $this->pathing = new Pathing;
        $this->fname = "lock.file";
        $this->dir = $this->pathing->aTSlash($dir) . $this->fname;
    }

    function __destruct() {
        unset($this->pathing);
    }

    public function lock() {

        if ($this->check()) {
            return array(false, "Lock file found!");
        }

        $handle = fopen($this->dir, 'w');
        fwrite($handle, date('U') . "\n");
        if (isset($_SESSION['username'])) {
            fwrite($handle, $_SESSION['username'] . "\n");
        }
        fclose($handle);
        return array(true, $this->dir);
    }

    public function unlock() {
        if ($this->check()) {
            $handle = fopen($this->dir, 'r');
            $timestamp = fgets($handle);
            fclose($handle);
            unlink($this->dir);
            return array(true, $timestamp);
        }
        return array(false, "No lock file found!");
    }

//    public function checkLocker() {
//        if ($this->check()) {
//            $data = file($this->dir);
//            if (isset($data[1])) {
//                $user = $data[1];
//            }
//            if (isset($user)) {
//                return( $user);
//            } else {
//                return false;
//            }
//        }
//    }
    public function checkLocker() {
        if ($this->check()) {
            $data = file($this->dir);
            return $data;
        }
    }

    /**
     * check if server is locked.
     * @return boolean true if locked, false if not
     */
    public function check() {
        if (is_file($this->dir)) {
            return true;
        }
        return false;
    }

}
