<?php
/*
 * loads our translation files.
 */
if (!defined('NineteenEleven')) {
    die('No direct access allowed.');
}
class language{

    public function __construct(){
        $this->dir = ABSDIR . "translations/";
        //windows code.
//        if ($c==0) {
//            $this->dir = str_replace("includes\LanguageClass.php", "", __FILE__, $c). "translations\\";
//        }
    }
    public function getLang($lang){
        $json = file_get_contents($this->dir . $lang . '.json');
        return json_decode($json);
    }

    public function listLang(){
        $scan = scandir($this->dir);
        $i = 0;
        foreach ($scan as $lang) {
            if (!is_dir($lang)) {
                $list[$i] = str_replace(".json", "", $lang);
            }
            $i++;
        }
        return $list;
    }

}

$language = new language;
$lang = $language->getLang(DEFAULT_LANGUAGE);